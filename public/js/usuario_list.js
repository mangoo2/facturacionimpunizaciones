var base_url = $('#base_url').val();
$(document).ready(function() {
    table();
});


function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Usuarios/getlistado",
            type: "post",
            "data":{empleado:$('#empleado_busqueda').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data":"Usuario"},
            {"data":"perfil"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.check_baja!=''){
                        html='<a class="btn btn_light_morado font-weight-bold btn-pill sus_'+row.personalId+'" data-motivo="'+row.motivo+'" data-fechabaja="'+row.fechabaja+'" onclick="ver_motivo('+row.personalId+')">Ver</a>\
                         <span class="label label-lg label-light-danger label-inline">Suspendido</span>'; 
                    }else{
                        html='';
                    }
                    
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<a class="btn btn_light_morado font-weight-bold btn-pill">Ver</a>'; 
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">\
                        <a href="'+base_url+'Usuarios/registro/'+row.personalId+'" class="btn"><i class="icon-xl far fa-edit"></i></a>\
                        <a onclick="eliminar_registro('+row.personalId+')" class="btn"><i class="icon-xl far fa-trash-alt"></i></a>';
                var btn_sus='';        
                        if(row.check_baja==''){
                            btn_sus='<a onclick="suspender_registro('+row.personalId+')" class="btn"><i class="icon-xl fas fa-power-off"></i></a>'; 
                        }else{
                            btn_sus='';
                        }
                        html+=btn_sus;
                    html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
    $('#table_datos').find('input[type=search]').val('');
    $("#table_datos").DataTable().search('').draw();
}

var id_registro_aux=0;
function eliminar_registro(id){ 
    id_registro_aux=id;
    $('#elimina_registro_modal').modal();
}

function delete_registro(){
    var idp=id_registro_aux; 
    $.ajax({
        type:'POST',
        url: base_url+"Usuarios/delete_record",
        data:{id:idp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            Swal.fire({
                    position: "Éxito",
                    icon: "success",
                    title: "Elimando Correctamente",
                    showConfirmButton: false,
                    timer: 1500
                });
            $('#elimina_registro_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}

var id_registro_sus_aux=0;
function suspender_registro(id){ 
    id_registro_sus_aux=id;
    $('#suspender_registro_modal').modal();
}

function update_registro(){
    var idp=id_registro_sus_aux; 
    $.ajax({
        type:'POST',
        url: base_url+"Usuarios/suspender_record",
        data:{id:idp,motivo:$('#motivo').val()},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            Swal.fire({
                    position: "Éxito",
                    icon: "success",
                    title: "Suspendido Correctamente",
                    showConfirmButton: false,
                    timer: 1500
                });
            $('#suspender_registro_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    }); 
}

function ver_motivo(id){
    var motivo = $('.sus_'+id).data('motivo');
    var fechabaja = $('.sus_'+id).data('fechabaja');
    $('#motivo_registro_modal').modal();
    $('#text_motivo').html(motivo);
    $('.txt_fecha').html(fechabaja);
    
}