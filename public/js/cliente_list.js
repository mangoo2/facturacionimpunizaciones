var base_url = $('#base_url').val();
var table;
$(document).ready(function() {
  $('.btn_pagina_4').addClass('menu-item-active');
  	table = $('#tabla_clientes').DataTable();
    
	loadtable();
	
});
function loadtable(){
	table.destroy();
	table = $('#tabla_clientes').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Clientes/getlistclientes",
            type: "post",
            "data": {
                'cliente':0},
        },
        "columns": [
            {"data": "clienteId"},
           
            {"data": "razon_social"},
            {"data": "cp"},
            {"data": "rfc"},
            {"data": "direccion"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        
                    html='<a class="btn btn_light_morado font-weight-bold btn-pill" href="'+base_url+'Folios?id='+row.clienteId+'&name='+row.razon_social+'">Ver</a>';
                    return html;
                }
            },
            {"data": null,
            	render:function(data,type,row){
                    var html='';
                        html='<div class="btn-group">\
                                <a href="'+base_url+'Clientes/registro/'+row.clienteId+'" class="btn btn-warning"><i class="icon-xl far fa-edit"></i></a>\
                                <a onclick="deletecliente('+row.clienteId+')" class="btn btn-warning"><i class="icon-xl far fa-trash-alt"></i></a>\
                            </div>';
                    return html;
                }
        	},
            
            
          
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    }).on('draw',function(){
        /*
        $('#search').keyup(function(){
     		table.search($(this).val()).draw() ;
		});
		*/

    });
    $('#tabla_clientes').find('input[type=search]').val('');
    $("#tabla_clientes").DataTable().search('').draw();
}
function deletecliente(id){
    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminarlo?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Clientes/deletecliente",
                        data: {
                            id:id
                        },
                        success:function(response){  
                            loadtable();
                             Swal.fire({
                                position: "Éxito",
                                icon: "success",
                                title: "Éxito!",
                                text:'Se ha eliminado correctamente',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
}
function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}