var base_url=$('#base_url').val();
$(document).ready(function($) {
  var validateSubmitForm = $('#validateSubmitForm');
      validateSubmitForm.validate({
            rules: 
            {
                Monto: {
                    equalTo: '#totalimportes'
                },
                NumOperacion:{
                  required:true
                }

            },
            // Para mensajes personalizados
            messages: 
            {
                Monto:{
                    required: "Ingrese un monto",
                    equalTo:'Por favor ingrece el total de los importes'
                },
               
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
        {   
            
            
        }
    
     });
  //----------------------------------------------------------------------------------------------------------------
	$('#btn_savecomplemento').click(function(event) {
		var form = $('#validateSubmitForm');
		if (form.valid()) {
      $('#modal_previefactura').modal('hide');
      $('body').loading({theme: 'dark',message: 'Timbrando complemento...'});
      //================================================
      var DATAr  = [];
        var TABLA   = $("#tabledocumentosrelacionados tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["idfactura"]      = $(this).find("input[id*='idfactura']").val();
            item ["IdDocumento"]    = $(this).find("input[id*='IdDocumento']").val();
            item ["serie"]          = $(this).find("input[id*='doc_serie']").val();
            item ["folio"]          = $(this).find("input[id*='doc_folio']").val();
            item ["NumParcialidad"] = $(this).find("input[id*='NumParcialidad']").val();
            item ["ImpSaldoAnt"]    = $(this).find("input[id*='ImpSaldoAnt']").val();
            item ["ImpPagado"]      = $(this).find("input[id*='ImpPagado']").val();
            item ["ImpSaldoInsoluto"]  = $(this).find("input[id*='ImpSaldoInsoluto']").val();
            item ["MetodoDePagoDR"] = $(this).find("input[id*='MetodoDePagoDR']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arraydocumento   = JSON.stringify(DATAr);
        var f_r = $('#facturarelacionada').is(':checked')==true?1:0;
        var f_r_t = $('#TipoRelacion option:selected').val();
        var f_r_uuid = $('#uuid_r').val();

			$.ajax({
		        type:'POST',
		        url: base_url+"Timbrado/addcomplemento",
		        data: form.serialize()+'&arraydocumento='+arraydocumento+'&f_r='+f_r+'&f_r_t='+f_r_t+'&f_r_uuid='+f_r_uuid,
		        success: function (response){
		        	 var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            
                           console.log(array);
                            Swal.fire({
                                position: "Éxito",
                                icon: "error",
                                title: "Error "+array.CodigoRespuesta+"!",
                                text:array.MensajeError,
                                showConfirmButton: false,
                                timer: 4500
                            });
                            
                            //retimbrar(array.facturaId,0)
                        }else{
                            Swal.fire({
                                position: "Éxito",
                                icon: "success",
                                title: "Se ha creado el complemento!",
                                text:array.MensajeError,
                                showConfirmButton: false,
                                timer: 3500
                            });
                            
                        }
                        setTimeout(function(){ 
                              window.location.href = base_url+"index.php/ComplementosPago"; 
                        }, 3000);
                        
		        	
		        }
		    });
      //=================================================================================
		}else{
			Swal.fire({
                                position: "Éxito",
                                icon: "error",
                                title: "Error !",
                                text:'Completa los campos requeridos',
                                showConfirmButton: false,
                                timer: 1500
                            });
		}
	});
  $('#btn_savecomplemento_previe').click(function(event) {
    btn_savecomplemento_preview();
  });
  $('#facturarelacionada').click(function(event) {
        if($('#facturarelacionada').is(':checked')){
            $('.divfacturarelacionada').show('show');
            $('#ImpSaldoAnt').prop('readonly',false);
        }else{
            $('.divfacturarelacionada').hide('show');
            $('#uuid_r').val('');
            $('#ImpSaldoAnt').prop('readonly',true);
        }
        /* Act on the event */
  });
});

/*
$.ajax({
        type:'POST',
        url: base_url+"Facturaslis/obtenerventasproductos",
        data: {
        	venta:id,
        	tipo: tipo
        },
        success: function (data){
        	console.log(data); 
        	var array = $.parseJSON(data);
        	$.each(array, function(index, item) {
        		if (parseFloat(item.iva)>0) {
        			var aiva=1;
        		}else{
        			var aiva=0;
        		}
        		agregarconceptovalor(item.cantidad,'','','','',item.producto,item.preciou,aiva);
        	});
        	funcionconceptos();
        }
    });
*/
function calcularsinsoluto(factura){
  var impsa=$('.ImpSaldoAnt_'+factura).val();
  var impp=$('.ImpPagado_'+factura).val();
  var impsi=parseFloat(impsa)-parseFloat(impp);
  $('.ImpSaldoInsoluto_'+factura).val(impsi.toFixed(2));

  var addtp = 0;
  $(".ImpPagado").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
  var valor =parseFloat(addtp.toFixed(2));
  console.log(valor);
  $('#totalimportes').val(valor);
  $('.totalimport').html(valor);

}
function adddocumento(cliente,fac){
  $('.listadodocumentos').html('');
  $.ajax({
        type:'POST',
        url: base_url+"Timbrado/obtenerfacturas",
        data: {
          cli:cliente,
          fact:fac
        },
        success: function (data){
          console.log(data);
          $('#modaldocumentos').modal();
          $('.listadodocumentos').html(data);
          
          $('#facturascli').DataTable({
            "order": [[ 0, "desc" ]]
          });

        }
    });
}
function adddoc(id){
  $.ajax({
        type:'POST',
        url: base_url+"Timbrado/documentoadd",
        data: {
          factura:id
        },
        success: function (data){
          var array = $.parseJSON(data);
          if (array.ImpSaldoAnt>0) {
            $('.documento_add_'+array.idfactura).remove();
              var html ='<tr class="documento_add_'+array.idfactura+'">\
                          <td>\
                            <div class="row">\
                            <div class="col-md-3">\
                              <input type="hidden" id="idfactura" class="form-control" value="'+array.idfactura+'"  readonly required>\
                              <input type="hidden" id="MetodoDePagoDR" class="form-control" value="'+array.MetodoDePagoDR+'"  readonly required>\
                              <input type="hidden" id="doc_folio" class="form-control" value="'+array.folio+'"  readonly required>\
                              <input type="hidden" id="doc_serie" class="form-control" value="'+array.serie+'"  readonly required>\
                             <label class="labeltable">Id del documento:</label>\
                             <input type="text" id="IdDocumento" class="form-control IdDocumentos" value="'+array.IdDocumento+'" readonly required>\
                            </div>\
                            <div class="col-md-2" style="padding: 0px;">\
                             <label class="labeltable">Número de parcialidad:</label>\
                             <input type="text" id="NumParcialidad" class="form-control" value="'+array.NumParcialidad+'"  required>\
                            </div>\
                            <div class="col-md-2" style="padding: 0px;">\
                             <label class="labeltable">Importe de saldo anterior:</label>\
                             <input type="text" id="ImpSaldoAnt" class="form-control ImpSaldoAnt_'+array.idfactura+'" value="'+array.ImpSaldoAnt+'" readonly required>\
                            </div>\
                            <div class="col-md-2">\
                             <label class="labeltable">Importe de Pagado:</label>\
                             <input type="text" id="ImpPagado" class="form-control ImpPagado ImpPagado_'+array.idfactura+'" value="0" oninput="calcularsinsoluto('+array.idfactura+')">\
                            </div>\
                            <div class="col-md-2">\
                             <label class="labeltable">Importe de Pagado:</label>\
                             <input type="text" id="ImpSaldoInsoluto" class="form-control ImpSaldoInsoluto_'+array.idfactura+'" value="'+array.ImpSaldoAnt+'" readonly>\
                            </div>\
                            <div class="col-md-1">\
                              <a class="btn btn-danger" onclick="deletedoc('+array.idfactura+')"><i class="icon-2x  flaticon-delete"></i></a>\
                            </div>\
                            </div>\
                          </td>\
                        </tr>';
              $('.tabletbodydr').append(html);
          }else{
            toastr["error"]("Documento Cubierto", "Alerta!");
          }
        }
    });
}
function deletedoc(id){
  $('.documento_add_'+id).remove();
}
function btn_savecomplemento_preview(){
    var form = $('#validateSubmitForm');
    if (form.valid()) {
      //================================================
      var DATAr  = [];
        var TABLA   = $("#tabledocumentosrelacionados tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["idfactura"] = $(this).find("input[id*='idfactura']").val();
            item ["IdDocumento"]   = $(this).find("input[id*='IdDocumento']").val();
            item ["serie"]          = $(this).find("input[id*='doc_serie']").val();
            item ["folio"]          = $(this).find("input[id*='doc_folio']").val();
            item ["NumParcialidad"]  = $(this).find("input[id*='NumParcialidad']").val();
            item ["ImpSaldoAnt"]  = $(this).find("input[id*='ImpSaldoAnt']").val();
            item ["ImpPagado"]  = $(this).find("input[id*='ImpPagado']").val();
            item ["ImpSaldoInsoluto"]  = $(this).find("input[id*='ImpSaldoInsoluto']").val();
            item ["MetodoDePagoDR"]  = $(this).find("input[id*='MetodoDePagoDR']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arraydocumento   = JSON.stringify(DATAr);
      
      //===
        var datos =form.serialize()+'&arraydocumento='+arraydocumento;
        $('#modal_previefactura').modal('show');
        setTimeout(function(){ 
            var urlfac=base_url+"index.php/Preview/complementodoc?"+datos;
            var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
            $('.preview_iframe').html(htmliframe);
            //window.location.href = base_url+"index.php/Preview/factura?"+datos; 
        }, 1000);
      //=================================================================================
    }else{
     
      Swal.fire({
                                position: "Éxito",
                                icon: "error",
                                title: "Error !",
                                text:'Completa los campos requeridos',
                                showConfirmButton: false,
                                timer: 1500
                            });
    }
}
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "4000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}