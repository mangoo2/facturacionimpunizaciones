var base_url = $('#base_url').val();
var total=0;
var blockearfact=1;
var trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
var reg_items=0;
$(document).ready(function($) {
	$('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Timbrado/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clienteId,
                        text: element.razon_social,
                        rfc: element.rfc,
                        uso_cfdi: element.uso_cfdi

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
    	var data = e.params.data;
        
        $('#rfc').val(data.rfc);
        if(data.rfc=='XAXX010101000'){
            $('#sdescripcion').val('Venta');
            datos_pg();
        }else{
            $('.agregardatospublicogeneral').html('');
        }
        $('#uso_cfdi').val(data.uso_cfdi);
    });
    $('#sunidadsat').select2();
	$('#sconseptosat').select2();
    $('#sdescripcion').select2();
	$('.registrofac_preview').click(function(event) {
		
		registrofac_preview();
	});
	$('.registrofac').click(function(event) {
        registrofac(1,0);
	});
    obtenerdatosretencion();
    /*$("#sdescripcion").autocomplete({
            source:function(request,response){
                $.ajax({
                    url:base_url+"ProductosHulesGrapas/searchproductos",
                    dataType:"json",
                    data:{
                        search: request.term
                    },
                    success:function(data){
                        response($.map(data, function (item) {
                            return {
                                label: item.nombre,
                                value: item.nombre,
                                precio: item.precio
                            }
                        }))
                    }
                })
            },
            select: function( event, ui ) {
                $('#sprecio').val(ui.item.precio);
                
            },
    });*/
    $('#facturarelacionada').click(function(event) {
        if($('#facturarelacionada').is(':checked')){
            $('.divfacturarelacionada').show('show');
        }else{
            $('.divfacturarelacionada').hide('show');
            $('#uuid_r').val('');
        }
        /* Act on the event */
    });
    $('#FormaPago').change(function(event) {
        if($('#FormaPago').val()=='PPD'){
            $('#MetodoPago').val(99);
        }
    });
});
function agregarconcepto(){
    var precio = $('#sprecio').val();
    var cantidad = $('#scantidad').val();
    var unidad = $('#sunidadsat option:selected').val();
    var unidadt = $('#sunidadsat option:selected').text();
    var concepto = $('#sconseptosat option:selected').val();
    var conceptot = $('#sconseptosat option:selected').text();
    var descripcion = $('#sdescripcion option:selected').val();
    var descripciont = $('#sdescripcion option:selected').text();
    var aiva = $('#aplicariva').is(':checked')==true?1:0;

    var rfc = $('#rfc').val();
    if(rfc=='XAXX010101000'){
        if($('#pg_global').is(':checked')){
            if(unidad!='ACT'){
                $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Atención!',
                                    content: 'la <b>Unidad SAT</b> para PUBLICO EN GENERAL debera de ser <b>ACT</b> "Actividad" '});   
                precio=0;
            }
            if(concepto!='01010101'){
                $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Atención!',
                                    content: 'el <b>Concepto SAT</b> para PUBLICO EN GENERAL debera de ser <b>01010101</b> "No existe en el catálogo" '}); 
                                    precio=0; 
            }
            if(descripciont!='Venta'){
                $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Atención!',
                                    content: 'La <b>Descripción</b> para PUBLICO EN GENERAL debera de ser <b>Venta</b>'});  
                precio=0;
            }
        }
    }
    if(precio>0){
        agregarconceptovalor(cantidad,unidad,unidadt,concepto,conceptot,descripcion,precio,aiva,descripciont);
        $('#sdescripcion').val('');
        $('#scantidad').val(1);
        $('#sprecio').val(0);
    }
}
var rowcobrar=0;
function agregarconceptovalor(cantidad,unidad,unidadt,concepto,conceptot,descripcion,precio,aiva,descripciont){
	var subtotal=parseFloat(cantidad)*parseFloat(precio);
	if (aiva==1) {
		var siva =subtotal*0.16;
        if(trunquearredondear==0){
            siva = siva.toFixed(4);
        }else{
            siva=Math.floor(siva * 100) / 100;
        }
		
	}else{
		var siva =0.00;
	}
	
	var subtotal=parseFloat(subtotal)+parseFloat(siva);
		subtotal=subtotal.toFixed(2);
	var datoscobrar ='	<tr class="rowcobrar_'+rowcobrar+'">\
                          <td>\
                            <input type="number" name="cantidad" id="cantidad" \
                            class="cantidad_row_'+rowcobrar+' form-control-bmz" \
                            value="'+cantidad+'" style="display:none" readonly>\
                            '+cantidad+'\
                          </td>\
                          <td >\
                            <select name="sunidadsat" id="unidadsat" class="browser-default form-control-bmz unidadsat" style="display:none;">\
                            	<option value="'+unidad+'">'+unidadt+'</option>\
                            </select>\
                            '+unidadt+'\
                          </td>\
                          <td >\
                            <select name="conseptosat" id="conseptosat" class="browser-default form-control-bmz conseptosat" style="display:none;">\
                            	<option value="'+concepto+'">'+conceptot+'</option>\
                            </select>\
                            '+conceptot+'\
                          </td>\
                          <td >\
                            <input type="text" name="descripcion" id="descripcion" class="form-control-bmz" value="'+descripciont+'" style="background: transparent !important; border: 0px !important;" >\
                          </td>\
                          <td >\
                            <input type="number" name="precio" id="precio" class="preciorow form-control-bmz precio_'+rowcobrar+'" value="'+precio+'" style="display:none" readonly>\
                            '+precio+'\
                          </td>\
                          <td>\
                            <input type="number"\
                            name="descuento"\
                            id="descuento"\
                            data-diva="'+aiva+'"\
                            class="form-control-bmz cdescuento cdescuento_'+rowcobrar+'"\
                            onclick="activardescuento('+rowcobrar+')"\
                            value="0" style="width: 95px" readonly>\
                          </td>\
                          <td>\
                          	<input type="number" name="subtotal" id="subtotal" class="form-control-bmz csubtotal csubtotal_'+rowcobrar+'" value="'+subtotal+'" style="background: transparent !important; border: 0px !important; width: 95px" readonly>\
                          </td>\
                          <td >\
                            <input type="number"  id="tiva" value="'+siva+'" class="form-control-bmz siva tiva_'+rowcobrar+'" style="width: 95px;background: transparent !important; border: 0px !important;" readonly>\
                          </td>\
                          <td >\
                            <a class="waves-effect red btn-bmz" onclick="deleteconcepto('+rowcobrar+')"><i class="fas fa-trash-alt"></i></a>\
                          </td>\
                        </tr>';

	$('.addcobrar').append(datoscobrar);
	rowcobrar++;
	calculartotales();
}
function deleteconcepto(row){
	$('.rowcobrar_'+row).remove();
	calculartotales();
}
function calculartotales(){
	var totales = 0;
    /*
    $(".preciorow").each(function() {
        var vtotales = parseFloat($(this).val());
        totales += Number(vtotales);
    });
    */
    var TABLApr   = $("#table_conceptos tbody > tr");
        TABLApr.each(function(){ 
            var vtotales = parseFloat($(this).find("input[id*='cantidad']").val())*parseFloat($(this).find("input[id*='precio']").val());
                //vtotales = vtotales.toFixed(2);
                vtotales = redondear(vtotales,2);
                totales += Number(vtotales);
        });
    //=================================================
    var ivas = 0;
    $(".siva").each(function() {
        var vivas = parseFloat($(this).val());
        ivas += Number(vivas);
    });
    var dest = 0;
    $(".cdescuento").each(function() {
        var destc = parseFloat($(this).val());
        dest += Number(destc);
    });


    var subtotalc = parseFloat(totales);
    	subtotalc = subtotalc.toFixed(2);
    	ivas=ivas.toFixed(2);

    	totales=totales.toFixed(2);
        ivas = parseFloat(ivas);
        ivas =ivas.toFixed(2);

    var Subtotalinfo=parseFloat(subtotalc);
    var subtotalc=parseFloat(subtotalc);
    
    if ($('#risr').is(':checked')) {
        var v_isr=Subtotalinfo*0.1;
            v_isr =v_isr.toFixed(2);
            $('#isr').val(v_isr);
            totales=totales-v_isr;
            totales=totales.toFixed(2);
    }else{
        var v_isr=0;
        $('#isr').val(0.00);
    }
    if ($('#riva').is(':checked')) {
        var v_riva=Subtotalinfo*0.106666;
            v_riva =v_riva.toFixed(2);
            $('#ivaretenido').val(v_riva);
            totales=totales-v_riva;
            totales=totales.toFixed(2);
    }else{
        $('#ivaretenido').val(0.00);
    }
    if ($('#5almillar').is(':checked')) {
        var v_5millar=(Subtotalinfo/1000)*5;
            v_5millar =v_5millar.toFixed(2);
            $('#5almillarval').val(v_5millar);
            totales=totales-v_5millar;
            totales=totales.toFixed(2);
    }else{
        $('#5almillarval').val(0.00);
    }
    if ($('#aplicaout').is(':checked')) {
        var aplicaout = Subtotalinfo*0.06;
            aplicaout = aplicaout.toFixed(2);
        
            $('#outsourcing').val(aplicaout);
            totales=totales-aplicaout;
            totales=totales.toFixed(2);
    }else{
        $('#outsourcing').val(0.00);
    }

    totales=totales-parseFloat(dest)+parseFloat(ivas);
    $('#Subtotalinfo').val(Subtotalinfo);
    $('#Subtotal').val(subtotalc);
    $('#descuentof').val(dest);
    $('#iva').val(ivas);
    $('#total').val(totales.toFixed(2));

}
function calculartotales_set(tiempo){
    setTimeout(function(){ 
        calculartotales();
    }, tiempo);
}
function registrofac_preview(){
    blockearfact=1;
        var form =$('#validateSubmitForm');
        var valid =form.valid();
        if (valid) {
            //$( ".registrofac" ).prop( "disabled", true );
            var datos = form.serialize();
            var productos = $("#table_conceptos tbody > tr");
            //==============================================
                var DATAa  = [];
                productos.each(function(){         
                    item = {};                    
                    item ["Cantidad"]   = $(this).find("input[id*='cantidad']").val();
                    item ["Unidad"]  = $(this).find("select[id*='unidadsat']").val();
                    console.log($(this).find("select[id*='unidadsat']").val());
                    if ($(this).find("select[id*='unidadsat']").val()==null || $(this).find("select[id*='unidadsat']").val()=='') {
                        blockearfact=0;
                    }
                    item ["servicioId"]  = $(this).find("select[id*='conseptosat']").val();
                    console.log($(this).find("select[id*='conseptosat']").val());
                    if ($(this).find("select[id*='conseptosat']").val()==null || $(this).find("select[id*='conseptosat']").val()=='') {
                        blockearfact=0;
                    }
                    item ["Descripcion"]  = $(this).find("select[id*='conseptosat']").text();
                    item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
                    item ["Cu"]  = $(this).find("input[id*='precio']").val();
                    item ["descuento"]  = $(this).find("input[id*='descuento']").val();
                    item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
                    item ["iva"]  = $(this).find("input[id*='tiva']").val();
                    DATAa.push(item);
                });
                INFOa  = new FormData();
                aInfoa   = JSON.stringify(DATAa);
            //========================================
            var productoslength=productos.length;
            if (blockearfact==0) {
                productoslength=0;
            }
            if (productoslength>0) {
                //$('body').loading({theme: 'dark',message: 'Timbrando factura...'});

                var tiporelacion = $('input:radio[name=tiporelacion]:checked').val();
                var ventaviculada = $('#ventaviculada').val();
                var ventaviculadatipo = $('#ventaviculadatipo').val();
                var Subtotal=$('#Subtotal').val();
                var iva=$('#iva').val();
                var visr= $('#isr').val();
                var vriva= $('#ivaretenido').val();
                var v5millar= $('#5almillarval').val();
                var outsourcing= $('#outsourcing').val();

                var total=$('#total').val();               
                datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&tiporelacion='+tiporelacion+'&ventaviculada='+ventaviculada+'&ventaviculadatipo='+ventaviculadatipo;
                //===
                $('#modal_timbrado').modal({
                    dismissible: false
                });
                $('#modal_timbrado').modal('show');
                setTimeout(function(){ 
                    var urlfac=base_url+"index.php/Preview/factura?"+datos;
                    var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
                    $('.preview_iframe').html(htmliframe);

                }, 1000);
            }else{
                toastr["error"]("Agregar por lo menos un concepto", "Atención!");
            }
        }else{
        	toastr["error"]("faltan campos obligatorios", "Atención!");
        }
}

function registrofac(save,saveante){
    if(reg_items==0){
        reg_items++;
    $( ".registrofac" ).prop( "disabled", true );
        blockearfact=1;
        var form =$('#validateSubmitForm');
        var valid =form.valid();
        if (valid) {
            
            var datos = form.serialize();
            var productos = $("#table_conceptos tbody > tr");
            //==============================================
                var DATAa  = [];
                productos.each(function(){         
                    item = {};                    
                    item ["Cantidad"]   = $(this).find("input[id*='cantidad']").val();
                    item ["Unidad"]  = $(this).find("select[id*='unidadsat']").val();
                    console.log($(this).find("select[id*='unidadsat']").val());
                    if ($(this).find("select[id*='unidadsat']").val()==null || $(this).find("select[id*='unidadsat']").val()=='') {
                        blockearfact=0;
                    }
                    item ["servicioId"]  = $(this).find("select[id*='conseptosat']").val();
                    console.log($(this).find("select[id*='conseptosat']").val());
                    if ($(this).find("select[id*='conseptosat']").val()==null || $(this).find("select[id*='conseptosat']").val()=='') {
                        blockearfact=0;
                    }
                    item ["Descripcion"]  = $(this).find("select[id*='conseptosat']").text();
                    item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
                    item ["Cu"]  = $(this).find("input[id*='precio']").val();
                    if ($(this).find("input[id*='precio']").val()>0) { }else{
                        blockearfact=0;
                    }
                    item ["descuento"]  = $(this).find("input[id*='descuento']").val();
                    item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
                    item ["iva"]  = $(this).find("input[id*='tiva']").val();
                    DATAa.push(item);
                });
                INFOa  = new FormData();
                aInfoa   = JSON.stringify(DATAa);
            //========================================
            var productoslength=productos.length;
            if (blockearfact==0) {
                productoslength=0;
            }
            if (productoslength>0) {
                $('#modal_timbrado').modal('hide');
                if(save==0){
                    var textofactura='Guardado';
                }else{
                    var textofactura='Timbrando';
                }
                $('body').loading({theme: 'dark',message: textofactura+' factura...'});

                //====================================================================
                
                //====================================================================
                var f_r = $('#facturarelacionada').is(':checked')==true?1:0;
                var f_r_t = $('#TipoRelacion option:selected').val();
                var f_r_uuid = $('#uuid_r').val();

                var Subtotal=$('#Subtotal').val();
                var iva=$('#iva').val();
                var visr= $('#isr').val();
                var vriva= $('#ivaretenido').val();
                var v5millar= $('#5almillarval').val();
                var outsourcing= $('#outsourcing').val();
                var total=$('#total').val();               
                datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&save='+save+'&saveante='+saveante+'&f_r='+f_r+'&f_r_t='+f_r_t+'&f_r_uuid='+f_r_uuid;
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Timbrado/generafacturarabierta",
                    data: datos,
                    success: function (response){
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            Swal.fire({
                                position: "center",
                                icon: "error",
                                title: "Error "+array.CodigoRespuesta+"!",
                                text:array.MensajeError,
                                showConfirmButton: false,
                                timer: 3000
                            });
                            
                            //retimbrar(array.facturaId,0);
                        }else{
                            if(save==0){
                                var textofactura='Se ha Guardado la factura';
                            }else{
                                var textofactura='Se ha creado la factura';
                            }
                            Swal.fire({
                                position: "Éxito",
                                icon: "success",
                                title: "Éxito!",
                                text:textofactura,
                                showConfirmButton: false,
                                timer: 3000
                            });
                            //toastr["success"](textofactura, "Exito!");
                        }
                        setTimeout(function(){ 
                            window.location.href = base_url+"index.php/Folios"; 
                        }, 3000);
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Atención!"); 
                        reg_items=0;
                    }
                });
            }else{
                
                toastr["error"]("Agregar por lo menos un concepto", "Atención!");
                reg_items=0;
            }
        }else{
            toastr["error"]("faltan campos obligatorios", "Atención!");
            reg_items=0;
        }
    }
    
}
function obtenerdatosfactura(factura){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Timbrado/obtenerdatosfactura",
        data: {
            factura:factura
        },
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            $('#idcliente').html('<option value="'+array.facturas[0].clienteId+'" selected>'+array.facturas[0].Nombre+'<option>');
            
            $('#idcliente').select2();
            $('#rfc').val(array.facturas[0].Rfc);
            
            //$('#rfc').html('<option>'+array.facturas[0].Rfc+'<option>');
            //$('#rfc').select2();
            $('#numproveedor').val(array.facturas[0].numproveedor);
            $('#numordencompra').val(array.facturas[0].numordencompra);
            $('#Lote').val(array.facturas[0].Lote);
            $('#Caducidad').val(array.facturas[0].Caducidad);

            $('#FormaPago').val(array.facturas[0].FormaPago);

           

            $('#moneda').val(array.facturas[0].moneda);
            

            $('#uso_cfdi').val(array.facturas[0].uso_cfdi);
            $('#uso_cfdi').select2();

            $('#CondicionesDePago').val(array.facturas[0].CondicionesDePago);

            //formapagoselect(array.facturas[0].MetodoPago);
            $('#MetodoPago').val(array.facturas[0].MetodoPago);
            
            $('#numproveedor').val(array.facturas[0].numproveedor);
            $('#numordencompra').val(array.facturas[0].numordencompra);
            $('#observaciones').val(array.facturas[0].observaciones);
            if(array.facturas[0].isr>0){
                $('#risr').attr('checked',true);
            }
            if(array.facturas[0].ivaretenido>0){
                $('#riva').attr('checked',true);
            }
            if(array.facturas[0].cincoalmillarval>0){
                $('#5almillar').attr('checked',true);
            }
            if(array.facturas[0].outsourcing>0){
                $('#aplicaout').attr('checked',true);
            }
            array.facturasd.forEach(function(element) {
                if(element.iva>0){
                    var aplicaiva=1;
                }else{
                    var aplicaiva=0;
                }
                agregarconceptovalor(element.Cantidad,element.Unidad,element.Unidad,element.servicioId,element.Descripcion,element.Descripcion2,element.Cu,aplicaiva,element.Descripcion2);
                
                
            });
            
        }
    });
}
function obtenerdatosretencion(){
    
    
    
    
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Timbrado/getretenciones",
        
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            
            array.forEach(function(element) {
                if(element.retencionId==1){
                    if(element.estatus==1){
                        $('#risr').show('show');
                    }else{
                        $('.risr').hide('show');
                    }
                }
                if(element.retencionId==2){
                    if(element.estatus==1){
                        $('#riva').show('show');
                    }else{
                        $('.riva').hide('show');
                    }
                }
                if(element.retencionId==3){
                    if(element.estatus==1){
                        $('#5almillar').show('show');
                    }else{
                        $('.5almillar').hide('show');
                    }
                }
                if(element.retencionId==4){
                    if(element.estatus==1){
                        $('#aplicaout').show('show');
                    }else{
                        $('.aplicaout').hide('show');
                    }
                }
                
                
            });
            
        }
    });
}
function activardescuento(idrow){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    $('.cdescuento_'+idrow).attr({'readonly':false});
                                    $('.cdescuento_'+idrow).removeAttr("onclick");
                                    //$('.cdescuento_'+idrow).removeAttr("onclick");
                                    $('.cdescuento_'+idrow).attr('onchange', 'calculardescuento('+idrow+');');
                                }else{
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'No tiene permisos'}); 
                                }
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });
                    
                }else{
                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'Ingrese una contraseña'}); 
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function calculardescuento(idrow){
    var costo =  parseFloat($('.cantidad_row_'+idrow).val())*parseFloat($('.precio_'+idrow).val());
                costo.toFixed(2);
    var descuento = $('.cdescuento_'+idrow).val();
    var rowtotal =parseFloat(costo)-parseFloat(descuento);
    var ivaif = $('.cdescuento_'+idrow).data('diva');
    if (ivaif==1) {
        var siva =rowtotal*0.16;
        //siva = siva.toFixed(2);
        if(trunquearredondear==0){
            siva = siva.toFixed(4);
        }else{
            siva=Math.floor(siva * 100) / 100;
        }
    }else{
        var siva =0.00;
    }
    $('.tiva_'+idrow).val(siva);
    var totalg = parseFloat(rowtotal)+parseFloat(siva);
    $('.csubtotal_'+idrow).val(totalg);
    calculartotales();
}
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "4000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function pgglobal(){
    setTimeout(function(){ 
        var pgg=$('#pg_global').is(':checked');
        if(pgg==true){
            $('.infoglobal').show();
            $('#pg_global').val(1);
        }else{
            $('.infoglobal').hide();
            $('#pg_global').val(0);
        }
    }, 1000);
}
function datos_pg(){
    //05 Bimestral solo es para el regimen del cliente que emite 621
        var html='<div>\
                    <input type="checkbox" class="filled-in" id="pg_global" name="pg_global" wtx-context="4D1A9C7C-0B37-41A5-88EB-ECB7877AFCAD" onclick="pgglobal()">\
                    <label for="pg_global">GLOBAL</label>\
                 </div>\
                 <div class="infoglobal row" style="display:none">\
                <label class="col-md-2">Periodicidad</label><div class="col-md-2">\
                    <select class="form-control" name="pg_periodicidad" id="pg_periodicidad" onchange="v_periocidad()">\
                      <option value="01">01 Diario</option>\
                      <option value="02">02 Semanal</option>\
                      <option value="03">03 Quincenal</option>\
                      <option value="04">04 Mensual</option>\
                      <option value="05" disabled>05 Bimestral</option>\
                    </select>\
                  </div>\
                  <label class="col-md-1" >Mes</label><div class="col-md-3">\
                    <select class="form-control" name="pg_meses" id="pg_meses">\
                      <option value="01" class="select_no_bimestral">01 Enero</option>\
                      <option value="02" class="select_no_bimestral">02 Febrero</option>\
                      <option value="03" class="select_no_bimestral">03 Marzo</option>\
                      <option value="04" class="select_no_bimestral">04 Abril</option>\
                      <option value="05" class="select_no_bimestral">05 Mayo</option>\
                      <option value="06" class="select_no_bimestral">06 Junio</option>\
                      <option value="07" class="select_no_bimestral">07 Julio</option>\
                      <option value="08" class="select_no_bimestral">08 Agosto</option>\
                      <option value="09" class="select_no_bimestral">09 Septiembre</option>\
                      <option value="10" class="select_no_bimestral">10 Octubre</option>\
                      <option value="11" class="select_no_bimestral">11 Noviembre</option>\
                      <option value="12" class="select_no_bimestral">12 Diciembre</option>\
                      <option value="13" class="select_bimestral">13 Enero-Febrero</option>\
                      <option value="14" class="select_bimestral">14 Marzo-Abril</option>\
                      <option value="15" class="select_bimestral">15 Mayo-Junio</option>\
                      <option value="16" class="select_bimestral">16 Julio-Agosto</option>\
                      <option value="17" class="select_bimestral">17 Septiembre-Octubre</option>\
                      <option value="18" class="select_bimestral">18 Noviembre-Diciembre</option>\
                    </select>\
                  </div>\
                  <label class="col-md-1" >Año</label><div class="col-md-3">\
                    <input type="number" name="pg_anio" id="pg_anio" class="form-control" readonly>\
                  </div></div>';
        $('.agregardatospublicogeneral').html(html);
        setTimeout(function(){ 
            var mesactual = $('#mesactual').val();
            var anioactual = $('#anioactual').val();
            console.log(mesactual);
            console.log(anioactual);
            $('#pg_meses').val(mesactual);
            $('#pg_anio').val(anioactual);
            $('.select_bimestral').hide('show');
        }, 1000);
}
function v_periocidad(){
    var mesactual = $('#mesactual').val();
    var pg_periodicidad = $('#pg_periodicidad').val();
    if(pg_periodicidad=='05'){
        $('.select_no_bimestral').hide('show');
        $('.select_bimestral').show('show');
        if(mesactual=='01' || mesactual=='02'){
            var mesactual_n=13;
        }
        if(mesactual=='03' || mesactual=='04'){
            var mesactual_n=14;
        }
        if(mesactual=='05' || mesactual=='06'){
            var mesactual_n=15;
        }
        if(mesactual=='07' || mesactual=='08'){
            var mesactual_n=16;
        }
        if(mesactual=='09' || mesactual=='10'){
            var mesactual_n=17;
        }
        if(mesactual=='11' || mesactual=='12'){
            var mesactual_n=18;
        }
        $('#pg_meses').val(mesactual_n);
    }else{
        $('#pg_meses').val(mesactual);
        $('.select_no_bimestral').show('show');
        $('.select_bimestral').hide('show');
    }
    
}


function check_datosgenerales(){
    if($('#iddatosgenerales').is(':checked')){
        $('.texto_datos_generales').css('display','block');
    }else{
        $('.texto_datos_generales').css('display','none');
    }
}

function seleccionarproducto(){
    var id=$('#sdescripcion option:selected').val();
    var precio=$('#sdescripcion option:selected').data('precio');
    $('#sprecio').val(precio);
}
function redondearLejosDeCero(numero){
    return Math.sign(numero) * Math.floor(Math.abs(numero) + 0.5);
}
function redondear(numero, digitos){
    let base = Math.pow(10, digitos);
    let entero = redondearLejosDeCero(numero * base);
    return entero / base;
}