var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	//table();
});
function reload_registro(){
	tabla=$("#table_datos").DataTable();
    tabla.destroy();
    table();
}
function table(){
	var points_check = $('#points_check').val();
	var fecha_inicio = $('#fecha_inicio').val();
	var fecha_fin = $('#fecha_fin').val();
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Reportes/getlistado",
            type: "post",
            "data":{fecha_inicio:fecha_inicio,fecha_fin:fecha_fin,points_check:points_check},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"reg"},
            {"data":"empleado"},
            {"data":"point"},
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            },
            "buttons": {
                "copy": 'Copiar',
                "csv": 'Exportar a CSV',
                "print": 'Imprimir',
            }
        },
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
}
function fecha_actual(){
	var fecha_hoy=$('#fecha_hoy').val();
	if($('#fecha_actual').is(':checked')){
        $('#fecha_inicio').val(fecha_hoy);
        $('#fecha_fin').val(fecha_hoy);
        reload_registro();
	}else{
		$('#fecha_inicio').val('');
		$('#fecha_fin').val('');
	}
}
function fechas_in_fi(){
    reload_registro();
}