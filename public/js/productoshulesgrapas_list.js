var base_url = $('#base_url').val();
var tabla;

$(document).ready(function() {
    table();
    $("#modal_canteogrias").on("hidden.bs.modal", function () {
        // Aquí va el código a disparar en el evento
        $('#modal_nuevo_producto').css('opacity', 1.5);
    });	

});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#tabla_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"ListaVacunas/getlistado",
            type: "post",
            "data":{buscar_registro:$('#buscar_registro').val()},
            error: function(){
               $("#tabla_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"nombre"},
            {"data":"precio"},
            {"data":"laboratorio"},
            {"data": null,
                "render": function ( data, type, row, meta ){

                var html='<div class="btn-group">\
                        <a class="btn btn-warning"><i class="icon-xl far fa-edit pro_'+row.id+'"\
                            data-codigo="'+row.codigo+'"\
                            data-nombre="'+row.nombre+'"\
                            data-idcategoria="'+row.idcategoria+'"\
                            data-precio="'+row.precio+'"\
                            data-stock="'+row.stock+'"\
                            data-laboratorio="'+row.laboratorio+'"\
                            data-codigo="'+row.codigo+'"\
                            onclick="editar_registro('+row.id+')"></i></a>\
                        <a class="btn btn-warning"><i class="icon-xl far fa-trash-alt" onclick="eliminar_registro('+row.id+')"></i></a>\
                    </div>';
                    
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
    $('#tabla_datos').find('input[type=search]').val('');
    $("#tabla_datos").DataTable().search('').draw();
}

function guardar_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            codigo:{
              required: true
            },
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'ListaVacunas/registrar_datos',
            data: datos,
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                Swal.fire({
                    position: "Éxito",
                    icon: "success",
                    title: "Guardado Correctamente",
                    showConfirmButton: false,
                    timer: 1500
                });
                $('#modal_nuevo_producto').modal('hide');
                reload_registro();
                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                }, 1500);
            }
        });
    }   
}

function modal_producto(){
	$('#modal_nuevo_producto').modal("show");
    $('#idproducto').val(0);
    $('#codigo').val('');
    $('#nombre').val('');
    $('#categoria').val('');
    $('#precio').val('');
    $('#stock').val('');
    $('#laboratorio').val('');
    $('#codigo').val('');
    cateforia_get();
}

function editar_registro(id){
    $('#modal_nuevo_producto').modal("show");
    $('#idproducto').val(id);
    $('#codigo').val($('.pro_'+id).data('codigo'));
    $('#nombre').val($('.pro_'+id).data('nombre'));
    var categoriaid = $('.pro_'+id).data('idcategoria');
    $('#precio').val($('.pro_'+id).data('precio'));
    $('#stock').val($('.pro_'+id).data('stock'));
    $('#laboratorio').val($('.pro_'+id).data('laboratorio'));
    $('#codigo').val($('.pro_'+id).data('codigo'));
    cateforia_get();
    setTimeout(function(){ 
        $("#categoria option[value='"+categoriaid+"']").attr("selected", true);
    }, 1000);
    
}

var id_registro_aux=0;
function eliminar_registro(id){ 
    id_registro_aux=id;
    $('#elimina_registro_modal').modal('show');

}

function delete_registro(){
    var idp=id_registro_aux; 
    $.ajax({
        type:'POST',
        url: base_url+"ListaVacunas/delete_record",
        data:{id:idp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            Swal.fire({
                    position: "Éxito",
                    icon: "success",
                    title: "Elimando Correctamente",
                    showConfirmButton: false,
                    timer: 1500
                });
            $('#elimina_registro_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}

function modal_categoria(){
    $('#modal_canteogrias').modal();
    $('#modal_nuevo_producto').css('opacity', 0.5);
    tabla_cat();
    $('#idcategoria').val(0);
    $('#nombre_categoria').val('');
    $('.vd_red').remove();
}

function guardar_registro_cat(){
    var form_register = $('#form_registro_cat');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_cat").valid();
    if($valid) {
        $('.btn_registro_cat').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'ListaVacunas/registrar_datos_cat',
            data: datos,
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                Swal.fire({
                    position: "Éxito",
                    icon: "success",
                    title: "Guardado Correctamente",
                    showConfirmButton: false,
                    timer: 1500
                });
                $('#modal_canteogrias').modal('hide');
                cateforia_get();
                setTimeout(function(){ 
                    $('.btn_registro_cat').attr('disabled',false);
                }, 1500);

            }
        });
    }   
}

function tabla_cat(){
    $.ajax({
        type:'POST',
        url: base_url+"ListaVacunas/tabla_datos_cat_get",
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('#tabla_datos_cat').html(data);
        }
    });  
    
}

var id_registro_cat_aux=0;
function eliminar_registro_cat(id){ 
    id_registro_cat_aux=id;
    $('#elimina_registro_modal_cat').modal('show');

}

function delete_registro_cat(){
    var idp=id_registro_cat_aux; 
    $.ajax({
        type:'POST',
        url: base_url+"ListaVacunas/delete_record_cat",
        data:{id:idp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            Swal.fire({
                    position: "Éxito",
                    icon: "success",
                    title: "Elimando Correctamente",
                    showConfirmButton: false,
                    timer: 1500
                });
            cateforia_get();
            $('#elimina_registro_modal_cat').modal('hide');
            setTimeout(function(){ 
                tabla_cat(); 
            }, 1000); 
        }
    });  
}

function cateforia_get(){
    $.ajax({
        type:'POST',
        url: base_url+"ListaVacunas/select_datos_cat_get",
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.categoria_txt').html(data);
        }
    });  
}

function editar_registro_cat(id){
    $('#idcategoria').val(id);
    $('#nombre_categoria').val($('.cat_'+id).data('nombre'));
}