var base_url = $('#base_url').val();
var tablec;
var envionconplemento;
$(document).ready(function() {
	tablec = $('#tabla_complementos').DataTable();
	loadtable_c();
});
function loadtable_c(){
    var carpetausuario = $('#carpetausuario').val();
    var inicio=$('#finicial_c').val();
    var fin=$('#ffinal_c').val();
    var inicio='';
    var fin='';
	tablec.destroy();
	tablec = $('#tabla_complementos').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/ComplementosPago/getlistcomplementos",
            type: "post",
            "data": {
                'finicio':inicio,
                'ffin':fin
            },
        },
        "columns": [
            {"data": "complementoId",
            	render:function(data,type,row){
            		var html='';
            		
                    if(row.Estado==1){
                        
                        html='<span class="switch switch_morado switch-icon">\
                                    <label>\
                                        <input type="checkbox" class="complementos_checked" id="complemento_'+row.complementoId+'" value="'+row.complementoId+'">\
                                        <span></span>\
                                    </label>\
                                </span>';
                    }else{
                        html='<span class="switch switch_morado switch-icon">\
                                    <label>\
                                        <input type="checkbox" class="complementos_checked" id="complemento_'+row.complementoId+'" value="'+row.complementoId+'" disabled>\
                                        <span></span>\
                                    </label>\
                                </span>';
                    }
                    return html;
            	}
        	},
            {"data": "Folio"},
            {"data": "R_nombre"},
            {"data": "R_rfc",},
            {"data": "Monto",
                render:function(data,type,row){
                    var html='';
                        html=new Intl.NumberFormat('es-MX').format(row.Monto);
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='<a class="btn btn-warning font-weight-bold btn-pill">Cancelado</a>';
                    }else if(row.Estado==1){
                        html='<a class="btn btn-success font-weight-bold btn-pill">Timbrado</a>';
                    }else if(row.Estado==2){
                        html='<a class="btn btn-danger font-weight-bold btn-pill">Retimbrar</a>';
                    }else{
                    	html='';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    if (row.Estado==0) {//cancelado
                    	html+='<a class="btn b-btn-primary tooltipped" href="'+base_url+'ComplementosPago/complementodoc/'+row.complementoId+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" >\
                                <i class="fas fa-file-pdf fa-2x"></i>\
                                </a> ';
                        html+='<a class="btn b-btn-primary tooltipped" href="'+base_url+row.rutaXml+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" >\
                        		<i class="far fa-file-code fa-2x"></i></a> ';
                        html+='<a class="btn b-btn-primary tooltipped" href="'+base_url+carpetausuario+'/facturas/'+row.rutaAcuseCancelacion+'" target="_blank" data-position="top" data-delay="50" data-tooltip="Acuse de cancelación" download="" >\
                        		<i class="far fa-file-code fa-2x" style="color: #ef7e4f;"></i></a> ';
                    }
                    if (row.Estado==1) {//
                    	html+='<a class="btn b-btn-primary tooltipped" href="'+base_url+'ComplementosPago/complementodoc/'+row.complementoId+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" >\
                                <i class="fas fa-file-pdf fa-2x"></i>\
                                </a> ';
                        html+='<a class="btn b-btn-primary tooltipped" href="'+base_url+''+row.rutaXml+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" >\
                        		<i class="far fa-file-code fa-2x"></i></a> ';
                            if(row.correoenviado==1){
                                var stylecorreo='style="color: #12264b;"';
                            }else{
                                var stylecorreo='';
                            }
                        html+='<a class="btn b-btn-primary tooltipped"  \
                                onclick="enviarcomplemento('+row.complementoId+')"\
                                target="_blank" data-position="top" data-delay="50" data-tooltip="XML" >\
                                <i class="icon-xl far fa-paper-plane" '+stylecorreo+'></i>\
                                </a> ';
                    }
                    if (row.Estado==2) {
                    	html+='<a type="button"\
                          class="b-btn b-btn-primary tooltipped btn-retimbrar" \
                          onclick="retimbrarcomplemento('+row.complementoId+')"\
                          data-position="top" data-delay="50" data-tooltip="Retimbrar" id="refacturar">\
                          <span class="fa-stack">\
                          <i class="fas fa-file fa-stack-2x"></i>\
                          <i class="fas fa-sync fa-stack-1x" style="color: red;"></i>\
                          </span>\
                        </a> ';
                    }
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
    $('.enviocorreomail').click(function(event) {
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Envio/enviocorreocomplemento",
            data: {
                conplementoid:envionconplemento,
                correo:$('#enviocorreo option:selected').val()
            },
            success:function(response){  
                console.log(response);
                Swal.fire({
                                position: "center",
                                icon: "success",
                                title: "Email",
                                text: 'Correo enviado',
                                showConfirmButton: false,
                                timer: 2500
                            });
                $('#modalseleccioncorreo').modal('hide');
            }
        });
    });
}
function cancelarcomplementos(){
	var complementoslis = $("#tabla_complementos tbody > tr");
	var DATAc  = [];
	var num_complemento_selected=0;
	var c_complementoId=0;
    complementoslis.each(function(){  
    	if ($(this).find("input[class*='complementos_checked']").is(':checked')) {
    		num_complemento_selected++;
    		                  
        	c_complementoId  = $(this).find("input[class*='complementos_checked']").val();
        	
    	}       
        
    });
    if (num_complemento_selected==1) {
        
    	//console.log(c_complementoId);
    	cancelarcomplemento(c_complementoId);
    	setTimeout(function(){ 
    		$('#contrasena').val('');
    	 }, 1000);
    	
    }else{
        Swal.fire({
                    position: "Éxito",
                    icon: "warning",
                    title: "Atención!",
                    text: 'Seleccione solo un complemento',
                    showConfirmButton: false,
                    timer: 2500
                });
    }
}
function retimbrarcomplemento(idcomplemento){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Timbrado/retimbrarcomplemento",
                    data: {
                        complemento:idcomplemento
                    },
                    success:function(response){  
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            Swal.fire({
                                position: "Éxito",
                                icon: "error",
                                title: "Error "+array.CodigoRespuesta+"!",
                                html: '<span title="'+array.info2+'">'+array.MensajeError+'</span>',
                                //footer: 'Detalle: '+array.info2,
                            });
                        }else{
                            Swal.fire({
                                position: "center",
                                icon: "success",
                                title: "Éxito!",
                                text: 'Complemento timbrado',
                                showConfirmButton: false,
                                timer: 2500
                            });
                            loadtable();
                        }

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
/*
function cancelarcomplemento(idcomplemento){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de realizar la cancelación de un complemento?<br>Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //===================================================
                            $('body').loading({theme: 'dark',message: 'Cancelando complemento...'});
                    //console.log(aInfoa);
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Timbrado/cancelarCfdicomplemento",
                            data: {
                                facturas:idcomplemento
                            },
                            success:function(response){  
                                console.log(response);
                                var array = $.parseJSON(response);
                                if (array.resultado=='error') {
                                    
                                    Swal.fire({position: "center",icon: "warning",title: "Error "+array.CodigoRespuesta+"!",text: array.MensajeError,showConfirmButton: false,timer: 3500});
                                }else{
                                    Swal.fire({position: "center",icon: "success",title: "Éxito!",text: 'Se ha Cancelado el complemento',showConfirmButton: false,timer: 3500});
                                    loadtable_c();
                                }
                                $('body').loading('stop');

                            }
                        });
                    
                //================================================
                                }else{
                                    
                                    Swal.fire({position: "center",icon: "warning",title: "Advertencia!",text: 'No tiene permisos',showConfirmButton: false,timer: 2500});
                                }
                        },
                        error: function(response){
                             
                            Swal.fire({position: "center",icon: "error",title: "Advertencia!",text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',showConfirmButton: false,timer: 2500}); 
                             
                        }
                    });
                    
                }else{
                    
                     Swal.fire({position: "center",icon: "warning",title: "Advertencia!",text: 'Ingrese una contraseña',showConfirmButton: false,timer: 2500});
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
*/
function cancelarcomplemento(idcomplemento){
    var html='<div class="row col-md-12">\
                <div class="col-md-12">\
                    ¿Está seguro de realizar la Cancelación de un complemento?<br>Se necesita permisos de administrador\
                </div>\
                <div class="col-md-12">\
                    <form method="post" action="#" autocomplete="off">\
                    <input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" autocomplete="off" required/>\
                    </form>\
                </div>\
              </div>\
              <div class="row col-md-12">\
                <div class="col-md-6">\
                    <label>Motivo Cancelación</label>\
                    <select id="motivocancelacion" class="form-control" onchange="mocance()">\
                        <option value="00" disabled selected> Seleccione una opcion</option>\
                        <option value="01" >01 Comprobante emitido con errores con relación</option>\
                        <option value="02" >02 Comprobante emitido con errores sin relación</option>\
                        <option value="03" >03 No se llevó a cabo la operación</option>\
                        <option value="04" >04 Operación nominativa relacionada en una factura global</option>\
                    </select>\
                </div>\
                <div class="col-md-6 class_foliorelacionado" style="display:none">\
                    <label>Folio Relacionado</label>\
                    <input type="text" id="foliorelacionado" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">\
                </div>\
              </div>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                var motivo = $('#motivocancelacion option:selected').val();
                var uuidrelacionado=$('#foliorelacionado').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //===================================================
                    
                    //console.log(aInfoa);
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Timbrado/cancelarCfdicomplemento",
                            data: {
                                facturas:idcomplemento,
                                motivo:motivo,
                                uuidrelacionado:uuidrelacionado
                            },
                            success:function(response){  
                                console.log(response);
                                var array = $.parseJSON(response);
                                if (array.resultado=='error') {
                                    Swal.fire({position: "center",icon: "warning",title: "Error "+array.CodigoRespuesta+"!",text: array.MensajeError,showConfirmButton: false,timer: 3500});
                                }else{
                                    Swal.fire({position: "center",icon: "success",title: "Éxito!",text: 'Se ha Cancelado el complemento',showConfirmButton: false,timer: 3500});
                                    loadtable_c();
                                }

                            }
                        });
                    
                //================================================
                                }else{
                                    Swal.fire({position: "center",icon: "warning",title: "Advertencia!",text: 'No tiene permisos',showConfirmButton: false,timer: 2500}); 
                                }
                        },
                        error: function(response){
                            Swal.fire({position: "center",icon: "error",title: "Advertencia!",text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',showConfirmButton: false,timer: 2500});    
                             
                        }
                    });
                    
                }else{
                    Swal.fire({position: "center",icon: "warning",title: "Advertencia!",text: 'Ingrese una contraseña',showConfirmButton: false,timer: 2500});
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function mocance(){
    var motivo=$('#motivocancelacion option:selected').val();
    if(motivo=='01'){
        $('.class_foliorelacionado').show('show');
    }else{
        $('.class_foliorelacionado').hide('show');
        $('#foliorelacionado').val('');
    }
}
function enviarcomplemento(idcomplemento){
    envionconplemento=idcomplemento;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/ComplementosPago/correoscliente",
        data: {
            idcomplemento:idcomplemento
        },
        success:function(response){  
           $('#enviocorreo').html(response);
           loadtable_c();
        }
    });
    $('#modalseleccioncorreo').modal();
}
function search(){
    var searchtext = $('#searchtext').val();
    tablec.search(searchtext).draw();
}