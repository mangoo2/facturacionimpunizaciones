var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	table();
});

function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#tabla_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Unidades/getlistado",
            type: "post",
            "data":{buscar_registro:$('#buscar_registro').val()},
            error: function(){
               $("#tabla_datos").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                var check_uni='';
                if(row.status==1){
                    check_uni='checked';
                }else{
                    check_uni='';
                }
                var html='<span class="switch switch_morado switch-icon">\
                            <label>\
                                <input type="checkbox" class="check_unidad_'+row.UnidadId+'" onclick="check_unidad('+row.UnidadId+')" '+check_uni+'>\
                                <span></span>\
                            </label>\
                        </span>';
                    
                return html;
                }
            },
            {"data":"Clave"},
            {"data":"nombre"},
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
    $('#tabla_datos').find('input[type=search]').val('');
    $("#tabla_datos").DataTable().search('').draw();
}

function check_unidad(id){
    var check_aux=0;
    if($('.check_unidad_'+id).is(':checked')){
        check_aux=1; 
    }else{
        check_aux=0;
    }
    $.ajax({
        type:'POST',
        url: base_url+'Unidades/update_check',
        data:{id:id,check:check_aux},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            tabla.ajax.reload();
        }
    });

}