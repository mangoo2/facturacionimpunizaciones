var base_url = $('#base_url').val();
$(document).ready(function($) {
	$("#barcode").keypress(function(e) {
        if(e.which == 13) {
          validarcodigo();
        }
      });
});
function validarcodigo(){
  alert(234567);
	var barcode = $("#barcode").val();
	$.ajax({
            type:'POST',
            url: base_url+'Tracking/validarcodigo',
            data: {
            	codigo:barcode
            },
            async: false,
            statusCode:{
                404: function(data){
			           $.toast({
			            heading: 'Error',
			            text: 'No se encuentra el archivo.',
			            position: 'top-right',
			            loaderBg:'#ff6849',
			            icon: 'error',
			            hideAfter: 3500
			          });
                },
                500: function(){
                    $.toast({
			            heading: 'Error',
			            text: '500.',
			            position: 'top-right',
			            loaderBg:'#ff6849',
			            icon: 'error',
			            hideAfter: 3500
			          });

                }
            },
            success:function(data){
            	var idcodigo=parseInt(data);
            	if (idcodigo==0) {
            		$.toast({
			            heading: 'Error',
			            text: 'Guia no existe',
			            position: 'top-right',
			            loaderBg:'#ff6849',
			            icon: 'error',
			            hideAfter: 3500
			          });
            	}else{
            		window.location.href = base_url+'Tracking/status/'+idcodigo;
            	}
                
                
            }
        });
}
function incidencia(){
    $('#modalincicencias').modal();
}
function incidenciachange(){
    var descripcion = $('#incidencia option:selected').data('descripcion');
    $('.incidenciaresultado').html(descripcion);
}
function camaraincidente(){
    $('.titulocamaradual').html('Captura Incidencia');
    $('.camaralive').show();
    $('.camaracaptura').hide();
    $('#modalcamaraincidente').modal();
    start();
}
function mensaje1(){
    $('#textmensaje1').val('');
    $('#modalmensaje1').modal();
}
function mensaje2(){
    $('#textmensaje2').val('');
    $('#modalmensaje2').modal();
}
function siguientestatus(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Tracking/siguientestatus",
        data: {
          status: $('#statusg').val(),
          idorden:$('#ordenid').val()
        },
        async: false,
        statusCode:{
                404: function(data){
                 $.toast({
                  heading: 'Error',
                  text: 'No se encuentra el archivo.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });
                },
                500: function(){
                    $.toast({
                  heading: 'Error',
                  text: '500.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });

                }
        },
        success: function (response){
            $.toast({
                  heading: 'success',
                  text: 'Siguiente Estatus registrado',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'success',
                  hideAfter: 3500
                });
            setTimeout(function(){ 
                location.reload();
            }, 3000);
        }
    });
}
function entregadostatus(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Tracking/siguientestatus",
        data: {
          status: 3,
          idorden:$('#ordenid').val()
        },
        async: false,
        statusCode:{
                404: function(data){
                 $.toast({
                  heading: 'Error',
                  text: 'No se encuentra el archivo.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });
                },
                500: function(){
                    $.toast({
                  heading: 'Error',
                  text: '500.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });

                }
        },
        success: function (response){
            $.toast({
                  heading: 'success',
                  text: 'Siguiente Estatus registrado',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'success',
                  hideAfter: 3500
                });
            setTimeout(function(){ 
                location.reload();
            }, 3000);
        }
    });
}
function trackingcancelacion(){
    $('#modalcancelacion').modal();
}
function trackingdevolucion(){
    $('#modaldevolucion').modal();
}
function trackingcancelacionok(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Tracking/cancelado",
        data: {
          idorden:$('#ordenid').val()
        },
        async: false,
        statusCode:{
                404: function(data){
                 $.toast({
                  heading: 'Error',
                  text: 'No se encuentra el archivo.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });
                },
                500: function(){
                    $.toast({
                  heading: 'Error',
                  text: '500.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });

                }
        },
        success: function (response){
            $.toast({
                  heading: 'success',
                  text: 'Cancelado registrado',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'success',
                  hideAfter: 3500
                });
            $('#modalcancelacion').modal('hide');
        }
    });
}
function trackingdevolucionok(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Tracking/devolucion",
        data: {
          idorden:$('#ordenid').val()
        },
        async: false,
        statusCode:{
                404: function(data){
                 $.toast({
                  heading: 'Error',
                  text: 'No se encuentra el archivo.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });
                },
                500: function(){
                    $.toast({
                  heading: 'Error',
                  text: '500.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });

                }
        },
        success: function (response){
            $.toast({
                  heading: 'success',
                  text: 'Devolucion registrado',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'success',
                  hideAfter: 3500
                });
            $('#modaldevolucion').modal('hide');
        }
    });
}
function statusfinalizado(){
    $('#modalfinalizado').modal();
}
//================================================================================================
var base_url = $('#base_url').val();
$(document).ready(function() {
   createSignature('patientSignature');
});
(function () {
  var lastTime = 0;
  var vendors = ['ms', 'moz', 'webkit', 'o'];
  for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }
  if(!window.requestAnimationFrame)
    window.requestAnimationFrame = function (callback, element) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = window.setTimeout(function () {
        callback(currTime + timeToCall);
      },
      timeToCall);
      lastTime = currTime + timeToCall;
      return id;
  };
  if(!window.cancelAnimationFrame)
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id);
  };
}());

Math.hypot = Math.hypot || function() {
  var y = 0, i = arguments.length;
  while (i--) y += arguments[i] * arguments[i];
  return Math.sqrt(y);
};

var colorLine = "black";

function setColorLine(canvasId, color) {
    colorLine = color;
    var canvas = document.getElementById(canvasId);
    if (canvas) {
        ctx = canvas.getContext("2d");
        ctx.strokeStyle = colorLine;
    }
}

function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;
    return canvas.toDataURL() == blank.toDataURL();
}
/*
function sendSignature(canvasId, mk, pk, ck, distribute, process){
    var canvas = $("#" + canvasId)[0];
    canvas.toBlob(function callback(blob){ 
        console.log(blob);
        $.ajax({
            url: "/privacity/uploadUrl",
            data: { file: "signature", mk: mk, pk:pk, ck:ck, d: distribute },
            success: function(uploadurl) {
                var formData = new FormData();
                formData.append("signature", blob);
                $.ajax({
                    url: uploadurl,
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: process ? process : function(response){
                        console.log(response);
                    }
                });
            }
        });
    });
}
*/

function enterfullscreen(){
    $("body").css({"overflow":"hidden", "position":"fixed"});
    var signature = $("#signature"); 
    $("body").find("#step1,#step2,#clinicHistory,.hiddeable").hide();
    signature.css({ "position": "fixed", "background-color":"white", "top":"10px", "left": "10px", "width": (window.width - 10) + "px", "height": (window.height - 10) + "px" }).show();
    $("body").append(signature);    
    $("body").scroll(0);
    $("#enterFullScreenButton").hide();
    signature.find(".sighiddeable").removeClass("hidden-sm").removeClass("hidden-xs");
    
    toggleFullScreen();

}

function restorefullscreen() {
    
    toggleFullScreen();
    
    $("body").css({"overflow":"visible", "position":"static"});
    var signature = $("#signature"); 
    $("body").find("#step1,#step2,#clinicHistory,.hiddeable").show();
    signature.css({ "position": "initial", "background-color":"initial" }).show();
    $("#aceptance").append(signature);  
    signature.find(".sighiddeable").not("#patientSignature").addClass("hidden-sm").addClass("hidden-xs");
    $("#saveFormButton,#sendPediatricButton").removeClass("hidden-sm").removeClass("hidden-xs");
    setTimeout(function(){
        try {
            $("html,body").animate({
                scrollTop: $("#signature").offset().top
            }, "fast");
        } catch (ex) { console.log(ex); }
    }, 150);
    
    $("body").animate({"scrollTop": $("body").height() + "px"}, "fast");
}


function clearCanvas(canvasId) {
    canvas = document.getElementById(canvasId);
    var ctx = canvas.getContext("2d"); 
    ctx.clearRect(0,0, canvas.width, canvas.height);
    return canvas;
}

function toggleFullScreen() {
    try {
        var doc = window.document,
            docEl = doc.documentElement,
            requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen,
            cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

      if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
      } else {
        cancelFullScreen.call(doc);
      }
    } catch (ex) {
        console.log("Error al entrar/salir de fullscreen");
    }
}

function createSignature(canvasId) {
    var canvas = document.getElementById(canvasId),
        ctx = canvas.getContext("2d"),
        mouse = {
            x:0,
            y:0,
            buttonLastRaw:0, // user modified value 
            buttonRaw:0,
            buttons:[1,2,4,6,5,3], // masks for setting and clearing button raw bits;
        };
    
    $canvas = $(canvas);
    $canvas.closest(".signature").find(".clearSignature").click(function(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    });
    $canvas.closest(".signature").find("#enterFullScreenButton").click(enterfullscreen);
    $canvas.closest(".signature").find("#restoreFullScreenButton").click(restorefullscreen);
    
    if (document.fullscreenElement || 
            document.webkitFullscreenElement || 
            document.mozFullScreenElement ||
            document.fullscreenEnabled) {
        try {
            document.webkitExitFullscreen();
        } catch (ex){ console.log(ex); }
        try {
           // document.exitFullscreen();
        } catch (ex){ console.log(ex); }
    }
    
    function mouseMove(event){
        mouse.x = event.offsetX;  mouse.y = event.offsetY; 
        
        if(mouse.x === undefined){ mouse.x = event.clientX;  mouse.y = event.clientY;}
        if(event.type === "mousedown"){ 
            mouse.buttonRaw |= mouse.buttons[event.which-1];
        } else if(event.type === "mouseup"){
            mouse.buttonRaw &= mouse.buttons[event.which+2];
        } else if(event.type === "mouseout"){ 
            mouse.buttonRaw = 0; mouse.over = false;
        } else if(event.type === "mouseover"){ 
            mouse.over = true; 
        }
        event.preventDefault();
    }
    
    function processMouse(x, y) {
        
    }
    
        
    /*canvas.addEventListener("touchstart", function (e) {
        mousePos = getTouchPos(canvas, e);
        currentLine.push([mousePos.x,mousePos.y])
          var touch = e.touches[0];
          var mouseEvent = new MouseEvent("mousedown", {
            clientX: touch.clientX,
            clientY: touch.clientY,         
          });
          canvas.dispatchEvent(mouseEvent);
    }, false);
    canvas.addEventListener("touchend", function (e) {
          var mouseEvent = new MouseEvent("mouseup", {});
          canvas.dispatchEvent(mouseEvent);
    }, false);*/
    
    canvas.addEventListener("touchstart", function (e) {
        var init = getTouchPos(canvas, e);  
        var touch = e.touches[0];
        ctx.beginPath();
        ctx.moveTo(init.x, init.y);     
    });
    
    canvas.addEventListener("touchend", function (e) {
        ctx.stroke();
    });
    canvas.addEventListener("touchmove", function (e) {
        var init = getTouchPos(canvas, e);  
        var touch = e.touches[0];
          ctx.lineWidth = 3;
          ctx.lineJoin = "round";
          ctx.lineCap = "round";
          ctx.strokeStyle = "black";
         // console.log("Line from X: (" + init.x + "," + init.y + ") to (" + touch.clientX + "," + touch.clientY + ")");
         // ctx.beginPath();
         // ctx.moveTo(init.x, init.y);
          ctx.lineTo(init.x, init.y);
          ctx.stroke();
          //requestAnimationFrame(draw);
    }, false);
        
        // Get the position of a touch relative to the canvas
        function getTouchPos(canvasDom, touchEvent) {
          var rect = canvasDom.getBoundingClientRect();
          return {
            x: touchEvent.touches[0].clientX - rect.left,
            y: touchEvent.touches[0].clientY - rect.top
          };
        }
    
    /*document.body.addEventListener("touchstart", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);
    document.body.addEventListener("touchend", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);
    document.body.addEventListener("touchmove", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);*/
    
    canvas.addEventListener('mousemove', mouseMove);
    canvas.addEventListener('mousedown', mouseMove);
    canvas.addEventListener('mouseup', mouseMove); 
    canvas.addEventListener('mouseout', mouseMove); 
    canvas.addEventListener('mouseover', mouseMove); 
    canvas.addEventListener("contextmenu", function(e){ e.preventDefault();}, false);

    // Line simplification based on
    // the RamerÃ¢â‚¬â€œDouglasÃ¢â‚¬â€œPeucker algorithm
    // referance https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm
    // points are and array of arrays consisting of [[x,y],[x,y],...,[x,y]]
    // length is in pixels and is the square of the actual distance.
    // returns array of points of the same form as the input argument points.
    var simplifyLineRDP = function(points, length) {
        var simplify = function(start, end) { // recursize simplifies points from start to end
            var maxDist, index, i, xx , yy, dx, dy, ddx, ddy, p1, p2, p, t, dist, dist1;
            p1 = points[start];
            p2 = points[end];   
            xx = p1[0];
            yy = p1[1];
            ddx = p2[0] - xx;
            ddy = p2[1] - yy;
            dist1 = (ddx * ddx + ddy * ddy);
            maxDist = length;
            for (var i = start + 1; i < end; i++) {
                p = points[i];
                if (ddx !== 0 || ddy !== 0) {
                    t = ((p[0] - xx) * ddx + (p[1] - yy) * ddy) / dist1;
                    if (t > 1) {
                        dx = p[0] - p2[0];
                        dy = p[1] - p2[1];
                    } else 
                    if (t > 0) {
                        dx = p[0] - (xx + ddx * t);
                        dy = p[1] - (yy + ddy * t);
                    } else {
                        dx = p[0] - xx;
                        dy = p[1] - yy;
                    }
                }else{
                    dx = p[0] - xx;
                    dy = p[1] - yy;
                }
                dist = dx * dx + dy * dy 
                if (dist > maxDist) {
                    index = i;
                    maxDist = dist;
                }
            }

            if (maxDist > length) { // continue simplification while maxDist > length
                if (index - start > 1){
                    simplify(start, index);
                }
                newLine.push(points[index]);
                if (end - index > 1){
                    simplify(index, end);
                }
            }
        }    
        var end = points.length - 1;
        var newLine = [points[0]];
        simplify(0, end);
        newLine.push(points[end]);
        return newLine;
    }

    // This is my own smoothing method 
    // It creates a set of bezier control points either 2nd order or third order 
    // bezier curves.
    // points: list of points
    // cornerThres: when to smooth corners and represents the angle between to lines. 
    //     When the angle is smaller than the cornerThres then smooth.
    // match: if true then the control points will be balanced.
    // Function will make a copy of the points

    var smoothLine = function(points,cornerThres,match){  // adds bezier control points at points if lines have angle less than thres
        var  p1, p2, p3, dist1, dist2, x, y, endP, len, angle, i, newPoints, aLen, closed, bal, cont1, nx1, nx2, ny1, ny2, np;
        function dot(x, y, xx, yy) {  // get do product
            // dist1,dist2,nx1,nx2,ny1,ny2 are the length and  normals and used outside function
            // normalise both vectors
            dist1 = Math.sqrt(x * x + y * y); // get length
            if (dist1  > 0) {  // normalise
                nx1 = x / dist1 ;
                ny1 = y / dist1 ;
            }else {
                nx1 = 1;  // need to have something so this will do as good as anything
                ny1 = 0;
            }
            dist2  = Math.sqrt(xx * xx + yy * yy);
            if (dist2  > 0) {
                nx2 = xx / dist2;
                ny2 = yy / dist2;
            }else {
                nx2 = 1;
                ny2 = 0;
            }
           return Math.acos(nx1 * nx2 + ny1 * ny2 ); // dot product
        }
        newPoints = []; // array for new points
        aLen = points.length;
        if(aLen <= 2){  // nothing to if line too short
            for(i = 0; i < aLen; i ++){  // ensure that the points are copied          
                newPoints.push([points[i][0],points[i][1]]);
            }
            return newPoints;
        }
        p1 = points[0];
        endP =points[aLen-1];
        i = 0;  // start from second poitn if line not closed
        closed = false;
        len = Math.hypot(p1[0]-endP[0], p1[1]-endP[1]);
        if(len < Math.SQRT2){  // end points are the same. Join them in coordinate space
            endP =  p1;
            i = 0;             // start from first point if line closed
            p1 = points[aLen-2];
            closed = true;
        }       
        newPoints.push([points[i][0],points[i][1]])
        for(; i < aLen-1; i++){
            p2 = points[i];
            p3 = points[i + 1];
            angle = Math.abs(dot(p2[0] - p1[0], p2[1] - p1[1], p3[0] - p2[0], p3[1] - p2[1]));
            if(dist1 !== 0){  // dist1 and dist2 come from dot function
                if( angle < cornerThres*3.14){ // bend it if angle between lines is small
                      if(match){
                          dist1 = Math.min(dist1,dist2);
                          dist2 = dist1;
                      }
                      // use the two normalized vectors along the lines to create the tangent vector
                      x = (nx1 + nx2) / 2;  
                      y = (ny1 + ny2) / 2;
                      len = Math.sqrt(x * x + y * y);  // normalise the tangent
                      if(len === 0){
                          newPoints.push([p2[0],p2[1]]);                                  
                      }else{
                          x /= len;
                          y /= len;
                          if(newPoints.length > 0){
                              var np = newPoints[newPoints.length-1];
                              np.push(p2[0]-x*dist1*0.25);
                              np.push(p2[1]-y*dist1*0.25);
                          }
                          newPoints.push([  // create the new point with the new bezier control points.
                                p2[0],
                                p2[1],
                                p2[0]+x*dist2*0.25,
                                p2[1]+y*dist2*0.25
                          ]);
                      }
                }else{
                    newPoints.push([p2[0],p2[1]]);            
                }
            }
            p1 = p2;
        }  
        if(closed){ // if closed then copy first point to last.
            p1 = [];
            for(i = 0; i < newPoints[0].length; i++){
                p1.push(newPoints[0][i]);
            }
            newPoints.push(p1);
        }else{
            newPoints.push([points[points.length-1][0],points[points.length-1][1]]);      
        }
        return newPoints;    
    }

    // creates a drawable image
    var createImage = function(w,h){
        var image = document.createElement("canvas");
        image.width = w;
        image.height =h; 
        image.ctx = image.getContext("2d"); 
        return image;
    }  

    // draws the smoothed line with bezier control points.
    var drawSmoothedLine = function(line){
        var i,p;
        ctx.beginPath()
        ctx.moveTo(line[0][0],line[0][1])
        for(i = 0; i < line.length-1; i++){
           p = line[i];
           p1 = line[i+1]
           if(p.length === 2){ // linear 
                ctx.lineTo(p[0],p[1])
           }else if(p.length === 4){ // bezier 2nd order
               ctx.quadraticCurveTo(p[2],p[3],p1[0],p1[1]);
           }else{              // bezier 3rd order
               ctx.bezierCurveTo(p[2],p[3],p[4],p[5],p1[0],p1[1]);
           }
        }
        if(p.length === 2){
            ctx.lineTo(p1[0],p1[1])
        }
        ctx.stroke();
    }

    
    // smoothing settings
    var liveSmooth,
        lineSmooth = {},
        backBuffer = createImage(canvas.width,canvas.height),
        currentLine = [],
        drawing = false,
        input = false,
        smoothIt = false;
    lineSmooth.lengthMin = 1;  // square of the pixel length
    lineSmooth.angle = 0;      // angle threshold
    lineSmooth.match = false;  // not working.
    // back buffer to save the canvas allowing the new line to be erased
    mouse.lastButtonRaw = 0;  // add mouse last incase not there
    ctx.lineWidth = 3;
    ctx.lineJoin = "round";
    ctx.lineCap = "round";
    ctx.strokeStyle = colorLine;
    ctx.clearRect(0,0,canvas.width,canvas.height);
    
    function draw(){
        // if not drawing test for menu interaction and draw the menus
        if(!drawing){      
            /*if(mouse.x < 203 && mouse.y < 24){
                if(mouse.y < 13){
                    if(mouse.buttonRaw === 1){
                        ctx.clearRect(3,3,200,10);
                        lineSmooth.angle = (mouse.x-3)/200;
                        input = true;
                    }
                }else if(mouse.buttonRaw === 1){
                    ctx.clearRect(3,14,200,10);
                    lineSmooth.lengthMin = (mouse.x-3)/10;
                    input = true;
                }
                canvas.style.cursor = "pointer";
            }else{*/
                canvas.style.cursor = "crosshair";
            //}
            if(mouse.buttonRaw === 0 && input){
                input = false;
                mouse.lastButtonRaw = 0;
            }
            /*ctx.lineWidth = 0.5;
            ctx.fillStyle = "red";
            ctx.clearRect(3,3,200,10);
            ctx.clearRect(3,14,200,10);
            ctx.fillRect(3,3,lineSmooth.angle*200,10);
            ctx.fillRect(3,14,lineSmooth.lengthMin*10,10);

            ctx.textAlign = "left";
            ctx.textBaseline = "top";
            ctx.fillStyle = "#000"
            ctx.strokeRect(3,3,200,10);
            ctx.fillText("Smooth "+(lineSmooth.angle * (180 / Math.PI)).toFixed(0)+"deg",5,2)
            ctx.strokeRect(3,14,200,10);
            ctx.fillText("Detail "+lineSmooth.lengthMin.toFixed(0) + "pixels",5,13);*/

        }else{
            canvas.style.cursor = "crosshair"; 
        }
        if(!input){
             ctx.lineWidth = 3;
            if(mouse.buttonRaw === 4 && mouse.lastButtonRaw === 0){
                currentLine = [];
                drawing  = true;

                backBuffer.ctx.clearRect(0,0,canvas.width,canvas.height);
                backBuffer.ctx.drawImage(canvas,0,0);
                currentLine.push([mouse.x,mouse.y])
            }else if(mouse.buttonRaw === 4){
                var lp = currentLine[currentLine.length-1]; // get last point
                // dont record point if no movement
                if(mouse.x !== lp[0] || mouse.y !== lp[1] ){
                    currentLine.push([mouse.x,mouse.y]);
                    ctx.beginPath();
                    ctx.moveTo(lp[0],lp[1])
                    ctx.lineTo(mouse.x,mouse.y);
                    ctx.stroke();
                    liveSmooth = smoothLine(
                        simplifyLineRDP(
                            currentLine,
                            lineSmooth.lengthMin
                        ),
                        lineSmooth.angle,
                        lineSmooth.match
                    );
                    ctx.clearRect(0,0,canvas.width,canvas.height);
                   ctx.drawImage(backBuffer,0,0);
                    ctx.strokeStyle = "Blue";
                    drawSmoothedLine(liveSmooth );
                    ctx.strokeStyle = "black";
                }
            }else if(mouse.buttonRaw === 0 && mouse.lastButtonRaw === 4){
                ctx.textAlign = "center"
                ctx.fillStyle = "red"
                ctx.fillText("Smoothing...",canvas.width/2,canvas.height/5);
                smoothIt = true;
            }else if(smoothIt){
                smoothIt = false;
                
                var newLine = smoothLine(
                    simplifyLineRDP(
                        currentLine,
                        lineSmooth.lengthMin
                    ),
                    lineSmooth.angle,
                    lineSmooth.match
                );
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.drawImage(backBuffer,0,0);
                drawSmoothedLine(newLine);
                drawing  = false;
                
            }

            if(mouse.buttonRaw === 1 && mouse.lastButtonRaw === 0){
                currentLine = [];
                drawing  = true;

                backBuffer.ctx.clearRect(0,0,canvas.width,canvas.height);
                backBuffer.ctx.drawImage(canvas,0,0);
                currentLine.push([mouse.x,mouse.y])
            }else if(mouse.buttonRaw === 1){
                var lp = currentLine[currentLine.length-1]; // get last point
                // dont record point if no movement
                if(mouse.x !== lp[0] || mouse.y !== lp[1] ){
                    currentLine.push([mouse.x,mouse.y]);
                    ctx.beginPath();
                    ctx.moveTo(lp[0],lp[1])
                    ctx.lineTo(mouse.x,mouse.y);
                    ctx.stroke();
                }
            /*} else if(mouse.buttonRaw === 0 && mouse.lastButtonRaw === 1){
                ctx.textAlign = "center"
                ctx.fillStyle = "red"
                ctx.fillText("Smoothing...",canvas.width/2,canvas.height/5);
                smoothIt = true; */
            } else if(smoothIt){
                smoothIt = false;
                
                var newLine = smoothLine(
                    simplifyLineRDP(
                        currentLine,
                        lineSmooth.lengthMin
                    ),
                    lineSmooth.angle,
                    lineSmooth.match
                );
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.drawImage(backBuffer,0,0);
                drawSmoothedLine(newLine);
                drawing  = false;
                
            }
        }
        // middle button clear
        if(mouse.buttonRaw === 2){
            ctx.clearRect(0,0,canvas.width,canvas.height);
        }
        mouse.lastButtonRaw = mouse.buttonRaw;
        try {
            requestAnimationFrame(draw);
        } catch(ex) {}
    }

    draw();
}

function initQr(holder) {
    if (!holder) {
        holder = $("body");
    }
    signaturesFB = firebase.database().ref("signatures");
    holder.find(".signatureqr").each(function(i, item){
        var qrHolder = $(item),
            qrRef = signaturesFB.push(qrHolder.data());
        
        qrRef.onDisconnect().remove();
        qrRef.on("value", updateSignature);
        
        qrHolder.closest(".signatureholder").attr("data-sk", qrRef.key).data("sk", qrRef.key);
        
        qrCode = new QRCode(item, {
            text: qrRef.key,
            width: qrHolder.data("width") || 150,
            height: qrHolder.data("height") || 150,
            colorDark: "#029B90",
            colorLight: "#ffffff"
        });
    });
}

function updateSignature(snap){
    var key = snap.key,
        data = snap.val(),
        signatureHolder = $(".signatureholder[data-sk=" + key + "]");
    
    if (data.servingUrl) {
        signatureHolder.find(".resume img").attr("src", data.servingUrl);
        signatureHolder.find(".resume").show();
        signatureHolder.find(".editable").hide();
    }
    
}
function entrega(){
    var canvasfc = document.getElementById('patientSignature');
    var dataURLfc = canvasfc.toDataURL();

    var canvasine = document.getElementById('camaracapturainci');
    var dataURLine = canvasine.toDataURL();

    $.ajax({
        type:'POST',
        url: base_url+'index.php/Tracking/entrega',
        data: {
            firma:dataURLfc,
            orden:$('#ordenid').val(),
            otra:dataURLine

        },
        success:function(data){
            $.toast({
                        heading: 'Hecho!',
                        text: 'Solicitud generada.',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500, 
                        stack: 6
                      });
            setTimeout(function(){ 
                window.location.href = base_url+'Tracking';
            }, 3000);

                
        }
    });
}
function otraversona(){
  camaraincidente();
  $('.titulocamaradual').html('Captura Identificacion');
}