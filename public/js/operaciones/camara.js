var base_url = $('#base_url').val();
$(document).ready(function($) {
	$("#barcode").keypress(function(e) {
        if(e.which == 13) {
          validarcodigo();
        }
      });
});
function validarcodigo(){
	var barcode = $("#barcode").val();
	var point = $('#ponit').val();
	$.ajax({
            type:'POST',
            url: base_url+'Operacion/validarcodigo',
            data: {
            	codigo:barcode,
            	point:point
            },
            async: false,
            statusCode:{
                404: function(data){
			           $.toast({
			            heading: 'Error',
			            text: 'No se encuentra el archivo.',
			            position: 'top-right',
			            loaderBg:'#ff6849',
			            icon: 'error',
			            hideAfter: 3500
			          });
                },
                500: function(){
                    $.toast({
			            heading: 'Error',
			            text: '500.',
			            position: 'top-right',
			            loaderBg:'#ff6849',
			            icon: 'error',
			            hideAfter: 3500
			          });

                }
            },
            success:function(data){
            	var idcodigo=parseInt(data);
            	if (idcodigo==0) {
            		$.toast({
			            heading: 'Error',
			            text: 'El empleado no existe',
			            position: 'top-right',
			            loaderBg:'#ff6849',
			            icon: 'error',
			            hideAfter: 3500
			          });
            	}else{
            		window.location.href = base_url+'Operacion/empleado/'+idcodigo+'/'+point;
            	} 
            }
        });
}
function actulizar_vista(id){
    window.location.href = base_url+'Operacion/camara/'+id;
}
function regresar_vista(){
    window.location.href = base_url+'Operacion';
}