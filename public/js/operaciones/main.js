var base_url=$('#base_url').val();
'use strict'; 
  const video = document.querySelector('video');
  const canvas = window.canvas = document.querySelector('canvas');

//canvas.width = 480;
//canvas.height = 360;

//const button = document.querySelector('button');
const constraints = {
  audio: false,
  video: true
};

function handleSuccess(stream) {
  window.stream = stream; // make stream available to browser console
  video.srcObject = stream;
} 

function handleError(error) {
  console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
}
function start(){
  navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
}
///////
function normal(){

	stream.getTracks().forEach(function(track) {
  track.stop();
});

	const constraints = {
	  audio: false,
	  video: true
	};
	navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
}
function frontal(){
	stream.getTracks().forEach(function(track) {
  track.stop();
});
	const constraints = {
	  audio: false,
	  video: { facingMode: { exact: "environment" } }
	};
	navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
}
function stopcam(){
  stream.getTracks().forEach(function(track) {
    track.stop();
  });
}
function capturar(){
  $('.camaralive').hide();
  $('.camaracaptura').show();
	//$('.collapsible').collapsible('open', 1);
	canvas.width = video.videoWidth;
  	canvas.height = video.videoHeight;
  	canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
  $('.css_guardar').css('display','block');  
  stopcam();
}
//==================================================================================================================

function saveinvidencia(){
	var dataURL = canvas.toDataURL();
	console.log(dataURL);
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Tracking/saveincidencia",
        data: {
        	image:dataURL,
          descripcionimg:$('#textmensaje2').val(),
        	oberser:$('#textmensaje1').val(),
          incidencia: $('#incidencia option:selected').val(),
          status: $('#statusg').val(),
          idorden:$('#ordenid').val()
        },
        async: false,
        statusCode:{
                404: function(data){
                 $.toast({
                  heading: 'Error',
                  text: 'No se encuentra el archivo.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });
                },
                500: function(){
                    $.toast({
                  heading: 'Error',
                  text: '500.',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'error',
                  hideAfter: 3500
                });

                }
        },
        success: function (response){
            $.toast({
                  heading: 'success',
                  text: 'Inidencia registrada',
                  position: 'top-right',
                  loaderBg:'#ff6849',
                  icon: 'success',
                  hideAfter: 3500
                });
            $('#modalincicencias').modal('hide');
        }
    });
}
