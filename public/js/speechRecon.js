var recognition,
	initialValue = "",
	recognizing = false,
	activeMic = null;

$(function(){
	$("body").on("click", ".rec", toggleListening);
	$("body").on("blur", ".recordable", stopListening);
});

function createRecordables(holder) {
	if (('webkitSpeechRecognition' in window)) {
		if (!holder) {
			holder = $("body");
		}
		holder.find("textarea:not(.rinited)").not(".irecordeable").not("[readonly]").each(function(i, item){
			var txtArea = $(item);
			txtArea.wrap("<div class='recordableHolder'></div>");
			txtArea.after("<a class='rec' ><i class='fas fa-microphone rastreable' data-category='VoiceRecord' data-event='click' data-label='"+(txtArea.attr("name") || txtArea.attr("id"))+"'></i></a>");
			txtArea.addClass("rinited").addClass("recordable");
//			$(item).click(function(){ 
//				if (recognizing) {
//					stopListening();
//				} 
//			});
		});
	}
}
	
function initRecognition(button, textHolder) {
	if (!('webkitSpeechRecognition' in window)) {
		alert("La función no está disponible en éste navegador. Recomendamos el uso de Chrome", "error");
	} else {
		if (!recognition) {
			recognition = new webkitSpeechRecognition();
			recognition.lang = "es-MX";
			recognition.continuous = true;
			recognition.interimResults = true;
		}

		recognition.onstart = function() {
			recognizing = true;
			initialValue = textHolder.val();
			console.log("empezando a eschucar");
		}
		
		recognition.onresult = function(event) {
			var finalText = initialValue, lastEvent, preText = "";
			for (var i = event.resultIndex; i < event.results.length; i++) {
				if(event.results[i].isFinal) {
					finalText += event.results[i][0].transcript.replace(finalText, "");
				} else {
					preText += event.results[i][0].transcript.replace(finalText, "");
				}
		    }
			console.log("> Results Lenght: " +event.results.length + " Result Index: " + event.resultIndex + "\n FinalText: " + finalText);
		    if (event.results[event.resultIndex].isFinal) {
		    	textHolder.val(finalText).trigger("change");
				initialValue = finalText;
		    } else {
		    	textHolder.val(initialValue + " |" + preText);
		    }
		}
		recognition.onerror = function(event) {
		}
		recognition.onend = function() {
			recognizing = false;
			console.log("terminó de eschucar, llegó a su fin");
		}
	}
}

function toggleListening() {
	if (recognizing == false) {
		try {
			initRecognition(this, $(this).siblings(".recordable"));
			recognition.start();
			recognizing = true;
			activeMic = $(this);
			activeMic.find("i").removeClass("fa-microphone").addClass("fa-microphone-alt");
			activeMic.addClass("active");
			$(this).siblings(".recordable").focus();
		} catch (ex) {
			console.log("Error al iniciar el reconocimiento" + ex);
			recognition.stop();
			recognizing = false;
		}
	} else {
		try {
			recognition.stop();
			recognizing = false;
			activeMic.find("i").removeClass("fa-microphone-alt").addClass("fa-microphone");
			activeMic.removeClass("active");
		} catch (ex) {
			console.log("Error al detener el reconocimiento" + ex);
		}
	}
}

function stopListening() {
	if (recognition){
		try {
			recognition.stop();
			recognizing = false;
			activeMic.find("i").removeClass("fa-microphone-alt").addClass("fa-microphone");
			activeMic.removeClass("active");
		} catch (ex) {
			console.log("Error al detener el reconocimiento" + ex);
		}
	}
}