var base_url = $('#base_url').val();
$(document).ready(function(){
    $("#cerdigital").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["cer"],
            browseLabel: 'Seleccionar Certificado',
            uploadUrl: base_url+'Conf_facturacion/cargacertificado',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'cer': '<i class="fas fa-file-invoice"></i>',
            }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          //location.reload();
          toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        });
        $("#claveprivada").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["key"],
            browseLabel: 'Seleccionar clave privada',
            uploadUrl: base_url+'Conf_facturacion/cargakey',
            preferIconicZoomPreview: false,
            previewFileIcon: '<i class="fas fa-passport"></i>',
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'key': '<i class="fas fa-passport"></i>',
            },
            uploadExtraData: function (previewId, index) {
            var info = {pass:$('#passprivada').val()};
            return info;
          }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          //location.reload();
          toastr.success('Se cargo la claveprivada Correctamente','Hecho!');
        });
});
function instalararchivos(){
    $('#cerdigital').fileinput('upload');
    $('#claveprivada').fileinput('upload');
    setTimeout(function(){
        instalararchivosprocesar();
    }, 3000);
}
function instalararchivosprocesar(){
    $.ajax({
        type:'POST',
        url: base_url+"hulesyg/elementos/procesarfiles.php",
        success: function (data){
            
        }
    });
}