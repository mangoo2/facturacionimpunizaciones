var base_url = $('#base_url').val();
$(document).ready(function() {
	
	var avatar1 = new KTImageInput('kt_image_1');

     ClassicEditor
	.create( document.querySelector( '#kt-ckeditor-5' ) )
	.then( editor => {
		console.log( editor );
	} )
	.catch( error => {
		console.error( error );
	} );
	//============================================
		var formulario = $('#form-configuraciones');
	formulario.validate({
            ignore: "",
            rules: 
            {
				Nombre: {
                    required: true
                },
                Rfc: {
                    required: true
                },
            },
            
            errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form){
                
                var datos = formulario.serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/DatosGenerales/updatedatos",
                    data: datos,
                    success:function(data){  
                       Swal.fire({
				                position: "Éxito",
				                icon: "success",
				                title: "Éxito!",
				                text:'Se ha actualizado correctamente',
				                showConfirmButton: false,
				                timer: 1500
				            });
                       fileupload();
                    }
                });
               
            }
        });
	//============================================

});
function fileupload(){
	var file_data = $('#profile_avatar').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    $.ajax({
        url: base_url+'DatosGenerales/do_upload', // point to server-side controller method
        dataType: 'text', // what to expect back from the server
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            $('#msg').html(response); // display success response from the server
        },
        error: function (response) {
            $('#msg').html(response); // display error response from the server
        }
    });
}