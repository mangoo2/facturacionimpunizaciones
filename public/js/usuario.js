var base_url = $('#base_url').val();
var validar_user=0;
$(document).ready(function() {
	var avatar1 = new KTImageInput('kt_image_1');
});


function guardar_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
        	nombre:{
              required: true
            },
            Usuario:{
              required: true,
              minlength: 4
            },
            contrasena:{
              required: true,
              minlength: 5
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        if(validar_user==0){
            var datos = form_register.serialize();
            $('.btn_registro').attr('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'Usuarios/registrar_datos',
                data: datos,
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                	var idp=data;
                	guardar_registro_usu(idp);
                    add_file(idp);
                    Swal.fire({
                        position: "Éxito",
                        icon: "success",
                        title: "Guardado Correctamente",
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setTimeout(function(){ 
                        $('.btn_registro').attr('disabled',false);
                        //window.location = base_url+'Usuarios';
                    }, 1000);

                }
            });
        }else{
            Swal.fire("¡Atención!", "El usuario ya existe", "error");
        }
    }   
}

function guardar_registro_usu(id){
	var datos = $('#form_registro').serialize()+'&personalId_aux='+id;
	$.ajax({
            type:'POST',
            url: base_url+'Usuarios/add_usuarios',
            data: datos,
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
            }
        });
}

function verificar_usuario(){
    $.ajax({
        type: 'POST',
        url: base_url+'Usuarios/validar',
        data: {
            Usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                Swal.fire("¡Atención!", "El usuario ya existe", "error");
            }else{
                validar_user=0;
            }
        }
    });
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Usuarios/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                        //console.log(array.ok);
                        /*
                        if (array.ok=true) {
                            swal("Éxito", "Guardado Correctamente", "success");
                        }else{
                            swal("Error!", "No Se encuentra el archivo", "error");
                        }
                        */
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
var pass_aux=0;
function clickoverpass(){
    var obj = document.getElementById('contrasena');
    var obj2 = document.getElementById('contrasena2');
    if(pass_aux==0){
        pass_aux=1;  
        obj.type = "text";
        obj2.type = "text";
    }else{
        pass_aux=0;
        obj.type = "password";
        obj2.type = "password";
    }  

}