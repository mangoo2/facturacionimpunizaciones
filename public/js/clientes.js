var base_url = $('#base_url').val();
$(document).ready(function($) {
	$('.savecliente').click(function(event) {
		var DATAr  = [];
        var TABLA   = $("#table_mail tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["climailId"] = $(this).find("input[id*='climailId']").val();
            item ["email"]   = $(this).find("input[id*='email']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arraymail   = JSON.stringify(DATAr);

		$.ajax({
            type:'POST',
            url: base_url+"index.php/Clientes/updatedatos",
            data: {
            	clienteId : $('#clienteId').val(),
				razon_social : $('#razon_social').val(),
				rfc : $('#rfc').val(),
                cp : $('#cp').val(),
                RegimenFiscalReceptor:$('#RegimenFiscalReceptor option:selected').val(),
				direccion : $('#direccion').val(),
				correos:arraymail,
                banco : $('#banco').val(),
                numero_cuenta : $('#numero_cuenta').val(),
                clabe : $('#clabe').val(),
                uso_cfdi:$('#uso_cfdi option:selected').val(),

            },
            success:function(data){  
               Swal.fire({
		                position: "Éxito",
		                icon: "success",
		                title: "Éxito!",
		                text:'Se ha actualizado correctamente',
		                showConfirmButton: false,
		                timer: 1500
		            });
                setInterval(function(){ 
                        	location.href=base_url+'Clientes';
                }, 3000);
            }
        });

	});
});
function checkdireccion(){
	var chedireccion = $('#check_direccion').is(':checked');
	if(chedireccion==true){
		$('.direccion').show('show');
	}else{
		$('.direccion').hide('show');
	}
}
function checkbanco(){
    var chebanco = $('#check_datos_bancario').is(':checked');
    if(chebanco==true){
        $('.datosbancarios').show('show');
    }else{
        $('.datosbancarios').hide('show');
    }
}




var rowemail=0;
function addemail(id,email){
	var htmlval='<tr class="tableemailtb_tr_'+rowemail+'">\
                    <td>\
                        <div class="row">\
                            <label align="right" class="col-3" style="color: #1770aa;font-weight: 600;">Correo electrónico</label>\
                            <div class="col-8">\
                                <input class="form-control" type="hidden" id="climailId" value="'+id+'">\
                                <input class="form-control" type="email" id="email" value="'+email+'">\
                            </div>\
                            <div class="col-1">\
                                <a class="btn btn-icon btn_light_morado btn-circle mr-2" onclick="deletemail('+rowemail+','+id+')">\
                                    <i class="icon-xl fas fa-minus white"></i>\
                                </a>\
                            </div>\
                        </div>\
                    </td>\
                </tr>';
    $('.tableemailtb').append(htmlval);
    rowemail++;
}
function obtenerdatos(id){

    $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/obtenerdatoscli",
        data: {
        	clienteId:id
        },
        success: function (data){
            var array = $.parseJSON(data); 
            console.log(array.direccion.length);
            $('#clienteId').val(array.clienteId);
			$('#razon_social').val(array.razon_social);
			$('#rfc').val(array.rfc);
            $('#cp').val(array.cp);
            $('#RegimenFiscalReceptor').val(array.RegimenFiscalReceptor);
            $('#uso_cfdi').val(array.uso_cfdi).change();
			$('#direccion').val(array.direccion);
            array.emails.forEach(function(element) {
            	addemail(element.climailId,element.mail);
            });
            if(array.direccion.length>2){
            	$('#check_direccion').prop('checked', true);
            	$('.direccion').show('show');
            }
            if(array.banco.length>0){
                $('#check_datos_bancario').prop('checked', true);
                $('.datosbancarios').show('show');
            }
            $('#banco').val(array.banco);
            $('#numero_cuenta').val(array.numero_cuenta);
            $('#clabe').val(array.clabe);
            v_rf();
        },
        error: function(data){ 
            //alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });

}
function deletemail(row,id){
	if(id==0){
		$('.tableemailtb_tr_'+row).remove();
	}else{
		$.confirm({
	        boxWidth: '30%',
	        useBootstrap: false,
	        icon: 'fa fa-warning',
	        title: 'Atención!',
	        content: '¿Está seguro de eliminarlo?',
	        type: 'red',
	        typeAnimated: true,
	        buttons:{
	            confirmar: function (){
	                $.ajax({
	                    type:'POST',
	                    url: base_url+"index.php/Clientes/deletemail",
	                    data: {
	                        id:id
	                    },
	                    success:function(response){  
	                        $('.tableemailtb_tr_'+row).remove();

	                    }
	                });
	            },
	            cancelar: function () 
	            {
	                
	            }
	        }
	    });
	}
}
function validarrfc(input) {
    var rfc = input.value.toUpperCase();

    if (rfcValido1(rfc)) { // ⬅️ Acá se comprueba
      //rfcexiste(rfc);
      toastr["success"](rfc+" válido", "Correcta!");
    } else {
      toastr["error"](rfc+" No válido", "Alerta!");
    }
        
}
function rfcValido1(rfcStr) {
    
    $('.help-block').remove();
    
    var strCorrecta;
    
    strCorrecta = rfcStr;
        
    if (rfcStr.length == 12){
       
       //var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
       var valid = '^(([A-Z,Ñ,&]|[a-z,ñ,&]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    
    } else {
       
       var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }
    
    var validRfc=new RegExp(valid);
    
    var matchArray=strCorrecta.match(validRfc);
    
    if (matchArray==null) {        
        return false;        
    } else {
        return true;
    }
}
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "4000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function v_rf(){
    $('.pararf').prop( "disabled", true );
    var rf=$('#RegimenFiscalReceptor option:selected').val();
    console.log('regimen:'+rf);
    $( "#uso_cfdi ."+rf ).prop( "disabled", false )
}