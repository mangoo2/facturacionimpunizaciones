var base_url = $('#base_url').val();
$(document).ready(function() {
    //$("#cantidad_pro").focus();
    /*$( "#idclientes" ).keypress(function(e) {
        if (e.which==13) {
            clientes_buscar(1);
            setTimeout(function(){ 
                $('#idclientes').val('');
                $('.cliente_buscar_t').html('');
            }, 500);
        }else{
            setTimeout(function(){ 
                clientes_buscar(0);
            }, 900);
            
        }
    });*/

    $(document).keydown(function(e) {
        //console.log(e.which)
        switch(e.which) { 
            case 38:
                navigate('up');
            break;
            case 40:
                navigate('down');
            break;
            case 13:
                navigate('enter');
            break;             
        }
    });

});

function clientes_buscar(tipo){
    var clientes=$('#idclientes').val();
    if(clientes!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Inicio/buscar_cliente',
            data:{
                clientes:clientes
                },
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){ 
                console.log(data); 
                
                var array = $.parseJSON(data);
                if (tipo==0) {
                    $('.cliente_buscar_t').html('');
                    array.forEach(function(item) {
                        var html='<a data-razon_social="'+item.razon_social+'" \
                                    onclick="get_factura('+item.clienteId+')" \
                                    class="btn btn-primary cli_'+item.clienteId+' proselected" style="width: 100%; color:white; border-color: white !important;">'+item.razon_social+'</a>';
                        $('.cliente_buscar_t').append(html);
                    });
                    proselected_length=$('.proselected').length;
                    proselected=-1;

                }else{
                    if (array.length==1) {
                        array.forEach(function(item) {
                            get_factura(item.clienteId);
                        });
                    }else{

                    }
                }
            }
        });
    }else{
        setTimeout(function(){ 
            $('.cliente_buscar_t').html('');
        }, 1000);
    }
}

function navigate(direction) {
    if(proselected_length>0){
        if(direction == 'up') {//subir
            $(".proselected").removeClass('selected');
            if (proselected==0 ||proselected==-1) {
                proselected=0;
            }else{
                proselected--;
            }
            
        }else if (direction == 'down') {//bajar
            $(".proselected").removeClass('selected');
            proselected++;
            if(proselected>=proselected_length){
                proselected=(proselected_length-1);
            }
        }else if (direction == 'enter') {
            $(".proselected.selected").click();
        }
    }
    
    //console.log(proselected);
    var valor=$(".proselected").eq(proselected).addClass('selected');
    //console.log(valor);
}

function campo_vacio(){
    var idclientes = $('#idclientes').val();
    if(idclientes==''){
        $('.cliente_buscar_t').html('');   
    }
}

function get_factura(cliente){
    var razon_social=$('.cli_'+cliente).data('razon_social');
    window.location = base_url+'Folios?id='+cliente+'&name='+razon_social+'';
}

