Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            32:30:30:30:31:30:30:30:30:30:30:33:30:30:30:32:32:38:32:33
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN=A.C. Pruebas, O=Pruebas, C=MX, L=Cuauhtemoc
        Validity
            Not Before: Oct 11 22:47:28 2022 GMT
            Not After : Apr  2 22:47:28 2028 GMT
        Subject: CN=Pablo Neruda Perez/name=Pablo Neruda Perez, O=Pablo Neruda Perez/x500UniqueIdentifier=TEST010203001 / TEST10317A46, OU=CENTRAL
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:b9:a5:bb:13:4f:36:8d:24:fd:27:52:3e:08:68:
                    b0:86:f9:73:a1:60:74:3b:41:b6:d4:17:ff:f1:cf:
                    c2:c6:bc:59:16:b8:f6:f7:90:e5:3b:d1:56:e9:df:
                    1a:c8:91:5b:8f:0b:58:e6:f1:1a:2b:78:af:25:97:
                    90:7b:7c:d2:9e:59:c8:02:8d:c3:fe:1a:18:ee:70:
                    95:2f:51:bc:be:7e:56:54:3f:53:a4:43:31:50:9b:
                    ef:7a:1e:49:33:a2:0a:06:e8:58:7c:2a:d3:52:eb:
                    3d:75:5d:22:b4:d8:fa:0e:b3:95:89:d7:90:7d:e4:
                    2e:93:4c:18:49:90:78:ac:c6:90:4f:8b:62:88:17:
                    eb:67:cb:bd:dc:19:d5:46:3b:78:d2:ea:1f:c3:16:
                    c7:24:bf:0e:62:8a:3b:06:b1:75:6d:1d:36:38:56:
                    4c:ea:fa:2b:c0:10:e9:48:05:03:88:fc:b2:5c:17:
                    e6:07:aa:32:37:11:be:43:76:b4:7c:71:8d:d3:6a:
                    25:73:aa:34:1a:b1:73:fc:b0:ca:74:aa:fe:5c:cd:
                    67:47:b3:9b:02:71:93:41:72:4e:46:95:6b:c8:47:
                    66:cb:d1:5c:f3:7c:05:38:9a:16:48:1d:c3:ea:06:
                    ba:9e:d3:18:c0:97:fe:b3:03:d3:6f:db:7e:1d:c5:
                    13:bb
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            X509v3 Key Usage: 
                Digital Signature, Non Repudiation
    Signature Algorithm: sha256WithRSAEncryption
         72:f6:ce:b5:cf:1b:22:af:2e:a2:cc:95:af:3d:eb:59:47:48:
         77:69:04:c9:a0:0d:6e:17:c7:50:52:58:cc:4d:8f:5e:6e:2a:
         2a:47:f9:f3:ac:d3:55:55:fe:4a:75:d7:cd:3b:de:4e:5e:01:
         14:39:df:a9:b9:c2:4b:19:99:9f:db:99:b3:f2:36:a5:92:4f:
         c6:a4:74:51:49:26:60:79:52:92:59:4d:ab:b9:0f:69:cb:c5:
         6c:8c:ca:96:f0:12:d6:3d:f7:d3:7c:31:ff:51:47:72:be:10:
         9b:da:df:f7:79:e6:7b:0a:27:f7:c9:dc:66:f3:0d:a1:3c:00:
         66:ae:59:99:41:f2:36:39:e1:b2:e9:92:95:2c:f7:e2:88:c1:
         41:39:88:45:0f:82:1f:6a:e8:8c:ab:aa:d0:1c:9a:5b:aa:6f:
         4d:04:c1:1c:57:71:36:dc:e4:eb:64:76:8c:ba:0e:d4:8b:47:
         52:d2:57:8b:6c:55:6f:2a:f6:66:b2:07:40:0b:d0:25:71:d2:
         11:54:36:70:25:63:96:e4:3e:ee:06:5a:45:ed:d8:8f:66:ed:
         b5:0e:83:bd:e7:eb:ec:59:bc:ef:e9:4c:fd:b9:4f:a7:a4:4f:
         f6:f9:1b:9f:6c:23:18:9b:15:d5:02:81:eb:fd:b6:80:15:64:
         fc:a1:3e:f0
