
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar concepto" id="buscar_registro" oninput="reload_registro()">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4"></div>   
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="tabla_datos">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Clave</th>
                                        <th scope="col">Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>