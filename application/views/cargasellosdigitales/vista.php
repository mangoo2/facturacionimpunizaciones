


<!--begin::Content-->

<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-lg-10" align="right"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>
                </div>   

            </div>  
            <br> 
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="card-body">
                    	<h2>Instalación de certificado de sello digital</h2>
                    	<hr>
                    	<div class="row">  
                            <div class="col-lg-6"> 
                                <input type="file" id="cerdigital" name="cerdigital" class="form-control-bmz" required >
                            </div>
                            <div class="col-lg-6"> 
                                <input type="file" id="claveprivada" name="claveprivada" class="form-control-bmz" required>
                                <div class="row" style="margin-top: 5px;">
                                  <label for="passprivada" class="active col s3" style="color: black; font-size: 14px;">Contraseña de clave privada:</label>
                                  <div class="col s9">
                                    <input type="password" id="passprivada" name="passprivada" class="form-control" required>          
                                  </div>
                                </div>
                            </div>
                        </div>    
                                                      	
                        
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-4"></div>
                            <div class="col-4">
                                <button  class="btn btn-primary" onclick="instalararchivos()" >Guardar archivos</button>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>