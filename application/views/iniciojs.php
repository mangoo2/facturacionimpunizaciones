<!--<script src="<?php //echo base_url() ?>assets/js/pages/features/charts/apexcharts.js"></script>-->
<script src="<?php echo base_url() ?>public/js/inicio.js?v=<?php echo date('Ymd')?>"></script>

<?php /*
<script type="text/javascript">
	"use strict";

// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';


var KTApexChartsDemo = function () {

	var _demo2 = function () {
		const apexChart = "#chart_2";
		var options = {
			series: [{
				name: "<?php echo $ano_anterior;?>",
				data: [<?php echo $valorEnerop.','.$valorFebrerop.','.$valorMarzop.','.$valorAbrilp.','.$valorMayop.','.$valorJuniop.','.$valorJuliop.','.$valorAgostop.','.$valorSeptiembrep.','.$valorOctubrep.','.$valorNomviembrep.','.$valorDiciembrep;?>]
			}, {
				name: "<?php echo $ano_actual;?>",
				data: [<?php echo $valorEnero.','.$valorFebrero.','.$valorMarzo.','.$valorAbril.','.$valorMayo.','.$valorJunio.','.$valorJulio.','.$valorAgosto.','.$valorSeptiembre.','.$valorOctubre.','.$valorNomviembre.','.$valorDiciembre;?>]
			}],
			chart: {
				height: 350,
				type: 'area'
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				curve: 'smooth'
			},
			xaxis: {
				type: 'text',
				categories: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","octubre","Noviembre","Diciembre"]
			},
			tooltip: {
				x: {
					format: 'dd/MM/yy HH:mm'
				},
			},
			colors: [primary, success]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}
	/*
	var _demo12 = function () {
		const apexChart = "#chart_12";
		var options = {
			series: [44, 55, 13, 43, 22],
			chart: {
				width: 380,
				type: 'pie',
			},
			labels: ['Team A', 'Team B', 'Team C', 'Team D', 'Team E'],
			responsive: [{
				breakpoint: 480,
				options: {
					chart: {
						width: 200
					},
					legend: {
						position: 'bottom'
					}
				}
			}],
			colors: [primary, success, warning, danger, info]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}
	*/
/*

	return {
		// public functions
		init: function () {
			_demo2();
			//_demo12();
		}
	};
}();

jQuery(document).ready(function () {
	KTApexChartsDemo.init();
});

</script>
*/
?>