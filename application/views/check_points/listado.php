<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-outline-info"  onclick="modal_registro()"><i class="fas fa-flag-checkered"></i> Nuevo check point</button> 
                                    </div>
                                    <div class="col-md-6">    
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar check points &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="check_points" type="text" class="form-control" placeholder="Escriba nombre del check points" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12"></div>
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>ID</th> 
                                                    <th>Check points</th>
                                                    <th>Código</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>    
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>            

<div id="registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Registro de check points</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form" method="post" role="form" id="form_registro">
                                    <input type="hidden" name="id" id="id_check_points" value="0"> 
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Nombre de check point</label>
                                                <input type="text" name="nombre" id="nombre" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Código</label>
                                                <input type="text" name="codigo" id="codigo" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect btn_registro" onclick="guarda_registro()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste check point? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_points">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="registro_delete()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>
