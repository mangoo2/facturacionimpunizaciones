<style type="text/css">
    .error{
        margin: 0px;
        color: red;
    }
</style>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-lg-12"> 
            <!--begin::Dashboard-->
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <form id="form-configuraciones" action="">
                            <input type="hidden" name="ConfiguracionesId" id="ConfiguracionesId" value="<?php echo $result->ConfiguracionesId;?>">
                            <div class="card-body">
                                <h2>Información General</h2>
                                <hr>
                                <div class="row">    
                                    <div class="col-lg-2">
                                        <h4>Logo</h4>
                                        <div class="image-input image-input-outline" id="kt_image_1">
                                            <?php 
                                                if($result->logotipo==''){
                                                    $urlimg=base_url().'public/img/logo_grapas.png';
                                                }else{
                                                    $urlimg=base_url().'public/img/'.$result->logotipo;
                                                }
                                            ?>
                                            <div class="image-input-wrapper" style="background-image: url(<?php echo $urlimg;?>)"></div>
                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                <input type="file" name="profile_avatar" id="profile_avatar" accept=".png, .jpg, .jpeg" />
                                                <input type="hidden" name="profile_avatar_remove" id="profile_avatar_remove" />
                                            </label>
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                        </div>
                                        <br>
                                        <br>
                                    </div>    
                                    <div class="col-lg-10">
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label" align="right">Razón social</label>
                                            <div class="col-4">
                                                <input class="form-control" type="text" name="Nombre" id="Nombre" value="<?php echo $result->Nombre;?>" required>
                                            </div>
                                            <label class="col-2 col-form-label" align="right">CURP</label>
                                            <div class="col-4">
                                                <input class="form-control" type="text" name="Curp" id="Curp" value="<?php echo $result->Curp;?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label" align="right">RFC</label>
                                            <div class="col-4">
                                                <input class="form-control" type="text" name="Rfc" id="Rfc" value="<?php echo $result->Rfc;?>" required>
                                            </div>
                                            <label class="col-2 col-form-label" align="right">Régimen fiscal</label>
                                            <div class="col-4">
                                                <select id="Regimen" name="Regimen" class="form-control">';
                                                    <option value="601" 
                                                            <?php if($result->Regimen=='601'){ echo 'selected';} ?>>601 General de Ley Personas Morales</option>
                                                    <option value="603" 
                                                            <?php if($result->Regimen=='603'){ echo 'selected';} ?>>603 Personas Morales con Fines no Lucrativos</option>
                                                    <option value="605" 
                                                            <?php if($result->Regimen=='605'){ echo 'selected';} ?>>605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                                                    <option value="606" 
                                                            <?php if($result->Regimen=='606'){ echo 'selected';} ?>>606 Arrendamiento</option>
                                                    <option value="607" 
                                                            <?php if($result->Regimen=='607'){ echo 'selected';} ?>>607 Régimen de Enajenación o Adquisición de Bienes</option>
                                                    <option value="608" 
                                                            <?php if($result->Regimen=='608'){ echo 'selected';} ?>>608 Demás ingresos</option>
                                                    <option value="609" 
                                                            <?php if($result->Regimen=='609'){ echo 'selected';} ?>>609 Consolidación</option>
                                                    <option value="610" 
                                                            <?php if($result->Regimen=='610'){ echo 'selected';} ?>>610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>
                                                    <option value="611" 
                                                            <?php if($result->Regimen=='611'){ echo 'selected';} ?>>611 Ingresos por Dividendos (socios y accionistas)</option>
                                                    <option value="612" 
                                                            <?php if($result->Regimen=='612'){ echo 'selected';} ?>>612 Personas Físicas con Actividades Empresariales y Profesionales</option>
                                                    <option value="614" 
                                                            <?php if($result->Regimen=='614'){ echo 'selected';} ?>>614 Ingresos por intereses</option>
                                                    <option value="615" 
                                                            <?php if($result->Regimen=='615'){ echo 'selected';} ?>>615 Régimen de los ingresos por obtención de premios</option>
                                                    <option value="616" 
                                                            <?php if($result->Regimen=='616'){ echo 'selected';} ?>>616 Sin obligaciones fiscales</option>
                                                    <option value="620" 
                                                            <?php if($result->Regimen=='620'){ echo 'selected';} ?>>620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>
                                                    <option value="621" 
                                                            <?php if($result->Regimen=='621'){ echo 'selected';} ?>>621 Incorporación Fiscal</option>
                                                    <option value="622" 
                                                            <?php if($result->Regimen=='622'){ echo 'selected';} ?>>622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                                                    <option value="623" 
                                                            <?php if($result->Regimen=='622'){ echo 'selected';} ?>>623 Opcional para Grupos de Sociedades</option>
                                                    <option value="624" 
                                                            <?php if($result->Regimen=='624'){ echo 'selected';} ?>>624 Coordinados</option>
                                                    <option value="628" 
                                                            <?php if($result->Regimen=='628'){ echo 'selected';} ?>>628 Hidrocarburos</option>
                                                    <option value="629" 
                                                            <?php if($result->Regimen=='629'){ echo 'selected';} ?>>629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>
                                                    <option value="630" 
                                                            <?php if($result->Regimen=='630'){ echo 'selected';} ?>>630 Enajenación de acciones en bolsa de valores</option>      
                                                  </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                                
                                <h2>Domicilio Fiscal</h2>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" align="right">Calle</label>
                                    <div class="col-4">
                                        <input class="form-control" type="text" name="Calle" id="Calle" value="<?php echo $result->Calle;?>" required>
                                    </div>
                                    <label class="col-2 col-form-label" align="right">Localidad</label>
                                    <div class="col-4">
                                        <input class="form-control" type="text" name="localidad" id="localidad" value="<?php echo $result->localidad;?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" align="right">No Ext / Int</label>
                                    <div class="col-2">
                                        <input class="form-control" type="text" placeholder="Exterior" name="Noexterior" id="Noexterior" value="<?php echo $result->Noexterior;?>" required>
                                    </div>
                                    <div class="col-2">
                                        <input class="form-control" type="text" placeholder="Interior" placeholder="Exterior" name="Nointerior" id="Nointerior" value="<?php echo $result->Nointerior;?>">
                                    </div>
                                    <label class="col-2 col-form-label" align="right">Municipio</label>
                                    <div class="col-4">
                                        <input class="form-control" type="text" name="Municipio" id="Municipio" value="<?php echo $result->Municipio;?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" align="right">Colonia</label>
                                    <div class="col-4">
                                        <input class="form-control" type="text" name="Colonia" id="Colonia" value="<?php echo $result->Colonia;?>" required>
                                    </div>
                                    <label class="col-2 col-form-label" align="right" >Estado</label>
                                    <div class="col-4">
                                        <input class="form-control" type="text" name="Estado" id="Estado" value="<?php echo $result->Estado;?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" align="right">Código postal</label>
                                    <div class="col-4">
                                        <input class="form-control" type="text" name="CodigoPostal" id="CodigoPostal" value="<?php echo $result->CodigoPostal;?>" required>
                                    </div>
                                    <label class="col-2 col-form-label" align="right">País</label>
                                    <div class="col-4">
                                        <input class="form-control" type="text" name="PaisExpedicion" id="PaisExpedicion" value="<?php echo $result->PaisExpedicion;?>" required>
                                    </div>
                                </div>
                                <h2>Datos de Correo</h2>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" align="right">E-mail</label>
                                    <div class="col-4">
                                        <input class="form-control" type="email" name="Email" id="Email" value="<?php echo $result->Email;?>">
                                    </div>
                                    <label class="col-2 col-form-label" align="right">Asunto</label>
                                    <div class="col-4">
                                        <input class="form-control" type="text" name="Asunto" id="Asunto" value="<?php echo $result->Asunto;?>">
                                    </div>
                                </div>
                                <h2>Cuerpo de Correo</h2>
                                <hr>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <textarea id="kt-ckeditor-5" name="cuerpo" id="cuerpo"><?php echo $result->cuerpo;?></textarea>
                                    </div>
                                </div>

                                <h2>Enviar copia de factura</h2>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" align="right">E-mail</label>
                                    <div class="col-10">
                                        <input class="form-control" type="email" name="Email2" id="Email2" value="<?php echo $result->Email2;?>">
                                    </div>
                                </div>    
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-4" align="center">
                                        <button type="submit" class="btn btn-primary">Guardar Información</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>                
                </div>
            </div>    
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>