
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar usuario" id="empleado_busqueda" oninput="reload_registro()">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-5"></div>  
                <div class="col-lg-3" align="right"> 
                    <a href="<?php echo base_url() ?>Usuarios/registro" class="btn btn-primary">Nuevo usuario</a>
                </div>    
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="table_datos">
                                <thead>
                                    <tr>
                                        <th scope="col">Empleado</th>
                                        <th scope="col">Usuario del sistema</th>
                                        <th scope="col">Tipo</th>
                                        <th scope="col">Motivo de suspensión</th>
                                        <th scope="col">Bitácora de movimientos</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="elimina_registro_modal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmación </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste registro? Esta operación no se puede deshacer.</h5>
            </div>
            <div class="modal-footer">
                <a class="btn btn_light_morado font-weight-bold btn-pill" onclick="delete_registro()">Aceptar</a> 
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="suspender_registro_modal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Suspención </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea suspender éste registro? Esta operación no se puede deshacer.</h5>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Motivo</label>
                            <textarea class="form-control" rows="5" id="motivo"></textarea>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <a class="btn btn_light_morado font-weight-bold btn-pill" onclick="update_registro()">Aceptar</a> 
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="motivo_registro_modal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Motivo de suspensión</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h4>Fecha: <span class="txt_fecha"></span></h4>
                <hr>
                <h4>Motivo</h4>
                <h6 id="text_motivo"></h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>