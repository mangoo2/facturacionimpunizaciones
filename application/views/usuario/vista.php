<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-lg-10" align="right"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Usuarios" class="btn btn-primary">Regresar</a>
                </div>    
            </div>  
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2">
                                <h4>Foto</h4>
                                <div class="image-input image-input-outline" id="kt_image_1">
                                    <?php if($foto==''){ ?>
                                        <div class="image-input-wrapper" style="background-image: url(<?php echo base_url() ?>images/avatar.png)"></div>
                                    <?php }else { ?>
                                        <div class="image-input-wrapper" style="background-image: url(<?php echo base_url() ?>uploads/personal/<?php echo $foto ?>)"></div>
                                    <?php }?>    
                                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="profile_avatar" id="foto_avatar" accept=".png, .jpg, .jpeg" />
                                        <input type="hidden" name="profile_avatar_remove" />
                                    </label>
                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-10">
                                <form id="form_registro" method="post">
                                    <input type="hidden" name="personalId" value="<?php echo $personalId ?>">
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label" align="right">Nombre del empleado</label>
                                        <div class="col-9">
                                            <input class="form-control" type="text" name="nombre" value="<?php echo $nombre ?>">
                                        </div>
                                    </div>
                                    <input type="hidden" name="UsuarioID" value="<?php echo $UsuarioID ?>">
                                    <div class="form-group row">
                                        <label for="example-search-input" align="right" class="col-3 col-form-label">Rol</label>
                                        <div class="col-4">
                                            <select class="form-control" name="perfilId">
                                                <?php foreach ($perfil as $x){ ?>
                                                    <option value="<?php echo $x->perfilId ?>" <?php if($x->perfilId==$perfilId) echo 'selected' ?>><?php echo $x->nombre ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label align="right" class="col-3 col-form-label">Nombre de usuario</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="Usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario()" value="<?php echo $Usuario ?>">
                                        </div>   
                                    </div>
                                    <div class="form-group row">
                                        <label align="right" class="col-3 col-form-label">Contraseña</label>
                                        <div class="col-4">
                                            <input class="form-control" type="password" name="contrasena" id="contrasena" autocomplete="new-password" oninput="" value="<?php echo $contrasena ?>">
                                        </div>  
                                        <div class="col-5">
                                            <a onclick="clickoverpass()" class="btn btn-icon btn-circle btn_light_morado">
                                                <i class="icon-xl fas fa-eye" style="color: white"></i>
                                            </a>
                                        </div> 
                                    </div>
                                    <div class="form-group row">
                                        <label align="right" class="col-3 col-form-label">Confirmar contraseña</label>
                                        <div class="col-4">
                                            <input class="form-control" type="password" name="contrasena2" id="contrasena2" autocomplete="new-password" value="<?php echo $contrasena2 ?>">
                                        </div>   
                                    </div>
                            </div>  
                        </div>    
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-4"></div>
                            <div class="col-4">
                                <button class="btn btn-primary btn_registro" onclick="guardar_registro()">Guardar empleado</button>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>