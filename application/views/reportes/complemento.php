<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';
$this->load->helper('url');
$GLOBALS["idcomplemento"]=$idcomplemento;
$GLOBALS["logotipo"]=$logotipo;
$GLOBALS["rfcemisor"]=$rfcemisor;
$GLOBALS["nombreemisor"]=$nombreemisor;
$GLOBALS["rdireccion"]=$rdireccion;

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Complemento '.$idcomplemento);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);



// set margins
$pdf->SetMargins('6', '9', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);// margen del footer

$pdf->SetFont('dejavusans', '', 12);
$pdf->AddPage();
      $logos = $GLOBALS["logotipo"];
      $logos2 = $GLOBALS["logotipo"];
$styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
          $params = $pdf->serializeTCPDFtagParameters(array($uuid, 'QRCODE,L', '', '', 45, 45, $styleQR, 'N'));
$html='<style type="text/css">
            .info_fac{
              font-size: 9px;
            }
            .info_facd{
              font-size: 8px;
            }
            .httablelinea{
              vertical-align: center;
              border-bottom: 1px solid #9e9e9e;
            }
            
            .httableleft{
              border-left: 1px solid #9e9e9e;
            }
            .httableright{
              border-right: 1px solid #9e9e9e;
            }
            .httabletop{
              border-top: 1px solid #9e9e9e;
            }
          </style>
          <style type="text/css">
        .httable{
          vertical-align: center;
          border-bottom: 2px solid #9e9e9e;
          border-top: 1px solid #9e9e9e;

        }
        .httablepro{
            vertical-align: center;
            border-bottom: 1px solid #9e9e9e;
          }
        .magintablepro{
            margin-top:0px;
            margin-bottom:0px;
            margin: 0px;
        }
        .tamaniofuente{
          font-size:7.5px;
        }
        .tamaniofuente2{
          font-size:6px;
        }
        .colorfondo{
          background-color: #e2e0e0;
        }
      </style>
      <table width="100%" border="0" cellpadding="4px" class="info_fac">
            <tr >
              <td width="23%" heig><img src="'.$logos.'"></td>
              <td width="54%"  align="center" class="httablepro">
                <span style="margin:0px; font-size:15px;">Nombre o razon social '.$GLOBALS["nombreemisor"].'</span><br>
                <span style="margin:0px; font-size:11px;"><b>Domicilio fiscal:</b></span><br>
                <span style="margin:0px; font-size:11px;">'.$GLOBALS["rdireccion"].'</span>
                <span style="margin:0px; font-size:11px;">RFC: '.$GLOBALS["rfcemisor"].'</span><br><br>
                <span style="margin:0px; font-size:11px;">Tel: 00000000</span>
              </td>
              <td width="23%"><img src="'.$logos2.'" width="150px"></td>
            </tr>
      </table>';
      $html.='<table border="0" width="100%"><tr><td></td></tr></table>
      <table width="100%" border="0" cellpadding="4px" class="info_fac">
        <tr>
          <td width="17%" class="tamaniofuente"><b>RFC emisor:</b></td>
          <td width="29%" class="tamaniofuente">'.$rfcemisor.'</td>
          <td width="21%" class="tamaniofuente"><b>Folio fiscal:</b></td>
          <td width="33%" class="tamaniofuente">'.$uuid.'</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b>Nombre emisor:</b></td>
          <td class="tamaniofuente">'.$nombreemisor.'</td>
          <td class="tamaniofuente"><b>No. de serie del CSD:</b></td>
          <td class="tamaniofuente">'.$nocertificadosat.'</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b>RFC receptor:</b></td>
          <td class="tamaniofuente">'.$rfcreceptor.'</td>
          <td class="tamaniofuente"><b>Código postal,fecha y hora de emision:</b></td>
          <td class="tamaniofuente">'.$LugarExpedicion.' '.$fechatimbre.'</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b>Nombre receptor:</b></td>
          <td class="tamaniofuente">'.$nombrereceptorr.'</td>
          <td class="tamaniofuente"><b>Efecto de comprobante:</b></td>
          <td class="tamaniofuente">Pago</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b>Uso CFDI:</b></td>
          <td class="tamaniofuente">Por definir</td>
          <td class="tamaniofuente"><b>Régimen fiscal:</b></td>
          <td class="tamaniofuente">'.$regimenf.'</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b>Numero de Operación:</b></td>
          <td class="tamaniofuente">'.$NumOperacion.'</td>
          <td class="tamaniofuente"></td>
          <td class="tamaniofuente"> </td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="4px" class="info_fac">
        <tr>
          <td style="font-size:10px;"><b>Conceptos</b></td>
        </tr>
      </table>
      <table width="100%" border="1" cellpadding="4px" class="info_fac" align="center">
        <tr>
          <td class="tamaniofuente2 colorfondo"><b>Clave del producto Y/o servicio</b></td>
          <td class="tamaniofuente2 colorfondo"><b>No. identificacion</b></td>
          <td class="tamaniofuente2 colorfondo"><b>Cantidad</b></td>
          <td class="tamaniofuente2 colorfondo"><b>Clave de unidad</b></td>
          <td class="tamaniofuente2 colorfondo"><b>Unidad</b></td>
          <td class="tamaniofuente2 colorfondo"><b>Valor unitario</b></td>
          <td class="tamaniofuente2 colorfondo"><b>Importe</b></td>
          <td class="tamaniofuente2 colorfondo"><b>Descuento</b></td>
          <td class="tamaniofuente2 colorfondo"><b>No. de pedimento</b></td>
          <td class="tamaniofuente2 colorfondo"><b>No. de cuenta predial</b></td>
        </tr>
        <tr>
          <td class="tamaniofuente2">84111506</td>
          <td class="tamaniofuente2"></td>
          <td class="tamaniofuente2">1</td>
          <td class="tamaniofuente2">ACT</td>
          <td class="tamaniofuente2"></td>
          <td class="tamaniofuente2">0</td>
          <td class="tamaniofuente2">0</td>
          <td class="tamaniofuente2"></td>
          <td class="tamaniofuente2"></td>
          <td class="tamaniofuente2"></td>
        </tr>
        <tr>
          <td class="tamaniofuente2 colorfondo"><b>Descripción</b></td>
          <td class="tamaniofuente2" colspan="9" align="left">Pago</td>
        </tr>
      </table>
      <table border="0" width="100%"><tr><td></td></tr></table>
      <!--<table border="0" width="100%">
        <tr>
          <td class="tamaniofuente" width="16%"></td>
          <td class="tamaniofuente" width="34%"></td>
          <td class="tamaniofuente" width="16%"><b>Subtotal</b></td>
          <td class="tamaniofuente" width="34%">$ 0.00</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td class="tamaniofuente"><b>Total</b></td>
          <td class="tamaniofuente"><b>$ 0.00</b></td>
        </tr>
      </table>-->
      <table border="0" width="100%"><tr><td></td></tr></table>
      <table border="0" width="100%"><tr><td style="font-size:10px;"><b>Información del pago</b></td></tr></table>
      <table border="0" width="100%">
        <tr>
          <td class="tamaniofuente"><b>Forma de pago:</b></td>
          <td class="tamaniofuente">'.$formapago.'</td>
          <td class="tamaniofuente"><b>Fecha de pago:</b></td>
          <td class="tamaniofuente">'.$fechapago.'</td>
        </tr>
        <tr>
          <td class="tamaniofuente" colspan="2" rowspan="2"></td>
          <td class="tamaniofuente"><b>Moneda de pago:</b></td>
          <td class="tamaniofuente">MXN Peso Mexicano</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b>Monto:</b></td>
          <td class="tamaniofuente">'.$monto.'</td>
        </tr>
      </table>
      <table border="0" width="100%"><tr><td></td></tr></table>
      <table border="0" width="100%"><tr><td style="font-size:10px;"><b>Documentos relacionados</b></td></tr></table>';
      
      foreach ($docrelac as $item) {
        if ($item->moneda=='pesos') {
            $moneda='MXN Peso Mexicano';
        }else{
            $moneda='Dolar';
        }
        $resultsmp=$this->ModeloCatalogos->getselectwheren('f_metodopago',array('metodopago'=>$item->FormaPago));
        $resultsmp=$resultsmp->row();
        $metodopago=$resultsmp->metodopago_text;
      
$html.='<table border="0" width="100%">
        <tr>
          <td class="tamaniofuente" width="17%" ><b>Id documento:</b></td>
          <td class="tamaniofuente" width="31%" >'.$item->IdDocumento.'</td>
          <td class="tamaniofuente" width="28%" ><b>Moneda del documento relacionado</b></td>
          <td class="tamaniofuente" width="24%" >'.$moneda.'</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b>Folio</b></td>
          <td class="tamaniofuente">'.$item->Folio.'</td>
          <td class="tamaniofuente"><b>Método de pago del documento relacionado:</b></td>
          <td class="tamaniofuente">'.$metodopago.'</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b>Serie:</b></td>
          <td class="tamaniofuente">'.$item->serie.'</td>
          <td class="tamaniofuente"><b>Importe de saldo anterior:</b></td>
          <td class="tamaniofuente">'.$item->ImpSaldoAnt.'</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b>Número parcialidad:</b></td>
          <td class="tamaniofuente">'.$item->NumParcialidad.'</td>
          <td class="tamaniofuente"><b>Importe pagado:</b></td>
          <td class="tamaniofuente">'.$item->ImpPagado.'</td>
        </tr>
        <tr>
          <td class="tamaniofuente"><b></b></td>
          <td class="tamaniofuente"></td>
          <td class="tamaniofuente"><b>Importe de saldo insoluto:</b></td>
          <td class="tamaniofuente">'.$item->ImpSaldoInsoluto.'</td>
        </tr>
      </table>';
      
    }
$html.='<table border="0" width="100%"><tr><td ></td></tr></table>
      <table border="0" width="100%" cellpadding="2" >
        <tr>
          <td rowspan="7"><tcpdf method="write2DBarcode" params="' . $params . '" /></td>
          <td colspan="4" style="font-size:6px;"><b>Sello digital del CFDI</b></td>
        </tr>
        <tr>
          <td colspan="4" style="font-size:6px;">'.$Sello.'</td>
        </tr>
        <tr>
          <td colspan="4" style="font-size:6px;"><b>Sello digital del SAT</b></td>
        </tr>
        <tr>
          <td colspan="4" style="font-size:6px;">'.$sellosat.'</td>
        </tr>
        <tr>
          <td colspan="4" style="font-size:6px;"><b>Cadena Original del complmento de certificación del SAT</b></td>
        </tr>
        <tr>
          <td colspan="4" style="font-size:6px;">'.$cadenaoriginal.'</td>
        </tr>
        <tr>
          <td style="font-size:6px;"><b>Fecha y hora de certificación:</b></td>
          <td style="font-size:6px;">'.$fechatimbre.'</td>
          <td style="font-size:6px;"><b>No. de serie del certificacio SAT</b></td>
          <td style="font-size:6px;">'.$nocertificadosat.'</td>
        </tr>
        
      </table>
      ';

$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->Output('Complemento_'.$idcomplemento.'.pdf', 'I');

$url=$_SERVER['DOCUMENT_ROOT'].'/hulesygrapas/hulesyg/facturaspdf/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
$pdf->Output($url.'Complemento_'.$idcomplemento.'.pdf', 'FI');

//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>