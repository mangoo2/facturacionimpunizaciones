<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

$GLOBALS["logotipo"]=$logotipo;

$GLOBALS["Folio"]=$Folio;
$GLOBALS["Nombrerasonsocial"]=$Nombrerasonsocial;
$GLOBALS["folio_fiscal"]=$folio_fiscal;
$GLOBALS["rrfc"]=$rrfc;
$GLOBALS["rdireccion"]=$rdireccion;
$GLOBALS["nocertificadosat"]=$nocertificadosat;
$GLOBALS['regimenf']=$regimenf;
$GLOBALS["certificado"]=$certificado;
$GLOBALS["cfdi"]=$cfdi;
$GLOBALS["fechatimbre"]=$fechatimbre;
$GLOBALS["Estado"]=1;//en dado caso de que se encuentre cancelado
$GLOBALS["cp"]=$cp;
$GLOBALS["cliente"]=$cliente;
$GLOBALS["clirfc"]=$clirfc;
$GLOBALS["clidireccion"]=$clidireccion.' C.P. '.$cp;
$GLOBALS["isr"]=$isr;
$GLOBALS['numordencompra']=$numordencompra;
$GLOBALS["ivaretenido"]=$ivaretenido;
$GLOBALS["cedular"]=$cedular;
$GLOBALS["cincoalmillarval"]=$cincoalmillarval;
$GLOBALS["outsourcing"]=$outsourcing;



if ($numproveedor!='') {
    $GLOBALS['numproveedor']=$numproveedor;
    $GLOBALS['numproveedorv']='block';
}else{
    $GLOBALS['numproveedor']='';
    $GLOBALS['numproveedorv']='none';
}

if ($Caducidad!='') {
    $GLOBALS['Caducidad']=$Caducidad;
    $GLOBALS['Caducidadv']='block';
}else{
    $GLOBALS['Caducidad']='';
    $GLOBALS['Caducidadv']='none';
}

if ($Lote!='') {
    $GLOBALS['Lote']=$Lote;
    $GLOBALS['Lotev']='block';
}else{
    $GLOBALS['Lote']='';
    $GLOBALS['Lotev']='none';
}

if ($numordencompra!='') {
    $GLOBALS['numordencompra']=$numordencompra;
    $GLOBALS['numordencomprav']='block';
}else{
    $GLOBALS['numordencompra']='';
    $GLOBALS['numordencomprav']='none';
}
$GLOBALS['observaciones']=$observaciones;
$GLOBALS["total"]=$total;
$GLOBALS["moneda"]=$moneda;
if ($moneda=='MXN') {
  $GLOBALS['abreviaturamoneda']='MXN';
}else{
  $GLOBALS['abreviaturamoneda']='USD';
}
$GLOBALS["subtotal"]=$subtotal;
$GLOBALS["iva"]=$iva;
$GLOBALS["tarjeta"]=$tarjeta;
$GLOBALS["selloemisor"]=$selloemisor;
$GLOBALS["sellosat"]=$sellosat;
$GLOBALS["cadenaoriginal"]=$cadenaoriginal;
if ($cadenaoriginal=='') {
  $serie='';
}else{
  $series=explode("|", $cadenaoriginal);
  $serie=$series[3];
}
$GLOBALS["serie"]=$serie;

$GLOBALS["FormaPago"]=$FormaPago;
$GLOBALS["FormaPagol"]=$FormaPagol;
$GLOBALS["MetodoPago"]=$MetodoPago;
$GLOBALS["MetodoPagol"] =$MetodoPagol; 
$GLOBALS["tipoComprobante"]=$tipoComprobante;
$GLOBALS["facturadetalles"]=$facturadetalles;
$GLOBALS["RegimenFiscalReceptor"]=$RegimenFiscalReceptor;
$GLOBALS["fechafac"]=$fechafac;
//====================================================
class MYPDF extends TCPDF {
  //===========================================================
      var $Void = ""; 
      var $SP = " "; 
      var $Dot = "."; 
      var $Zero = "0"; 
      var $Neg = "Menos";
      function ValorEnLetras($x, $Moneda ){ 
        $s=""; 
        $Ent=""; 
        $Frc=""; 
        $Signo=""; 
             
        if(floatVal($x) < 0) 
         $Signo = $this->Neg . " "; 
        else 
         $Signo = ""; 
         
        if(intval(number_format($x,2,'.','') )!=$x) //<- averiguar si tiene decimales 
          $s = number_format($x,2,'.',''); 
        else 
          $s = number_format($x,2,'.',''); 
            
        $Pto = strpos($s, $this->Dot); 
             
        if ($Pto === false) 
        { 
          $Ent = $s; 
          $Frc = $this->Void; 
        } 
        else 
        { 
          $Ent = substr($s, 0, $Pto ); 
          $Frc =  substr($s, $Pto+1); 
        } 

        if($Ent == $this->Zero || $Ent == $this->Void) 
           $s = "Cero "; 
        elseif( strlen($Ent) > 7) 
        { 
           $s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
                 "Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
        } 
        else 
        { 
          $s = $this->SubValLetra(intval($Ent)); 
        } 

        if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
           $s = $s . "de "; 

        $s = $s . $Moneda; 
        if ($Moneda=='MXN') {
          $abreviaturamoneda='M.N.';
        }else{
          $abreviaturamoneda='U.S.D';
        }

        if($Frc != $this->Void) 
        { 
           $s = $s . " " . $Frc. "/100"; 
           //$s = $s . " " . $Frc . "/100"; 
        } 
        $letrass=$Signo . $s . " ".$abreviaturamoneda; 
        return ($Signo . $s . " ".$abreviaturamoneda);    
      } 
      function SubValLetra($numero) { 
          $Ptr=""; 
          $n=0; 
          $i=0; 
          $x =""; 
          $Rtn =""; 
          $Tem =""; 

          $x = trim("$numero"); 
          $n = strlen($x); 

          $Tem = $this->Void; 
          $i = $n; 
           
          while( $i > 0) 
          { 
             $Tem = $this->Parte(intval(substr($x, $n - $i, 1).  
                                 str_repeat($this->Zero, $i - 1 ))); 
             If( $Tem != "Cero" ) 
                $Rtn .= $Tem . $this->SP; 
             $i = $i - 1; 
          } 

           
          //--------------------- GoSub FiltroMil ------------------------------ 
          $Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 
          while(1) 
          { 
             $Ptr = strpos($Rtn, "Mil ");        
             If(!($Ptr===false)) 
             { 
                If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 
                  $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 
                Else 
                 break; 
             } 
             else break; 
          } 

          //--------------------- GoSub FiltroCiento ------------------------------ 
          $Ptr = -1; 
          do{ 
             $Ptr = strpos($Rtn, "Cien ", $Ptr+1); 
             if(!($Ptr===false)) 
             { 
                $Tem = substr($Rtn, $Ptr + 5 ,1); 
                if( $Tem == "M" || $Tem == $this->Void) 
                   ; 
                else           
                   $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 
             } 
          }while(!($Ptr === false)); 

          //--------------------- FiltroEspeciales ------------------------------ 
          $Rtn=str_replace("Diez Un", "Once", $Rtn ); 
          $Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 
          $Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 
          $Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 
          $Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 
          $Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 
          $Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 
          $Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 
          $Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 
          $Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 
          $Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 
          $Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 
          $Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 
          $Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 
          $Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 
          $Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 
          $Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 
          $Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 

          //--------------------- FiltroUn ------------------------------ 
          If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 
          //--------------------- Adicionar Y ------------------------------ 
          for($i=65; $i<=88; $i++) 
          { 
            If($i != 77) 
               $Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 
          } 
          $Rtn=str_replace("*", "a" , $Rtn); 
          return($Rtn); 
      } 
      function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) { 
        $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 
      } 
      function Parte($x) { 
          $Rtn=''; 
          $t=''; 
          $i=''; 
          Do 
          { 
            switch($x) 
            { 
               Case 0:  $t = "Cero";break; 
               Case 1:  $t = "Un";break; 
               Case 2:  $t = "Dos";break; 
               Case 3:  $t = "Tres";break; 
               Case 4:  $t = "Cuatro";break; 
               Case 5:  $t = "Cinco";break; 
               Case 6:  $t = "Seis";break; 
               Case 7:  $t = "Siete";break; 
               Case 8:  $t = "Ocho";break; 
               Case 9:  $t = "Nueve";break; 
               Case 10: $t = "Diez";break; 
               Case 20: $t = "Veinte";break; 
               Case 30: $t = "Treinta";break; 
               Case 40: $t = "Cuarenta";break; 
               Case 50: $t = "Cincuenta";break; 
               Case 60: $t = "Sesenta";break; 
               Case 70: $t = "Setenta";break; 
               Case 80: $t = "Ochenta";break; 
               Case 90: $t = "Noventa";break; 
               Case 100: $t = "Cien";break; 
               Case 200: $t = "Doscientos";break; 
               Case 300: $t = "Trescientos";break; 
               Case 400: $t = "Cuatrocientos";break; 
               Case 500: $t = "Quinientos";break; 
               Case 600: $t = "Seiscientos";break; 
               Case 700: $t = "Setecientos";break; 
               Case 800: $t = "Ochocientos";break; 
               Case 900: $t = "Novecientos";break; 
               Case 1000: $t = "Mil";break; 
               Case 1000000: $t = "Millón";break; 
            } 

            if($t == $this->Void) 
            { 
              $i = floatval($i) + 1; 
              $x = $x / 1000; 
              if($x== 0) $i = 0; 
            } 
            else 
               break; 
                  
          }while($i != 0); 
          
          $Rtn = $t; 
          switch($i) 
          { 
             Case 0: $t = $this->Void;break; 
             Case 1: $t = " Mil";break; 
             Case 2: $t = " Millones";break; 
             Case 3: $t = " Billones";break; 
          } 
          return($Rtn . $t); 
      }  
  //===========================================================
  //Page header
  public function Header() {
      $logos = $GLOBALS["logotipo"];
      $logos2 = $GLOBALS["logotipo"];
      if ($GLOBALS["Estado"]==0) {
        $cancelado='<br><span  style="color:red; font-size:20px"><b>Cancelado</b></span>';
      }else{
        $cancelado='';
      }
        $tamano1='30';//30
        $tamano2='40';//40
        $tamano3='30';//30
      $html = '
          <style type="text/css">
            .info_fac{font-size: 9px;}
            .info_facd{font-size: 8px;}
            .httablelinea{vertical-align: center;border-bottom: 1px solid #9e9e9e;}
            .httableleft{border-left: 1px solid #9e9e9e;}
            .httableright{border-right: 1px solid #9e9e9e;}
            .httabletop{border-top: 1px solid #9e9e9e;}
          </style>
          <style type="text/css">
        .httable{vertical-align: center;border-bottom: 2px solid #9e9e9e;border-top: 1px solid #9e9e9e;}
        .httablepro{vertical-align: center;border-bottom: 1px solid #9e9e9e;}
        .httborderbottom{border-bottom: 1px solid #9e9e9e;}
        .httborderleft{border-left: 1px solid #9e9e9e;}
        .httborderright{border-right: 1px solid #9e9e9e;}
        .httbordertop{border-top: 1px solid #9e9e9e;}
        .magintablepro{margin-top:0px;margin-bottom:0px;margin: 0px;}
      </style>
          <table width="100%" border="0" cellpadding="4px" class="info_fac">
            <tr>
              <td width="24%"><img src="'.$logos.'"></td>
              <td width="38%" >
                <table border="0" class="httborderbottom httborderleft httborderright httbordertop">
                  <tr>
                    <td><b>Serie '.$GLOBALS["serie"].'</b></td>
                    <td><b>Factura No. '.str_pad($GLOBALS["Folio"], 4, "0", STR_PAD_LEFT).'</b></td>
                  </tr>
                  <tr><td colspan="2">Nombre o razon social '.$GLOBALS["Nombrerasonsocial"].'</td></tr>
                  <tr><td colspan="2"><b>RFC: '.$GLOBALS["rrfc"].'</b></td></tr>
                  <tr><td colspan="2">'.$GLOBALS["rdireccion"].'</td></tr>
                  <tr><td colspan="2" class="httborderbottom"><b>Régimen fiscal:</b> '.$GLOBALS['regimenf'].'</td></tr>
                  <tr><td colspan="2" class="httbordertop"><b>Uso de CFDI:</b> '.$GLOBALS["cfdi"].'</td></tr>
                </table>
              </td>
              <td width="38%">
                <table border="0">
                  <tr><td align="center" class="httborderbottom">Folio Fiscal</td></tr>
                  <tr><td align="center" class="httborderbottom">'.$GLOBALS["folio_fiscal"].' '.$cancelado.'</td></tr>
                  <tr><td align="center" class="httborderbottom" >NO. DE SERIE DEL CERTIFICADO DEL SAT</td></tr>
                  <tr><td align="center" class="httborderbottom" >'.$GLOBALS["nocertificadosat"].'</td></tr>
                  <tr><td align="center" class="httborderbottom" >NO. DE SERIE DEL CERTIFICADO EMISOR</td></tr>
                  <tr><td align="center" class="httborderbottom" >'.$GLOBALS["certificado"].'</td></tr>';
                  if ($GLOBALS["fechafac"]!='') {
          $html.='<tr><td align="center" class="httborderbottom">FECHA Y HORA DE EMISION</td></tr>
                  <tr><td align="center" class="httborderbottom">'.$GLOBALS["fechafac"].'</td></tr>';
                  }
          $html.='<tr><td align="center" class="httborderbottom">FECHA Y HORA DE CERTIFICACION</td></tr>
                  <tr><td align="center" class="httborderbottom">'.$GLOBALS["fechatimbre"].'</td></tr>
                </table>
              </td>
            </tr>
            </table>
            <table width="100%" border="0" cellpadding="4px" class="info_fac">
              <tr><td class="httborderbottom" style="width:15%"><b>NOMBRE :</b></td><td class="httborderbottom" style="width:85%">'.$GLOBALS["cliente"].'</td></tr>
              <tr><td class="httborderbottom"><b>R.F.C:</b></td><td class="httborderbottom">'.$GLOBALS["clirfc"].'</td></tr>
              <tr><td class="httborderbottom"><b>DIRECCIÓN : </b></td><td class="httborderbottom">'.$GLOBALS["clidireccion"].'</td></tr>
              <tr><td class="httborderbottom"><b>RÉGIMEN FISCAL: </b></td><td class="httborderbottom">'.$GLOBALS["RegimenFiscalReceptor"].'</td></tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    $styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
          $params = $this->serializeTCPDFtagParameters(array($GLOBALS["folio_fiscal"], 'QRCODE,L', '', '', 45, 45, $styleQR, 'N'));
    $html='';

    $html2 .= ' 
          <style type="text/css">
              .fontFooter10{font-size: 10px;}
              .fontFooterp{font-size: 9px;margin-top:0px;}
              .fontFooter{font-size: 9px;margin-top:0px;}
              .fontFooterp{font-size: 8px;}
              .fontFooterpt{font-size: 7px;}
              .fontFooterpt6{font-size: 6px;}
              p{margin:0px;}
              .valign{vertical-align:middle;}
              .httablelinea{vertical-align: center;border-bottom: 1px solid #9e9e9e;}
              .footerpage{font-size: 9px;color: #9e9e9e;}
          </style>';
          $html2 .= '<table width="100%" border="0" class="fontFooter10">
                      <tr >
                        <td cellpadding="2">';
                          if($GLOBALS['numproveedorv']=='block'){
                            $html2 .= '<b>Numero de Proveedor:</b> '.$GLOBALS['numproveedor'];
                          }
                          
                        $html2 .= '</td>
                        <td cellpadding="2">';
                          if($GLOBALS['numordencomprav']=='block'){
                            $html2 .= '<b>Numero de orden de compra:</b> '.$GLOBALS['numordencompra'];
                          }
                        $html2 .= '</td>
                      </tr>
                      <tr>
                        <td cellpadding="2" style="display:'.$GLOBALS['Caducidadv'].'">';
                          if($GLOBALS['Caducidadv']=='block'){
                            $html2 .= '<b>Caducidad:</b> '.$GLOBALS['Caducidad'];
                          }
                          
                        $html2 .= '</td>
                        <td cellpadding="2" style="display:'.$GLOBALS['Lotev'].'">';
                          if($GLOBALS['Lotev']=='block'){
                            $html2 .= '<b>Lote:</b> '.$GLOBALS['Lote'];
                          }
                        $html2 .= '</td>
                      </tr>
                      ';
            if ($GLOBALS['observaciones']!='') {
              $html2 .= '<tr>
                          <td cellpadding="4"><b>Observaciones:</b> '.$GLOBALS['observaciones'].'</td>
                        </tr>';
            }
            $descuentog=0;
            foreach ($GLOBALS["facturadetalles"] as $item) {
              $descuentog=$descuentog+$item->descuento;
            }
            $rowspan=6;
            if ($GLOBALS["isr"]>0) {
              $rowspan=$rowspan+1;
            }
            if ($GLOBALS["ivaretenido"]>0) {
              $rowspan=$rowspan+1;
            }
            if ($GLOBALS["cincoalmillarval"]>0) {
              $rowspan=$rowspan+1;
            }
            if ($GLOBALS["outsourcing"]>0) {
              $rowspan=$rowspan+1;
            }
  $html2 .= '<tr>
              <td  width="48%" rowspan="'.$rowspan.'" valign="top"><b>CANTIDAD EN LETRA::</b> '.($this->ValorEnLetras($GLOBALS["total"],$GLOBALS["moneda"])).'</td>
              <td width="20%" align="left"></td>
              <td width="17%" align="right" class="httablelinea"><b>SUBTOTAL</b></td>
              <td width="15%" align="center" class="httablelinea">$ '.number_format(($GLOBALS["subtotal"]),2,'.',',').'</td>
            </tr>
            <!--<tr>
              <td >&nbsp;</td>
              <td align="right" class="httablelinea"><b>DESCUENTO</b></td>
              <td align="center" class="httablelinea">$ '.number_format($descuentog,2,'.',',').'</td>
            </tr>-->
        <tr>
          <td >&nbsp;</td>
          <td align="right" class="httablelinea"><b>IVA</b></td>
          <td align="center" class="httablelinea">$ '.number_format($GLOBALS["iva"],2,'.',',').'</td>
        </tr>';

        if ($GLOBALS["isr"]>0) {
            $html2 .= '<tr>
                        <td></td>
                        <td align="right" class="httablelinea"><b>10% Retención ISR</b></td>
                        <td align="center"class="httablelinea">$ '.number_format($GLOBALS["isr"],2,'.',',').'</td>
                      </tr>';
        }
        if ($GLOBALS["ivaretenido"]>0) {
            $html2.='<tr>
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>10.67% Retención IVA</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["ivaretenido"],2,'.',',').'</td>
                    </tr>';
        }
        
        if ($GLOBALS["cincoalmillarval"]>0) {
            $html2.='<tr >
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>5 al millar</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["cincoalmillarval"],2,'.',',').'</td>
                    </tr>';
        }
        if ($GLOBALS["outsourcing"]>0) {
            $html2.='<tr >
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>6% Retención servicios de personal</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["outsourcing"],2,'.',',').'</td>
                    </tr>';
        }
        
        $html2 .= '<tr>
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>TOTAL</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["total"],2,'.',',').'</td>
                    </tr>';
      $html2 .= '</table>';
    $html2 .= '
      <table><tr><td></td></tr></table>
      <table width="100%" border="0" class="fontFooter">
        <tr>
          <td width="50%"><b>MÉTODO DE PAGO: </b>'.$GLOBALS["MetodoPagol"].'</td>
          <td width="50%" style="display:none"><b>ÚLTIMOS 4 DIGITOS DE LA CUENTA O TARJETA: </b>'.$GLOBALS["tarjeta"].'</td>
        </tr>
        <tr>
          <td width="50%"><b>FORMA DE PAGO: </b>'.$GLOBALS["FormaPagol"].'</td>
          <td width="50%"></td>
        </tr>
        <tr>
          <td width="50%"><b>TIPO DE COMPROBANTE: </b>'.$GLOBALS["tipoComprobante"].'</td>
          <td width="50%"></td>
        </tr>
      </table>';
      $html2 .= '<table><tr><td></td></tr></table>';
      $html2 .= '<table width="100%" border="0" class="fontFooterpt" cellpadding="3">';
        $html2 .= '<tr>';
          $html2 .= '<td rowspan="6" width="20%" align="center" style="text-align: center;" ><tcpdf method="write2DBarcode" params="' . $params . '" /></td>';
          $html2 .= '<td width="80%" class="httablelinea">SELLO DIGITAL DEL EMISOR</td>';
        $html2 .= '</tr>';
        $html2 .= '<tr>';
          $html2 .= '<td height="35px" class="httablelinea fontFooterpt">'.$GLOBALS["selloemisor"].'</td>';
        $html2 .= '</tr>';
        $html2 .= '<tr>';
          $html2 .= '<td class="httablelinea">SELLO DIGITAL SAT</td>';
        $html2 .= '</tr>';
        $html2 .= '<tr>';
          $html2 .= '<td height="35px" valign="top" class="httablelinea fontFooterpt">'.$GLOBALS["sellosat"].'';
          $html2 .= '</td>';
        $html2 .= '</tr>';
        $html2 .= '<tr>';
          $html2 .= '<td class="httablelinea">CADENA ORIGINAL DEL COMPLEMENTO DE CERTIFICACIÓN DIGITAL DEL SAT</td>';
        $html2 .= '</tr>';
        $html2 .= '<tr>';
          $html2 .= '<td height="35px" class="httablelinea fontFooterpt6" colspan="1">'.substr(utf8_encode($GLOBALS["cadenaoriginal"]), 0, 635).'</td>';
        $html2 .= '</tr>';
      $html2 .= '</table>';
    

      $html2 .= '<table width="100%" border="0" cellpadding="2" class="fontFooterp">';
        $html2 .= '<tr>';
          $html2 .= '<td align="center"></td>';
        $html2 .= '</tr>';
        $html2 .= '<tr>';
          $html2 .= '<td align="right" class="footerpage">';
          $html2 .= 'Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>';
        $html2 .= '</tr>';
      $html2 .= '</table>';
      //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      $this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Folio_'.$GLOBALS["Folio"]);
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '80', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('90');

// set auto page breaks
$pdf->SetAutoPageBreak(true, 90);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');
$htmlp='
      <style type="text/css">
        .httable{vertical-align: center;border-bottom: 2px solid #9e9e9e;border-top: 1px solid #9e9e9e;}
        .httablepro{vertical-align: center;border-bottom: 1px solid #9e9e9e;}
        .magintablepro{margin-top:0px;margin-bottom:0px;margin: 0px;}
      </style>
  <table border="0" align="center" cellpadding="2">
    <tr valign="middle">
      <th width="10%" class="httable"><b>CANTIDAD</b></th>
      <th width="10%" class="httable"><b>CLAVE DE UNIDAD</b></th>
      <th width="29%" class="httable"><b>CLAVE DEL PRODUCTO Y/O SERVICIO</b></th>
      <th width="31%" class="httable"><b>DESCRIPCION</b></th>
      <th width="10%" class="httable"><b>VALOR UNITARIO</b></th>
      <!--<th width="10%" class="httable"><b>Descuento</b></th>-->
      <th width="10%" class="httable"><b>SUBTOTAL</b></th>
    </tr>';
foreach ($facturadetalles as $item) {
  $htmlp.='
      <tr class="magintablepro">
        <td class="httablepro" align="center">'.$item->Cantidad.'</td>
        <td class="httablepro" align="center">'.$item->Unidad.' / '.$item->nombre.'</td>
        <td class="httablepro">'.$item->servicioId.' / '.$item->Descripcion.'</td>
        <td class="httablepro">'.$item->Descripcion2.'</td>
        <td class="httablepro" align="center">$ '.$item->Cu.'</td>
        <!--<td class="httablepro" align="center">$ '.$item->descuento.'</td>-->
        <td class="httablepro" align="center">$ '.number_format(($item->Cantidad*$item->Cu)-$item->descuento,2,'.',',').'</td>
      </tr>';
}
$htmlp.='</table>';
$pdf->writeHTML($htmlp, true, false, true, false, '');



//$pdf->Output(__DIR__.'Factura_'.$Folio.'.pdf', 'FI');
$url=$_SERVER['DOCUMENT_ROOT'].'/facturacionimpunizaciones/hulesyg/facturaspdf/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
$pdf->Output($url.'Factura_'.$Folio.'.pdf', 'FI');
if(isset($idfactura)){
  //$pdf->Output('hulesygrapas/hulesyg/facturas/'.$idfactura.'.pdf', 'F');
}
//$pdf->Output($idfactura.'.pdf', 'F');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>