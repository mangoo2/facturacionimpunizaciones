<link href="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
<script src="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/jquery-ui.css">

<script src="<?php echo base_url();?>public/js/timbrado.js?v=<?php echo date('YmdGis') ?>"></script>
<?php if($facturaId>0){ ?>
		<script type="text/javascript">
			$(document).ready(function($) {
				obtenerdatosfactura(<?php echo $facturaId;?>);
			});
		</script>
<?php } 
if(isset($_GET['duplicado'])){?>
	<script type="text/javascript">
			$(document).ready(function($) {
				obtenerdatosfactura(<?php echo $_GET['duplicado'];?>);
			});
		</script>
<?php }
?>