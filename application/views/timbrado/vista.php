<style type="text/css">
    .select2-results__option{
        padding-left: 5px !important;
        padding-right: 5px !important;
    }
    .totalesright{
        text-align: right;

    }
    .totalesborder{
        border-bottom: 0px !important;
        border-top: 0px !important;
    }
    .inputconceptos{
        width: 100px;
        border:0px;
    }
    .error{
        margin: 0px;
        color: red;
    }
    .ifrafac{
        width: 100%;
        height: 400px;
        border: 0px;
    }
    #modal_timbrado .modal-body{
        padding: 0px;
    }

</style>
<?php if(isset($_GET['idcli'])){
    $optioget='<option value="'.$_GET['idcli'].'">'.$_GET['clinombre'].'</option>';
    $rfcget=$_GET['clirfc'];
}else{
    $optioget='';
    $rfcget='';
} ?>
<input type="hidden" id="mesactual" value="<?php echo date('m');?>">
<input type="hidden" id="anioactual" value="<?php echo date('Y');?>">
<!--begin::Content-->

<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-sm-10"> 
                </div>   
                <div class="col-sm-2" align="right"> 
                    <a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>
                </div>    
            </div>  
            <br>
            <!--begin::Dashboard-->
            <div class="card">
                <div class="card-body">
                    <form id="validateSubmitForm" method="post" autocomplete="off">
                        <div class="card-body" style="padding-left: 10px;    padding-right: 10px;">
                            <h4>Datos de factura</h4>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <div class="row">
                                            <label class="col-sm-2 col-form-label">Buscar cliente</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="idcliente" name="idcliente" required>
                                                    <?php echo $optioget; ?>
                                                </select>
                                            </div>
                                            <label class="col-sm-2 col-form-label">RFC</label>
                                            <div class="col-sm-4">
                                                <input class="form-control" type="text" id="rfc" name="rfc" value="<?php echo $rfcget;?>" readonly required>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>        
                            <div class="row agregardatospublicogeneral">
                  
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <div class="row">
                                            <label class="col-sm-2 col-form-label">Método de pago</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="FormaPago" name="FormaPago" required style="width: 100%">
                                                    <?php foreach ($metodopago->result() as $item) { ?>
                                                        <option value="<?php echo $item->metodopago;?>"><?php echo $item->metodopago_text;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <label class="col-sm-2 col-form-label">Forma de pago</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="MetodoPago" name="MetodoPago" required>
                                                    <?php foreach ($formapago->result() as $item) { ?>
                                                        <option value="<?php echo $item->formapago;?>"><?php echo $item->formapago_text;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <div class="row">
                                            <label class="col-sm-2 col-form-label">Tipo de moneda</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="moneda" name="moneda" required>
                                                        <option value="MXN">MXN Peso Mexicano</option>
                                                        <option value="USD">Dolares Norteamericanos</option>
                                                        <option value="EUR">Euro</option>
                                                    
                                                </select>
                                            </div>
                                            <label class="col-sm-2 col-form-label" >Uso de CFDI</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="uso_cfdi" name="uso_cfdi" required>
                                                    <?php foreach ($uso_cfdi->result() as $item) { ?>
                                                        <option value="<?php echo $item->uso_cfdi;?>"><?php echo $item->uso_cfdi_text;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <div class="row">
                                            <label class="col-sm-2 col-form-label">Condiciones de pago</label>
                                            <div class="col-sm-4">
                                                <input class="form-control" type="text" id="CondicionesDePago" name="CondicionesDePago">
                                            </div>
                                            <label class="col-sm-2 col-form-label">Fecha</label>
                                            <div class="col-sm-4">
                                                <input class="form-control" type="datetime-local" id="fechafac" name="fechafac" min="<?php 
                                                $fecha = date('Y-m-d H:i');
                                                $newfecha =strtotime($fecha.' -2 day -20 hours');
                                                $newfecha2 =strtotime($fecha.' -1 hours');

                                                echo date("Y-m-d",$newfecha).'T'.date("H:i",$newfecha)?>" max="<?php echo date("Y-m-d",$newfecha2).'T'.date("H:i",$newfecha2)?>" value="<?php echo date("Y-m-d",$newfecha2).'T'.date("H:i",$newfecha2)?>" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>            
                            </div>
                        </form>
                            <h4>Datos adicionales</h4>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <span class="switch switch_morado switch-icon">
                                                    <label>
                                                        <input type="checkbox" id="iddatosgenerales" onclick="check_datosgenerales()">
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="texto_datos_generales" style="display: none">    
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <div class="row">
                                                <label class="col-sm-2 col-form-label"># Proveedor</label>
                                                <div class="col-sm-3">
                                                    <input class="form-control" type="text" name="numproveedor" id="numproveedor">
                                                </div>
                                                <label class="col-sm-2 col-form-label" >#Orden de compra</label>
                                                <div class="col-sm-2">
                                                    <input class="form-control" type="text" name="numordencompra" id="numordencompra">
                                                </div>
                                                <label class="col-sm-1 col-form-label" >Lote</label>
                                                <div class="col-sm-2">
                                                    <input class="form-control" type="text" id="Lote" name="Lote">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>            
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <div class="row">
                                                <label class="col-sm-2 col-form-label">Caducidad</label>
                                                <div class="col-sm-3">
                                                    <input class="form-control" type="text" name="Caducidad" id="Caducidad">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <h4>Factura Relacionada</h4>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <span class="switch switch_morado switch-icon">
                                                    <label>
                                                        <input type="checkbox" id="facturarelacionada" onclick="check_datosgenerales()">
                                                        <span></span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">     
                                        <div class="row divfacturarelacionada" style="display:none;">
                                            <div class="col-sm-4">
                                                <label>Tipo Relacion</label>
                                                <select class="form-control" id="TipoRelacion">
                                                    <!--
                                                    <option value="01">01 Nota de crédito de los documentos relacionados</option>
                                                    <option value="02">02 Nota de débito de los documentos relacionados</option>
                                                    <option value="03">03 Devolución de mercancía sobre facturas o traslados previos</option>
                                                    -->
                                                    <option value="04">04 Sustitución de los CFDI previos</option>
                                                    <!--
                                                    <option value="05">05 Traslados de mercancias facturados previamente</option>
                                                    <option value="06">06 Factura generada por los traslados previos</option>
                                                    <option value="07">07 CFDI por aplicación de anticipo</option>
                                                    -->
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Folio Fiscal</label>
                                                <input type="text" id="uuid_r" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>          
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <h4>Partidas</h4>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper no-footer">
                                                        <table  class="table table-sm" id="table_conceptos" style="display: block;overflow-x: auto;">
                                                            <thead>
                                                                <tr>
                                                                    <td >
                                                                        <label>Cantidad</label>
                                                                        <input type="number" class="form-control" id="scantidad" min="1" style="width:100px">
                                                                    </td>
                                                                    <td >
                                                                        <label>U.SAT</label>
                                                                        <select class="form-control" id="sunidadsat" style="width: 100%">
                                                                            <?php foreach ($rowunidades->result() as $item) { ?>
                                                                                <option value="<?php echo $item->Clave;?>"><?php echo $item->nombre;?></option>
                                                                            <?php } ?>
                                                                            
                                                                        </select>
                                                                    </td>
                                                                    <td >
                                                                        <label>Concepto<span style="color: white">_</span>SAT</label>
                                                                        <select class="form-control" id="sconseptosat" style="width: 100%">
                                                                            <?php foreach ($rowservicios->result() as $item) { ?>
                                                                                <option value="<?php echo $item->Clave;?>"><?php echo $item->nombre;?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </td>
                                                                    <td >
                                                                        <label>Descripción</label>
                                                                        <!-- <input type="text" class="form-control" id="sdescripcion"> -->
                                                                        <select class="form-control" id="sdescripcion" onchange="seleccionarproducto()" style="width: 100%">
                                                                            <option value="0">Selecciona opción</option>
                                                                            <?php foreach ($rowproductos_hules->result() as $item) { ?>
                                                                                <option data-precio="<?php echo $item->precio;?>" value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </td>
                                                                    <td >
                                                                        <label>Precio</label>
                                                                        <input type="number" class="form-control" id="sprecio" min="0.1" style="width:100px">
                                                                    </td>
                                                                    <td >
                                                                        <label>Descuento</label>
                                                                    </td>
                                                                    <td ><label>Monto</label></td>
                                                                    <td >
                                                                        <p>IVA</p>
                                                                        <span class="switch switch_morado switch-icon">
                                                                            <label>
                                                                                <input type="checkbox" name="aplicariva" id="aplicariva">
                                                                                <span></span>
                                                                            </label>
                                                                        </span>
                                                                    </td>
                                                                    <td >
                                                                        <br>
                                                                        <a class="btn btn-icon btn_light_morado btn-circle mr-2" onclick="agregarconcepto()">
                                                                            <i class="icon-xl fas fa-plus white"></i>
                                                                        </a>
                                                                    </td>


                                                                </tr>
                                                            </thead>
                                                            <tbody class="addcobrar">
                                                                
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                   <td rowspan="9" colspan="3"></td>
                                                                   <td colspan="4" class="totalesright">Subtotal:</td>
                                                                   <td colspan="2">
                                                                        <span style="float: left;">$</span>
                                                                        <input type="" name="Subtotalinfo" id="Subtotalinfo" readonly="" style="background: transparent; border:0; width: 85%;" value="0.00">
                                                                        <input type="hidden" name="Subtotal" id="Subtotal" readonly="" style="background: transparent; border:0; width: 90%;" value="11800">
                                                                   </td> 
                                                                </tr>
                                                                <tr>
                                                                   <td colspan="4" class="totalesright totalesborder">Descuento: </td>
                                                                   <td colspan="2" class="totalesborder">
                                                                        <span style="float: left;">$</span>
                                                                        <input type="" name="descuentof" id="descuentof" readonly="" style="background: transparent; border:0; width: 85%;" value="0.00">
                                                                   </td> 
                                                                </tr>
                                                                <tr>
                                                                   <td colspan="4" class="totalesright totalesborder">I.V.A: </td>
                                                                   <td colspan="2" class="totalesborder">
                                                                            <span style="float: left;">$</span>
                                                                            <input type="" name="iva" id="iva" readonly="" style="background: transparent; border:0; width: 85%;" value="0.00">
                                                                    </td> 
                                                                </tr>
                                                                <tr class="risr">
                                                                   <td colspan="4" class="totalesright totalesborder">
                                                                        <input type="checkbox" id="risr">
                                                                        <label for="risr" onclick="calculartotales_set(1000)">10 % Retencion I.S.R.</label>
                                                                   </td>
                                                                   <td colspan="2" class="totalesborder">
                                                                            <span style="float: left;">$</span>
                                                                            <input type="" name="isr" id="isr" readonly="" style="background: transparent; border:0; width: 85%;" value="0.00">
                                                                    </td> 
                                                                </tr>
                                                                <tr class="riva">
                                                                   <td colspan="4" class="totalesright totalesborder">
                                                                        <input type="checkbox" id="riva">
                                                                        <label for="riva" onclick="calculartotales_set(1000)">10.67 % Retencion I.V.A.</label>
                                          
                                                                   </td>
                                                                   <td colspan="2" class="totalesborder">
                                                                            <span style="float: left;">$</span>
                                                                            <input type="" name="ivaretenido" id="ivaretenido" readonly="" style="background: transparent; border:0; width: 85%;" value="0.00">
                                                                   </td> 
                                                                </tr>
                                                                <tr class="5almillar">
                                                                   <td colspan="4" class="totalesright totalesborder">
                                                                        <input type="checkbox" id="5almillar">
                                                                        <label for="5almillar" onclick="calculartotales_set(1000)">5 al millar.</label>
                                                                   </td>
                                                                   <td colspan="2" class="totalesborder">
                                                                            <span style="float: left;">$</span>
                                                                            <input type="" name="5almillarval" id="5almillarval" readonly="" style="background: transparent; border:0; width: 85%;" value="0.00">
                                                                   </td> 
                                                                </tr>
                                                                <tr class="aplicaout">
                                                                   <td colspan="4" class="totalesright totalesborder">
                                                                        <input type="checkbox" id="aplicaout">
                                                                        <label for="aplicaout" onclick="calculartotales_set(1000)">6% Retencion servicios de personal.</label>
                                                                   </td>
                                                                   <td colspan="2" class="totalesborder">
                                                                        <span style="float: left;">$</span>
                                                                        <input type="" name="outsourcing" id="outsourcing" readonly="" style="background: transparent; border:0; width: 85%;" value="0.00">
                                                                   </td> 
                                                                </tr>
                                                                <tr>
                                                                   <td colspan="4" class="totalesright totalesborder">Total</td>
                                                                   <td colspan="2" class="totalesborder">
                                                                        <span style="float: left;">$</span>
                                                                        <input type="" name="total" id="total" readonly="" style="background: transparent; border:0; width: 85%;" value="0.00">
                                                                      
                                                                   </td> 
                                                                </tr>
                                                                
                                                            </tfoot>
                                                            
                                                        </table>
                                                    </div>    
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <div class="row">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-3">
                                                <button class="btn btn-primary registrofac_save" style="width: 100%" onclick="registrofac(0,<?php echo $facturaId;?>)">Guardar factura</button>
                                            </div>
                                            <div class="col-sm-3">
                                                <button class="btn btn-primary registrofac_preview" style="width: 100%">Timbrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>            
                        </div>
                    
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modal_timbrado" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body preview_iframe">
                
                
            </div>
            <div class="modal-footer">
                   
                        <button type="button" class="btn btn_light_morado font-weight-bold btn-pill registrofac">Timbrar</button>
                    
                        <button type="button" class="btn btn_light_morado font-weight-bold btn-pill" data-dismiss="modal">Regresar</button>
                    
            </div>
        </div>
    </div>
</div>