<style type="text/css">
    .accordion .card {
        border: 2px solid #12264b !important;
        border-radius: 6px !important;
    }
    .labeltable{
        font-size: 1rem !important;
    }
    .fixed-action-btn{
        position: fixed;

    }
    .ifrafac{
        width: 100%;
        height: 420px;
        border:0px;
    }
    .preview_iframe{
        padding: 0px;
    }
    .error{
        color: red;
        font-weight: bold;
    }
    #collapse41,#collapse51{
        display: block !important;
    }
</style>


<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->                  
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            
            <!--begin::Dashboard-->
            <!--<div class="card card-custom gutter-b">
                <div class="card-body">-->
                    <div class="row">
                        <div class="col-md-12">
                            <form id="validateSubmitForm" method="post" autocomplete="off"> 
                                <div class="accordion accordion-toggle-arrow" id="accordionExample1">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="false">Datos del emisor</div>
                                        </div>
                                        <div id="collapseOne1" class="collapse" data-parent="#accordionExample1" style="">
                                            <div class="card-body">
                                                <div class="row">
                                                  <div class="col-md-6">
                                                   <label>Nombre o razón social:</label>
                                                   <input type="text" name="razonsocial" id="razonsocial" class="form-control" value="<?php echo $Nombre;?>" readonly required>
                                                  </div>
                                                  <div class="col-md-6">
                                                   <label>RFC:</label>
                                                   <input type="text" name="rfcemisor" id="rfcemisor" class="form-control" value="<?php echo $rFCEmisor;?>" readonly required>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <label>Régimen fiscal: <?php echo $Regimen;?></label>
                                                    <select id="Regimen" name="Regimen" class="form-control browser-default">
                                                    <?php if($Regimen==601){ ?>
                                                      <option value="601">601 General de Ley Personas Morales</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==603){ ?>
                                                      <option value="603">603 Personas Morales con Fines no Lucrativos</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==605){ ?>
                                                      <option value="605">605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==606){ ?>
                                                      <option value="606">606 Arrendamiento</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==607){ ?>
                                                      <option value="607">607 Régimen de Enajenación o Adquisición de Bienes</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==608){ ?>
                                                      <option value="608">608 Demás ingresos</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==609){ ?>
                                                      <option value="609">609 Consolidación</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==610){ ?>
                                                      <option value="610">610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==611){ ?>
                                                      <option value="611">611 Ingresos por Dividendos (socios y accionistas)</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==612){ ?>
                                                      <option value="612">612 Personas Físicas con Actividades Empresariales y Profesionales</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==614){ ?>
                                                      <option value="614">614 Ingresos por intereses</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==615){ ?><option value="615">615 Régimen de los ingresos por obtención de premios</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==616){ ?>
                                                      <option value="616">616 Sin obligaciones fiscales</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==620){ ?>
                                                      <option value="620">620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==621){ ?>
                                                      <option value="621">621 Incorporación Fiscal</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==622){ ?>
                                                      <option value="622">622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==623){ ?>
                                                      <option value="623">623 Opcional para Grupos de Sociedades</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==624){ ?>
                                                      <option value="624">624 Coordinados</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==628){ ?>
                                                      <option value="628">628 Hidrocarburos</option>
                                                    <?php } ?>
                                                    <?php if($Regimen==629){ ?>
                                                      <option value="629">629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>
                                                      <?php } ?>
                                                    <?php if($Regimen==630){ ?>
                                                      <option value="630">630 Enajenación de acciones en bolsa de valores</option>
                                                    <?php } ?>
                                                                 
                                                    </select> 
                                                  </div>
                                                  <div class="col-md-6">
                                                     <label>Tipo:</label>
                                                     <select name="tipof" id="tipof" class="form-control browser-default">
                                                       <option value="P">P Pago</option>
                                                     </select>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo1">Datos del Receptor</div>
                                        </div>
                                        <div id="collapseTwo1" class="collapse" data-parent="#accordionExample1">
                                            <div class="card-body">
                                                <div class="row">
                                                  <div class="col-md-6">
                                                   <label>Nombre o razón social:</label>
                                                   <input type="text" name="razonsocialreceptor" id="razonsocialreceptor" class="form-control" value="<?php echo $razonsocialreceptor;?>" readonly required>
                                                  </div>
                                                  <div class="col-md-6">
                                                   <label>RFC:</label>
                                                   <input type="text" name="rfcreceptor" id="rfcreceptor" class="form-control" value="<?php echo $rfcreceptor;?>" readonly required>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <label>Uso:</label>
                                                    <select id="UsoCFDI" name="UsoCFDI" class="form-control browser-default">
                                                      <option value="P01">P01 Por definir</option>
                                                    </select>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title" data-toggle="collapse" data-target="#collapseThree1" aria-expanded="true">Comprobante</div>
                                        </div>
                                        <div id="collapseThree1" class="collapse show" data-parent="#accordionExample1" style="">
                                            <div class="card-body">
                                                <div class="row">
                                                      <div class="col-md-4">
                                                       <label>Fecha y hora de expedición:</label>
                                                       <input type="datetime-local" name="Fecha" id="Fecha" class="form-control" value="<?php echo $Fecha;?>" style="background-color: white;"  required>
                                                      </div>
                                                      <div class="col-md-4">
                                                       <label>Codigo postal:</label>
                                                       <input type="text" name="LugarExpedicion" id="LugarExpedicion" class="form-control" value="<?php echo $LugarExpedicion;?>" style="background-color: white;" required>
                                                      </div>
                                                      <div class="col-md-4">
                                                        <label>Moneda:</label>
                                                        <select id="Moneda" name="Moneda" class="form-control browser-default">
                                                          <option value="XXX">XXX los códigos asignados para las transacciones en que intervenga ninguna moneda</option>
                                                        </select>
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <div class="col-md-4">
                                                       <label>Folio:</label>
                                                       <input type="text" name="Folio" id="Folio" class="form-control" value="<?php echo $Folio;?>" style="background-color: white;" required>
                                                      </div>
                                                      <div class="col-md-4">
                                                       <label>Serie:</label>
                                                       <input type="text" name="Serie" id="Serie" class="form-control" value="H" style="background-color: white;" required>
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label><input type="checkbox" id="facturarelacionada"> Complemento Relacionada</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row divfacturarelacionada" style="display:none;">
                                                        <div class="col-md-4">
                                                            <label>Tipo Relacion</label>
                                                            <select class="form-control" id="TipoRelacion">
                                                                <!--
                                                                <option value="01">01 Nota de crédito de los documentos relacionados</option>
                                                                <option value="02">02 Nota de débito de los documentos relacionados</option>
                                                                <option value="03">03 Devolución de mercancía sobre facturas o traslados previos</option>
                                                                -->
                                                                <option value="04">04 Sustitución de los CFDI previos</option>
                                                                <!--
                                                                <option value="05">05 Traslados de mercancias facturados previamente</option>
                                                                <option value="06">06 Factura generada por los traslados previos</option>
                                                                <option value="07">07 CFDI por aplicación de anticipo</option>
                                                                -->
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Folio Fiscal</label>
                                                            <input type="text" id="uuid_r" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title" data-toggle="collapsexxx" data-target="#collapse41" aria-expanded="true">Recepción de pagos</div>
                                        </div>
                                        <div id="collapse41" class="collapsexxx show" data-parent="#accordionExample1" style="">
                                            <div class="card-body">
                                                <div class="row">
                                                  <div class="col-md-4">
                                                   <label>Fecha de pago *:</label>
                                                   <input type="datetime-local" name="Fechatimbre" id="Fechatimbre" class="form-control" value="<?php echo $Fecha;?>" style="background-color: white;" required>
                                                  </div>
                                                  <div class="col-md-4">
                                                    <label>Forma de pago *:</label>
                                                    <select id="FormaDePagoP" name="FormaDePagoP" class="form-control browser-default">
                                                      <?php foreach ($forma as $key) { ?>
                                                        <option value="<?php echo str_pad($key->id, 2, "0", STR_PAD_LEFT); ?>" <?php if($key->id==$formapago){echo 'selected';} ?> ><?php echo $key->formapago_text; ?></option>
                                                      <?php } ?>
                                                    </select>
                                                  </div>
                                                  <div class="col-md-4">
                                                   <label>Moneda *:</label>
                                                   <select id="ModedaP" name="ModedaP" class="form-control browser-default">
                                                     <option value="MXN">MXN Peso Mexicano</option>
                                                   </select>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-4">
                                                   <label>Monto *:</label>
                                                   <input type="number" name="Monto" id="Monto" class="form-control" value="" style="background-color: white;" required>
                                                  </div>
                                                  <div class="col-md-4">
                                                   <label>Total de Importes *:</label>
                                                   <div class="totalimport"></div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-4">
                                                   <label>Número de operación*:</label>
                                                   <input type="text" name="NumOperacion" id="NumOperacion" class="form-control" onpaste="return false;" style="background-color: white;" >
                                                  </div>
                                                  <div class="col-md-4">
                                                   <label>Cuenta Beneficiario:</label>
                                                   <input type="text" name="CtaBeneficiario" id="CtaBeneficiario" class="form-control" value="" style="background-color: white;" >
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title" data-toggle="collapsexxx" data-target="#collapse51" aria-expanded="true">Documentos relacionados</div>
                                        </div>
                                        <div id="collapse51" class="collapsexxx show" data-parent="#accordionExample1" style="">
                                            <div class="card-body">
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <a class="btn btn-primary" onclick="adddocumento(<?php echo $cliente.','.$Folio ?>)">Agregar nuevo</a>
                                                  </div>
                                                  <div class="col-md-12" style="padding: 0px;">
                                                    <table id="tabledocumentosrelacionados" class="table">
                                                      <tbody class="tabletbodydr">
                                                        <tr>
                                                          <td>
                                                            <div class="row">
                                                            <div class="col-md-3">
                                                              <input type="hidden" id="idfactura" class="form-control" value="<?php echo $FacturasId;?>"  readonly required>
                                                              <input type="hidden" id="MetodoDePagoDR" class="form-control" value="<?php echo $MetodoDePagoDR;?>"  readonly required>
                                                              <input type="hidden" id="doc_folio" class="form-control" value="<?php echo $Folio;?>"  readonly required>
                                                              <input type="hidden" id="doc_serie" class="form-control" value="<?php echo $serie;?>"  readonly required>
                                                             <label class="labeltable">Id del documento:</label>
                                                             <input type="text" id="IdDocumento" class="form-control IdDocumentos" value="<?php echo $uuid;?>" readonly required>
                                                            </div>
                                                            <div class="col-md-2" style="padding: 0px;">
                                                             <label class="labeltable">Número de parcialidad:</label>
                                                             <input type="text" id="NumParcialidad" class="form-control" value="<?php echo $copnum;?>"  required>
                                                            </div>
                                                            <div class="col-md-2" style="padding: 0px;">
                                                             <label class="labeltable" >Importe de saldo anterior:</label>
                                                             <input type="text" id="ImpSaldoAnt" class="form-control ImpSaldoAnt_<?php echo $Folio;?>" value="<?php echo $saldoanterior;?>" readonly required>
                                                            </div>
                                                            <div class="col-md-2" style="padding: 0px;">
                                                             <label class="labeltable">Importe de Pagado:</label>
                                                             <input type="text" id="ImpPagado" class="form-control ImpPagado ImpPagado_<?php echo $Folio;?>" value="0" oninput="calcularsinsoluto(<?php echo $Folio;?>)">
                                                            </div>
                                                            <div class="col-md-2">
                                                             <label class="labeltable">Importe de Pagado:</label>
                                                             <input type="text" id="ImpSaldoInsoluto" class="form-control ImpSaldoInsoluto_<?php echo $Folio;?>" value="<?php echo $saldoanterior;?>" readonly>
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                        </div>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  

                                </div>
                            </form>
                            <input type="hidden" name="totalimportes" id="totalimportes" value="">
                            <div class="row center fixed-action-btn" style="bottom: 100px; right: 19px;">
                              <a href="<?php echo base_url();?>Folios" class="btn btn-secondary" style="background: #fcb520;color: white;">Regresar</a>
                            </div>
                            <div class="row center fixed-action-btn" style="bottom: 50px; right: 19px;">
                              <a class="btn btn-primary" id="btn_savecomplemento_previe" style="background: #12264b;color: white;">Agregar complemento</a>
                            </div>
                        </div>
                    </div>
                <!--</div>                
            </div>-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<div class="modal fade" id="modaldocumentos" tabindex="-1" aria-labelledby="exampleModalSizeXl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Documentos del cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-12 listadodocumentos">
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_previefactura" tabindex="-1" aria-labelledby="exampleModalSizeXl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Preview Complemento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body preview_iframe">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;">Cerrar</button>
                <button type="button" class="btn btn-primary registrofac" id="btn_savecomplemento" style="background: #12264b;color: white;">Timbrar complemento</button>
            </div>
        </div>
    </div>
</div>