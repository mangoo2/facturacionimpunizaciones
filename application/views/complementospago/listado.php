<style type="text/css">
    .btns-factura{
        width: 170px;
    }
    .dataTables_filter label, .dataTables_paginate .pagination{
        float: right;
    }
    #tabla_complementos_filter{
        display: none;
    }
    #tabla_complementos td{
        padding-left: 9px !important;
        padding-right: 9px !important;
        font-size: 12px;
    }
</style>
<input type="hidden" id="carpetausuario" value="<?php echo $carpetausuario;?>">
<!--begin::Content-->

<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">  
                <div class="col-lg-4">
                    <div class="form-group row"> 
                        <div class="col-12"> 
                            <input class="form-control" type="search" id="searchtext" placeholder="Buscar complemento" oninput="search()"> 
                        </div> 
                    </div>
                </div> 
                <div class="col-lg-5" align="right">
                </div>       
                <div class="col-lg-3" align="right"> 
                    <a class="btn btn-danger font-weight-bold btn-pill btn-block" onclick="cancelarcomplementos()">Cancelar complemento</a>
                </div>    
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="tabla_complementos">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Folio</th>
                                        <th scope="col">Cliente</th>
                                        <th scope="col">RFC</th>
                                        <th scope="col">Monto</th>
                                        <th scope="col">Fecha</th>
                                        <th scope="col">Estatus</th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modalseleccioncorreo" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Seleccione un correo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Correos</label>
                        <select class="form-control" id="enviocorreo">
                            
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary font-weight-bold enviocorreomail">Enviar</button>
            </div>
        </div>
    </div>
</div>