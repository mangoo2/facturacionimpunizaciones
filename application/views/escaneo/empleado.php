<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <br>
                                        <br>
                                        <br>
                                        <h1>Empleado</h1>
                                        <h2 style="color: #026934 "><?php echo $get_emp->nombre?></h2>
                                        <br>
                                        <h1>Check point</h1>
                                        <h2 style="color: #026934 "><?php echo $check_points->nombre ?></h2>
                                        <div style="color: #4caf50; font-size: 70px;"><i class="fas fa-check-circle"></i></div>
                                        <br>
                                        <button type="button"  class="btn waves-effect waves-light btn-rounded btn_sistema" onclick="escaneo_camara(<?php echo $point ?>)">Regresar a escaneo</button>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>        