
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">  
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar vacuna" id="buscar_registro" oninput="reload_registro()">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-5"></div>   
                <div class="col-lg-3" align="right"> 
                    <a onclick="modal_producto()" class="btn btn-primary">Nueva vacuna</a>
                </div>    
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="tabla_datos"> 
                                <thead>
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Nombre de vacuna</th>
                                        <th scope="col">Precio</th>
                                        <th scope="col">Laboratorio</th>
                                        <td scope="col"></td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>


<!-- Modal-->
<div class="modal fade" id="modal_nuevo_producto" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nueva vacuna</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form_registro" method="post">
                    <input type="hidden" name="id" id="idproducto">
                    <div class="row">    
                        <!-- <div class="col-lg-12">
                            <div class="form-group">
                                <label>Código</label>
                                <input type="text" class="form-control" name="codigo" id="codigo">
                            </div>
                        </div> -->
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="nombre" id="nombre">
                            </div>
                        </div>
                        <!-- <div class="col-lg-10">
                            <div class="form-group">
                                <div class="categoria_txt"></div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label style="color: #3f425400;">Categoría</label>
                                <a onclick="modal_categoria()" class="btn btn-icon btn-circle btn_light_morado">
                                    <i class="icon-xl fas fa-plus" style="color: white"></i>
                                </a>
                            </div>
                        </div>     -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Precio</label>
                                <input type="number" class="form-control" name="precio" id="precio">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Laboratorio</label>
                                <input type="text" class="form-control" name="laboratorio" id="laboratorio">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Código</label>
                                <input type="text" class="form-control" name="codigo" id="codigo">
                            </div>
                        </div>
                       <!--  <div class="col-lg-6">
                            <div class="form-group">
                                <label>Stock</label>
                                <input type="number" class="form-control" name="stock" id="stock">
                            </div>
                        </div> -->
                    </div>        
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary btn_registro" onclick="guardar_registro()">Guardar</a> 
            </div>
        </div>
    </div>
</div>

<!-- Modal-->
<div class="modal fade" id="modal_canteogrias" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Categorías </h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">    
                    <div class="col-lg-10">
                        <form id="form_registro_cat" method="post">
                            <input type="hidden" name="id" id="idcategoria" value="0">
                            <div class="form-group">
                                <label>Categoría</label>
                                <input type="text" class="form-control" name="nombre" id="nombre_categoria">
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label style="color: #3f425400;">Categoría</label>
                            <a onclick="guardar_registro_cat()" class="btn btn-icon btn-circle btn_light_morado btn_registro_cat">
                                <i class="icon-xl fas fa-save" style="color: white"></i>
                            </a>
                        </div>
                    </div>    
                </div>
                <div class="row">    
                    <div class="col-lg-12">
                        <table class="table table-sm" > 
                            <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Nombre</th>
                                </tr>
                            </thead>
                            <tbody id="tabla_datos_cat">
                            </tbody>
                        </table>
                    </div>
                </div>         
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="elimina_registro_modal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmación </h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste registro? Esta operación no se puede deshacer.</h5>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" onclick="delete_registro()">Aceptar</a> 
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="elimina_registro_modal_cat" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmación </h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste registro? Esta operación no se puede deshacer.</h5>
            </div>
            <div class="modal-footer">
                <a class="btn btn_light_morado font-weight-bold btn-pill" onclick="delete_registro_cat()">Aceptar</a> 
            </div>
        </div>
    </div>
</div>