<style type="text/css">
    .star {
        visibility:hidden;
        font-size:30px;
        cursor:pointer;
        margin-top: 15px;
    }
    .star:before {
       content: "\2605";
       position: absolute;
       visibility:visible;
    }
    .star:checked:before {
       content: "\2605";
       position: absolute;
       color: #ffeb3b;
    }
    .star_f {
        visibility:hidden;
        font-size:30px;
        cursor:pointer;
        margin-top: 5px;
    }
    .star_f:before {
       content: "\2605";
       position: absolute;
       visibility:visible;
       margin-top: -19px;
    }
    .star_f:checked:before {
       content: "\2605";
       position: absolute;
       color: #ffeb3b;
       margin-top: -19px;
    }
    .dataTables_filter label, .dataTables_paginate .pagination{
        float: right;
    }
    .btns-factura{
        min-width: 180px;
    }
    .btn-retimbrar span i:active{
        color:#12264b;
    }
    #tabla_facturacion_filter{
        display: none;
    }
    #tabla_facturacion td{
        padding-left: 9px ;
        padding-right: 9px ;
        font-size: 12px;
    }
    .fac_inf{
        min-width: 100px;
        padding: 13px 6px;
    }
    .td_padding_0{
        padding: 0px !important;
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    .bt_add_depositos i{
        font-size: 15px;
    }
    .td_width_0{
        min-width: 154px;
    }
    .div_bad{
        text-align: center;
        padding: 5px;
        border-radius: 7px;
    }
    @media (max-width:550px){
        .container{
            padding: 0 5px;
        }
        .card-body{
            padding: 2rem 2rem;;
        }
        td{
            padding-left: 5px !important;
            padding-right: 5px !important;
            font-size: 10px;
        }
    }
</style>
<div  clas="row" style="display: none;">
 <input type="password" name="" >
  <input type="search" name="" > 
</div>
<input type="text" id="mes_a" value="<?php echo date('n') ?>">
<!--begin::Content-->
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col">
                    <div class="mb-3 row">
                        <div class="col-sm-4"> 
                            <div class="mb-3">
                                <input class="form-control" type="search" id="searchtext" placeholder="Buscar factura" oninput="search()"> 
                            </div>     
                        </div> 
                        <div class="col-sm-2"></div>
                        <div class="col-sm-3">
                            <div class="mb-3">

                                <a href="<?php echo base_url() ?>Timbrado" class="btn btn-primary btn-block">Nueva factura</a>
                            </div>    
                        </div>
                        <div class="col-sm-3">
                            <div class="mb-3">
                                <a class="btn btn-danger font-weight-bold btn-block" onclick="cancelarfacturas()">Cancelar factura</a>
                            </div>    
                        </div>
                    </div>
                </div>        
            </div>
            <div class="row">
                <div class="col">
                    <div class="mb-3 row"> 
                        <div class="col-sm-2" style="display:none;"> 
                            <div class="mb-3">
                                <label class="form-label">Seleccionar estatus</label>
                                <select class="form-control" id="estatus_v" onchange="loadtable()">
                                    <option value="0">Todas</option>
                                    <option value="1">Timbradas</option>
                                    <option value="2">Guardadas</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4"> 
                            <div class="mb-3">
                                <label class="form-label">Seleccionar cliente</label>
                                <select class="form-control" id="idcliente" onchange="loadtable()">
                                    <?php if(isset($_GET['id'])){ ?>
                                        <option value="<?php echo $_GET['id'];?>"><?php echo $_GET['name'];?></option>    
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">                 
                            <div class="mb-3">
                                <label class="form-label">Fecha inicio</label>
                                <input type="date" class="form-control" id="finicial" onchange="loadtable()">
                            </div>
                        </div>
                        <div class="col-sm-3">                 
                            <div class="mb-3">
                                <label class="form-label">Fecha Fin</label>
                                <input type="date" class="form-control" id="ffin" onchange="loadtable()">
                            </div>
                        </div>
                        <div class="col-sm-1" style="display:none;">                     
                            <div class="mb-3">
                                <input class="star form-control" type="checkbox" title="bookmark page" id="facturas_star" onchange="loadtable()">
                            </div>
                        </div>
                    </div>        
                </div>    
            </div>
            <div class="row" style="display: none;">
                <div class="col">
                    <div class="mb-3 row"> 
                        <div class="col-sm-3"> 
                            <select class="form-control" id="f1" >
                                <option value="0">Seleccionar mes</option>  
                                <option value="1">ENERO</option>
                                <option value="2">FEBRERO</option>
                                <option value="3">MARZO</option>
                                <option value="4">ABRIL</option>
                                <option value="5">MAYO</option>
                                <option value="6">JUNIO</option>
                                <option value="7">JULIO</option>
                                <option value="8">AGOSTO</option>
                                <option value="9">SEPTIEMBRE</option>
                                <option value="10">OCTUBRE</option>
                                <option value="11">NOVIEMBRE</option>
                                <option value="12">DICIEMBRE</option>
                            </select>
                        </div>
                        <div class="col-sm-3"> 
                            <select class="form-control" id="anio">
                                <option value="<?php echo date('Y') ?>"><?php echo date('Y') ?></option>
                                <option value="<?php echo 2023; ?>"><?php echo 2023; ?></option>
                            </select>    
                        </div>
                        <div class="col-sm-2"> 
                            <input type="date" class="form-control" id="fecha_exc" placeholder="Fecha"> 
                        </div>
                        <div class="col-sm-2"> 
                            <select class="form-control" id="laboratprio">
                                <option value="0">Seleccionar laboratorio</option>  
                                <?php 
                                    foreach ($get_laboratorios->result() as $y){ ?>
                                        <option value="<?php echo $y->laboratorio ?>"><?php echo $y->laboratorio ?></option>  
                                <?php } ?>
                            </select>
                        </div>    
                        <div class="col-sm-2"> 
                            <a onclick="get_excel()" style="cursor: pointer;"><img style="width: 38px;" src="<?php echo base_url() ?>public/img/excel.png"></a>
                        </div>
                    </div>
                </div>
            </div>
                         
            <!--begin::Dashboard-->
            <div class="card">
                <div class="card-body">
                    <div class="row">   
                        <div class="col">
                            <div class="mb-3 row">  
                                <div class="col-md-4"></div>
                                <div class="col-md-2">
                                    <label>Producto</label>
                                    <select class="form-control" id="pro_con" onchange="rango_producto()">
                                        <option></option>
                                        <?php 
                                            foreach ($productosh->result() as $item) {
                                                echo '<option>'.$item->nombre.'</option>'; 
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>Fecha Inicial</label>
                                    <input type="date" id="fe1" value="2024-03-25" oninput="rango_producto()" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Fecha Final</label>
                                    <input type="date" id="fe2" value="<?php echo date('Y-m-d') ?>" oninput="rango_producto()" class="form-control">
                                </div>
                                 <div class="col-md-2">
                                     <h3>Cantidad <span style="color:red" class="text_cantidad">0</span></h3>
                                     <h3>Total: <span style="color:red" class="text_unitario">$ 0.00</span></h3>
                                 </div>
                                <div class="col-md-12" style="text-align: end;">
                                    <a class="btn btn-success" onclick="descargarmetadata()">Descargar Metadara</a>
                                </div>
                                
                                <div class="col-sm-12" style="padding: 0px;">
                                    <table class="table table-sm" id="tabla_facturacion">
                                        <thead>
                                            <tr><th>Folio</th>
                                                <th></th>
                                                
                                                <th>Nombre / Razón social</th>
                                                <th>Medico</th>
                                                <th>RFC</th>
                                                <th>Monto</th>
                                                <th>Fecha</th>
                                                <th>Fecha Timbre</th>
                                                <th>Folio Fiscal</th>
                                                <th>Estatus</th>
                                                <th>Vacunas</th>
                                                <th></th>
                                                <th>Depositos</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>        
                    </div>
                    <div class="row">    
                        <div class="col-md-12 iframepdf" style="display: none;">
                            
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modal_productos" tabindex="-1" aria-labelledby="exampleModalSizeXl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Vacunas</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="tabla_vacunas"></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalcomplementos" tabindex="-1" aria-labelledby="exampleModalSizeXl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Documentos del cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <a class="btn btn-primary" onclick="addcomplemento()" style="background: #12264b;color: white;">Nuevo</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 listadocomplementos">
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalseleccioncorreo" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #1770aa;"><b>Correos a enviar</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" id="table_mail_envios">
                            <thead>
                                <tr>
                                    <th>Correo</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="enviocorreo">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;" onclick="$('#modalseleccioncorreo').modal('hide')">Cancelar</button>
                <button type="button" class="btn btn-primary font-weight-bold enviocorreomail" style="background: #12264b;color: white;">Enviar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modaldepositosfac" tabindex="-1" aria-labelledby="exampleModalSizeXl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Depósitos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('#modaldepositosfac').modal('hide')">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_depositos">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Fecha Depósito</label>
                            <input type="date" id="deposito" name="deposito" class="form-control" required>
                        </div>
                        <div class="col-md-3">
                            <label>Depositante</label>
                            <input type="text" id="depositante" name="depositante" class="form-control" required>
                        </div>
                        <div class="col-md-3">
                            <label>Monto</label>
                            <input type="number" id="monto" name="monto" class="form-control" required>
                        </div>
                        <div class="col-md-3">
                            <label>Clave de rastreo</label>
                            <input type="text" id="clave_rastreo" name="clave_rastreo" class="form-control" required>
                        </div>
                        <div class="col-md-12" style="text-align:end">
                            <a class="btn btn-success" onclick="add_depositos()">Agregar</a>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Fecha Deposito</th>
                                    <th>Depositante</th>
                                    <th>Monto</th>
                                    <th>Clave de rastreo</th>
                                    <th>Estatus</th>
                                    <th>Diferencia</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="tbody_dep_fac">
                                
                            </tbody>
                            
                        </table>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;" onclick="$('#modaldepositosfac').modal('hide')">Cerrar</button>
            </div>
        </div>
    </div>
</div>