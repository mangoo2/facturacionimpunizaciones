<?php
if($mes==0){
    $mes=$fecha;
}else{
    $mes=$mes;
}
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_".$mes."_".$anio.".xls");
/*
<th>Mes</th>
            <th><?php echo utf8_decode('Año') ?></th>
            <th>Folio</th>

            <td>
            <?php
                if($mes==1){
                    echo 'Enero';
                }else if($mes==2){
                    echo 'Febrero';
                }else if($mes==3){
                    echo 'Marzo';
                }else if($mes==4){
                    echo 'Abril';
                }else if($mes==5){
                    echo 'Mayo';
                }else if($mes==6){
                    echo 'Junio';
                }else if($mes==7){
                    echo 'Julio';
                }else if($mes==8){
                    echo 'Agosto';
                }else if($mes==9){
                    echo 'Septiembre';
                }else if($mes==10){    
                    echo 'Octubre';
                }else if($mes==11){
                    echo 'Noviembre';
                }else if($mes==12){
                    echo 'Diciembre';
                }
            ?>
            </td>
            <td><?php echo date('Y'); ?></td>
            <td><?php echo utf8_decode($x->Folio) ?></td>
    */        
?>

<table  border="1">
    <thead>
        <tr>
            
            <th>Nombre</th>
            <th><?php echo utf8_decode('RFC') ?></th>
            <th><?php echo utf8_decode('Código postal') ?> </th>
            <th><?php echo utf8_decode('Dirección') ?></th>
            <th><?php echo utf8_decode('Descripción') ?></th>
            <th><?php echo utf8_decode('Código') ?></th>
            <th>Laboratorio</th>
            <th>Cantidad</th>
            <th>Fecha</th>
        </tr> 
    </thead>
    <tbody>
        <?php foreach ($get_folios->result() as $x){ ?>
        <tr>
            <td><?php echo utf8_decode($x->Nombre) ?></td>
            <td><?php echo utf8_decode($x->rfc) ?></td>
            <td><?php echo utf8_decode($x->cp) ?></td>
            <td><?php echo utf8_decode($x->direccion) ?></td>
            <td><?php echo utf8_decode($x->Descripcion2) ?></td>
            <td><?php echo utf8_decode($x->codigo) ?></td>
            <td><?php echo utf8_decode($x->laboratorio) ?></td>
            <td><?php echo utf8_decode($x->Cantidad) ?></td>
            <td><?php echo date('d/m/Y',strtotime($x->fechatimbre)) ?></td>
        </tr>  
        <?php } ?>
    </tbody>
</table>
