<div class="page-body">
    <div class="container-fluid dashboard-default-sec">
        <div class="row">
              <div class="col-sm-6 col-xl-3 col-lg-6">
                <div class="card o-hidden border-0">
                  <div class="bg-primary b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center"><i data-feather="database"></i></div>
                      <div class="media-body"><span class="m-0">Timbres activos</span>
                        <h4 class="mb-0 counter"><?php echo $facturasdisponibles;?></h4><i class="icon-bg" data-feather="database"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xl-3 col-lg-6">
                <div class="card o-hidden border-0">
                  <div class="bg-secondary b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
                      <div class="media-body"><span class="m-0">Timbres ocupados</span>
                        <h4 class="mb-0 counter"><?php echo $timbresocupados;?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xl-3 col-lg-6">
                <div class="card o-hidden border-0">
                  <div class="bg-primary b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center"><i data-feather="message-circle"></i></div>
                      <div class="media-body"><span class="m-0">Timbres cancelados</span>
                        <h4 class="mb-0 counter"><?php echo $timbrescancelados;?></h4><i class="icon-bg" data-feather="message-circle"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xl-3 col-lg-6">
                <div class="card o-hidden border-0">
                  <div class="bg-primary b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center"><i data-feather="user-plus"></i></div>
                      <div class="media-body"><span class="m-0">Complementos de pago</span>
                        <h4 class="mb-0 counter"><?php echo $complementotimbradas;?></h4><i class="icon-bg" data-feather="user-plus"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>   
        <!-- --->
        <div class="col-xl-12 col-md-12 box-col-12">
            <div class="card">
              <div class="card-header pb-0">
                <h5>Número de facturas Realizadas por mes</h5>
              </div>
              <div class="card-body chart-block">
                <canvas id="chart_mes"></canvas>
              </div>
            </div>
        </div>
        <!-- --->
        <div class="col-xl-12 col-md-12 box-col-12">
            <div class="card">
              <div class="card-header pb-0">
                <h5>Ventas del año</h5>
              </div>
              <div class="card-body chart-block">
                <canvas id="chart_anio"></canvas>
              </div>
            </div>
        </div>
        <!-- ---> 
        <div class="row">
          <div class="col-sm-6 col-xl-6 col-lg-6">
            <div class="card">
              <div class="card-header">
                <div class="header-top d-sm-flex align-items-center">
                  <h5>Top 10 mejores médicos</h5>
                </div>
              </div>
              <div class="card-body p-0">
                <div id="chart_doctores_10"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-xl-6 col-lg-6">
            <div class="card">
              <div class="card-header">
                <div class="header-top d-sm-flex align-items-center">
                  <h5>Top 10 mejores médicos</h5>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4">
                    <label>Año</label>
                    <select  class="form-control round" id="fecha1" onchange="clientes_top_10()">
                      <?php
                        for($i=date('o'); $i>=2020; $i--){
                            if ($i == date('o'))
                                echo '<option value="'.$i.'" selected>'.$i.'</option>';
                            else
                                echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                        ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label>Mes</label>
                    <select class="form-control round" id="fecha2" onchange="clientes_top_10()">
                        <option value="">Selecciona opción</option> 
                        <option value="1" <?php if($mes==1) echo 'selected' ?>>ENERO</option>
                        <option value="2" <?php if($mes==2) echo 'selected' ?>>FEBRERO</option>
                        <option value="3" <?php if($mes==3) echo 'selected' ?>>MARZO</option>
                        <option value="4" <?php if($mes==4) echo 'selected' ?>>ABRIL</option>
                        <option value="5" <?php if($mes==5) echo 'selected' ?>>MAYO</option>
                        <option value="6" <?php if($mes==6) echo 'selected' ?>>JUNIO</option>
                        <option value="7" <?php if($mes==7) echo 'selected' ?>>JULIO</option>
                        <option value="8" <?php if($mes==8) echo 'selected' ?>>AGOSTO</option>
                        <option value="9" <?php if($mes==9) echo 'selected' ?>>SEPTIEMBRE</option>
                        <option value="10" <?php if($mes==10) echo 'selected' ?>>OCTUBRE</option>
                        <option value="11" <?php if($mes==11) echo 'selected' ?>>NOVIEMBRE</option>
                        <option value="12" <?php if($mes==12) echo 'selected' ?>>DICIEMBRE</option>
                    </select>
                  </div> 
                  <div class="col-md-12">
                    <div class="tabla_top_10_cliente"></div>
                  </div>
                </div>  
              </div>
            </div>
          </div>
        </div>
        <!-- ---> 
        <div class="row">
          <div class="col-sm-6 col-xl-6 col-lg-6">
            <div class="card">
              <div class="card-header">
                <div class="header-top d-sm-flex align-items-center">
                  <h5>Top  10 vacunas</h5>
                </div>
              </div>
              <div class="card-body p-0">
                <div id="chart_vacunas_10"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-xl-6 col-lg-6">
            <div class="card">
              <div class="card-header">
                <div class="header-top d-sm-flex align-items-center">
                  <h5>Top  10 vacunas</h5>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                   <div class="col-md-4">
                    <label>Año</label>
                    <select  class="form-control round" id="fechav1" onchange="vacunas_top_v_10()">
                      <?php
                        for($i=date('o'); $i>=2020; $i--){
                            if ($i == date('o'))
                                echo '<option value="'.$i.'" selected>'.$i.'</option>';
                            else
                                echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                        ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label>Mes</label>
                    <select class="form-control round" id="fechav2" onchange="vacunas_top_v_10()">
                        <option value="">Selecciona opción</option> 
                        <option value="1" <?php if($mes==1) echo 'selected' ?>>ENERO</option>
                        <option value="2" <?php if($mes==2) echo 'selected' ?>>FEBRERO</option>
                        <option value="3" <?php if($mes==3) echo 'selected' ?>>MARZO</option>
                        <option value="4" <?php if($mes==4) echo 'selected' ?>>ABRIL</option>
                        <option value="5" <?php if($mes==5) echo 'selected' ?>>MAYO</option>
                        <option value="6" <?php if($mes==6) echo 'selected' ?>>JUNIO</option>
                        <option value="7" <?php if($mes==7) echo 'selected' ?>>JULIO</option>
                        <option value="8" <?php if($mes==8) echo 'selected' ?>>AGOSTO</option>
                        <option value="9" <?php if($mes==9) echo 'selected' ?>>SEPTIEMBRE</option>
                        <option value="10" <?php if($mes==10) echo 'selected' ?>>OCTUBRE</option>
                        <option value="11" <?php if($mes==11) echo 'selected' ?>>NOVIEMBRE</option>
                        <option value="12" <?php if($mes==12) echo 'selected' ?>>DICIEMBRE</option>
                    </select>
                  </div> 
                  <div class="col-md-12">
                    <div class="tabla_top_10_vacunas"></div>
                  </div>
                </div>  
              </div>
            </div>
          </div>
        </div>
        <!---->    
    </div>
</div>
