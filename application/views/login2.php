<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="viho admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, viho admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url(); ?>public/img/favico2.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/img/favico2.png" type="image/x-icon">
    <title>FACT INMEX</title>
    <!-- Google font-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/feather-icon.css">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/style.css">
    <link id="color" rel="stylesheet" href="<?php echo base_url(); ?>/assetsapp/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/estilos.css">
    <input type="hidden" id="base_url" value="<?php echo base_url() ?>">
  </head>
  <style type="text/css">
    .btn-primary{
      background-color: #1770aa !important;
      border-color: #1770aa !important;
    }
    .icon{
      color: #2196f3 !important;
    }

    .alert_success {
        background-color: #1770aa;
        border-color: #1770aa;
        color: #fff;
    }
  </style>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="theme-loader">    
        <div class="loader-p"></div>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-xl-7 order-1"><img class="bg-img-cover bg-center" src="<?php echo base_url(); ?>public/img/covin.png" alt="looginpage"></div>
          <div class="col-xl-5 p-0">
            <div class="login-card">
              <form class="theme-form login-form needs-validation" id="sign_in" novalidate="">
                <div align="center">
                  <img class="img-fluid" src="<?php echo base_url()?>public/img/logo.png" alt=""></a>  
                </div>
                <br>
                <div class="form-group">
                  <label>Correo Electrónico</label>
                  <div class="input-group"><span class="input-group-text icon"><i class="icon-email"></i></span>
                    <input class="form-control" type="text" required="" name="usuario" autofocus>
                  </div>
                </div>
                <div class="form-group">
                  <label>Contraseña</label>
                  <div class="input-group"><span class="input-group-text icon"><i class="icon-lock"></i></span>
                    <input class="form-control" type="password" name="password" id="ingresar_login" required="">
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary btn-block" type="button" id="kt_btn_2"onclick="ingresar()" style="width: 100%">Ingresar</button>
                </div>
                <div class="alert alert_success mb-5 alert1" role="alert" style="display: none">
                    <h4 class="alert-heading" align="center">Inicio de sesión correcto</h4>
                    <p class="mb-0" align="center">Conectando a a base de datos correctamente</p>
                </div>
                <div class="alert alert-danger mb-5 alert2" role="alert" style="display: none">
                    <h4 class="alert-heading" align="center">Atención</h4>
                    <p class="mb-0" align="center">La contraseña o el usuario son incorrectos</p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- page-wrapper end-->
    <script>
      (function() {
      'use strict';
      window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
      }
      form.classList.add('was-validated');
      }, false);
      });
      }, false);
      })();
    </script>
    <!-- latest jquery-->
    <script src="<?php echo base_url(); ?>/assetsapp/js/jquery-3.5.1.min.js"></script>
    <!-- feather icon js-->
    <script src="<?php echo base_url(); ?>/assetsapp/js/icons/feather-icon/feather.min.js"></script>
    <script src="<?php echo base_url(); ?>/assetsapp/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="<?php echo base_url(); ?>/assetsapp/js/sidebar-menu.js"></script>
    <script src="<?php echo base_url(); ?>/assetsapp/js/config.js"></script>
    <!-- Bootstrap js-->
    <script src="<?php echo base_url(); ?>/assetsapp/js/bootstrap/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>/assetsapp/js/bootstrap/bootstrap.min.js"></script>
    <!-- Plugins JS start-->
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?php echo base_url(); ?>/assetsapp/js/script.js"></script>
    
    <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
    <script src="<?php echo base_url();?>plugins/jquery-validation/jquery.validate.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/global/plugins.bundle.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
    <script src="<?php echo base_url();?>assets/js/scripts.bundle.js"></script>
    <script src="<?php echo base_url();?>public/js/login.js"></script>
    <!-- login js-->
    <!-- Plugin used-->
  </body>
</html>