
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-6">
                    <h1>Cliente: Mangoo software</h1>
                    <h1>Lista de facturas</h1>
                </div>
                <div class="col-lg-6" align="right">
                    <h1>RFC: HERD890308UAA</h1>
                    <a class="btn btn_amarillo font-weight-bold btn-pill">Ver complementos de pago</a>
                </div>    
            </div>        
            <br>
            <div class="row">  
                <div class="col-lg-4">
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="search" placeholder="Buscar factura">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-6"></div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>
                </div>   
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th scope="col">Folio</th>
                                        <th scope="col">UUID</th>
                                        <th scope="col">Monto</th>
                                        <th scope="col">Fecha de emisión</th>
                                        <th scope="col">Emitió</th>
                                        <th scope="col">Estatus</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>8765</td>
                                        <td>655787665500</td>
                                        <td>$2,345.00</td>
                                        <td>12/10/2021</td>
                                        <td>Mangoo</td>
                                        <td>
                                            <a class="btn btn-success font-weight-bold btn-pill">Timbrada</a>
                                            <a class="btn btn-danger font-weight-bold btn-pill">Cancelada</a>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>