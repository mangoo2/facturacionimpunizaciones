
<style type="text/css">
    #uso_cfdi option:disabled {
        background: #e4e6ef;
    }
</style>
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-10"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>
                </div>   

            </div>  
            <br> 
            <!--begin::Dashboard-->
            <div class="card">
                    <form>
                        <input class="form-control" type="hidden" id="clienteId" value="0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3 row">
                                        <label class="col-sm-3">Nombre de cliente / Razón Social</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" id="razon_social">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label class="col-sm-3">RFC</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" id="rfc" onchange="validarrfc(this)">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label class="col-sm-3" title="Codigo Postal relacionado al RFC">Domicilio Fiscal Receptor</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" id="cp" name="cp" placeholder="Codigo Postal" title="Codigo Postal relacionado al RFC" required>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label class="col-sm-3" >RegimenFiscalReceptor</label>
                                        <div class="col-sm-9">
                                            <select id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" class="form-control" required onchange="v_rf()">';
                                                <option value="601" >601 General de Ley Personas Morales</option>
                                                <option value="603" >603 Personas Morales con Fines no Lucrativos</option>
                                                <option value="605" >605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                                                <option value="606" >606 Arrendamiento</option>
                                                <option value="607" >607 Régimen de Enajenación o Adquisición de Bienes</option>
                                                <option value="608" >608 Demás ingresos</option>
                                                <option value="609" >609 Consolidación</option>
                                                <option value="610" >610 Residentes en el Extranjero sin Establecimiento Pe</option>
                                                <option value="611" >611 Ingresos por Dividendos (socios y accionistas)</option>
                                                <option value="612" >612 Personas Físicas con Actividades Empresariales</option>
                                                <option value="614" >614 Ingresos por intereses</option>
                                                <option value="615" >615 Régimen de los ingresos por obtención de premios</option>
                                                <option value="616" >616 Sin obligaciones fiscales</option>
                                                <option value="620" >620 Sociedades Cooperativas de Producción que optaingresos</option>
                                                <option value="621" >621 Incorporación Fiscal</option>
                                                <option value="622" >622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                                                <option value="623" >623 Opcional para Grupos de Sociedades</option>
                                                <option value="624" >624 Coordinados</option>
                                                <option value="625" >625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas</option>
                                                <option value="626" >626 Régimen Simplificado de Confianza</option>
                                                <option value="628" >628 Hidrocarburos</option>
                                                <option value="629" >629 De los Regímenes Fiscales Preferentes Multinacionales</option>
                                                <option value="630" >630 Enajenación de acciones en bolsa de valores</option>      
                                            </select>
                                        </div>
                                    </div>
                                    <div class=" row"> 
                                        <label class="col-sm-3" >Uso cfdi</label>
                                        <div class="col-9"> 
                                            <select class="form-control" id="uso_cfdi" name="uso_cfdi" required> 
                                                <option></option> 
                                                <?php foreach ($uso_cfdi->result() as $item) { ?> 
                                                    <option value="<?php echo $item->uso_cfdi;?>" class="pararf <?php echo str_replace(',',' ',$item->pararf) ?>" disabled ><?php echo $item->uso_cfdi_text;?></option> 
                                                <?php } ?> 
                                            </select> 
                                        </div> 
                                    </div> 
                                    <div class="form-group ">
                                        <table class="table" id="table_mail">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        <a class="btn btn-primary" style="width: 125px;float: right;" onclick="addemail(0,'')">Agregar Email</a>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody class="tableemailtb">
                                                
                                            </tbody>
                                            
                                        </table>
                                        
                                          
                                    </div>
                                    <div class="mb-3 row">
                                        <label class="col-sm-3">Agregar dirección completa</label>
                                        <div class="col-sm-3">
                                            <span class="switch switch_morado switch-icon">
                                                <label>
                                                    <input type="checkbox" id="check_direccion" onclick="checkdireccion()">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="col-sm-2"></div>
                                        <label class="col-sm-3">Agregar datos bancarios</label>
                                        <div class="col-sm-1">
                                            <span class="switch switch_morado switch-icon">
                                                <label>
                                                    <input type="checkbox" id="check_datos_bancario" onclick="checkbanco()">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="direccion" style="display: none;">
                                                <label>Dirección</label>
                                                <textarea class="form-control" id="direccion"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="datosbancarios" style="display: none;">
                                                <label>Banco</label>
                                                <input class="form-control" type="text" id="banco">
                                                <label>Número cuenta</label>
                                                <input class="form-control" type="text" id="numero_cuenta">
                                                <label>Clabe</label>
                                                <input class="form-control" type="text" id="clabe">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                        </div>
                        
                    </form>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4" align="center">
                                <button  class="btn btn-primary savecliente">Guardar cliente</button>
                            </div>
                        </div>
                    </div>       
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>