<style type="text/css">
    .dataTables_filter label, .dataTables_paginate .pagination{
        float: right;
    }
    #tabla_clientes_filter{
        display: none;
    }
</style>
<!--begin::Content-->

<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row"> 
                <div class="col-lg-4">
                    <div class="form-group row"> 
                        <div class="col-12"> 
                            <input class="form-control" type="search" id="searchtext" placeholder="Buscar Cliente" oninput="search()"> 
                        </div> 
                    </div>
                </div> 
                <div class="col-lg-4"></div>
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Timbrado/add" class="btn btn-primary" style="margin-bottom: 10px; width: 100%">Nueva factura</a>
                </div>
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Clientes/registro" class="btn btn-primary" style="margin-bottom: 10px; width: 100%">Nuevo cliente</a>
                </div>    
            </div>  
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="row">    
                        <div class="col-lg-12">
                            <table class="table table-sm" id="tabla_clientes">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th scope="col">Nombre / Razón social</th>
                                        <th scope="col">Código postal</th>
                                        <th scope="col">RFC</th>
                                        <th scope="col">Dirección</th>
                                        <th scope="col">Facturas</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    

                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>