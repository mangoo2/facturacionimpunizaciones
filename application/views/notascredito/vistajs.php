<link href="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
<script src="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/jquery-ui.css">

<script src="<?php echo base_url();?>public/js/notascredito.js?v=<?php echo date('YmdGis') ?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		<?php if($notaId>0){ ?>
			obtenerdatosnota(<?php echo $notaId;?>);
			
		<?php }else{ ?>
			agregarconceptos();
		<?php } ?>
	});
</script>