<style type="text/css">
    .select2-results__option{
        padding-left: 5px !important;
        padding-right: 5px !important;
    }
    .totalesright{
        text-align: right;

    }
    .totalesborder{
        border-bottom: 0px !important;
        border-top: 0px !important;
    }
    .inputconceptos{
        width: 100px;
        border:0px;
    }
    .error{
        color: red;
    }
    .ifrafac{
        width: 100%;
        height: 400px;
        border: 0px;
    }
    #modal_timbrado .modal-body{
        padding: 0px;
    }
    .form-control[readonly],#regimenf,#TipoDeComprobante,#usofactura,#TipoRelacion {
        background-color: #eaeaea;
    }
</style>
<!--begin::Content-->

<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-10" align="right"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Clientes" class="btn btn-primary">Regresar</a>
                </div>    
            </div>  
            <br>
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body" style="padding-left: 15px; padding-top: 0px;  padding-right: 15px;">
                    <form id="validateSubmitForm" method="post" autocomplete="off">
                            <input type="hidden" name="notaId" value="<?php echo $notaId;?>" readonly>
                            <input type="hidden" name="FacturasId" value="<?php echo $facturaId;?>" readonly>
                        <div class="card-body" style="padding-left: 10px;    padding-right: 10px;">
                            <h4>Datos del emisor</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label" align="right">RFC:</label>
                                    <input type="text" name="rfcemisor" id="rfcemisor" class="form-control" value="<?php echo $rrfc;?>" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label" align="right">Razon social:</label>
                                    <input type="text" name="rasonsocialemisor" id="rasonsocialemisor" class="form-control" value="<?php echo $Nombrerasonsocial;?>" readonly>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label" align="right">Regimen fiscal:</label>
                                    <select name="regimenf" id="regimenf" class="form-control">
                                        <?php echo $regimenf; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label" align="right">Tipo de factura:</label>
                                    <select name="TipoDeComprobante" id="TipoDeComprobante" class="form-control">
                                        <option value="E">E Egreso</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <h4>Datos del receptor</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label">Rason Social</label>
                                    <input type="text" class="form-control" name="nombrerasonsocialreceptor" id="nombrerasonsocialreceptor" value="<?php echo $nombrerasonsocialreceptor;?>" readonly>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label">RFC</label>
                                    <input type="text" class="form-control" name="rfcreceptor" id="rfcreceptor" value="<?php echo $rfcreceptor;?>" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label">Uso de Factura</label>
                                    <select class="form-control" name="usofactura" id="usofactura">
                                        <option value="G02">G02 Devoluciones,descuentos o bonificaciones</option>
                                    </select>
                                    
                                </div>
                            </div>
                            <br>
                            <h4>Comprobante</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-form-label">Fecha y hora de expedición</label>
                                    <input type="datetime" class="form-control" name="fecha" id="fecha" value="<?php echo $fecha;?>" readonly>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-form-label">Código postal</label>
                                    <input type="number" class="form-control" name="LugarExpedicion" id="$this->fechahoyc" value="<?php echo $LugarExpedicion;?>">
                                </div>
                                <div class="col-md-4">
                                    <label class="col-form-label">Moneda</label>
                                    <select class="form-control" name="moneda" id="moneda">
                                        <option value="MXN">MXN Peso Mexicano</option>
                                        <option value="USD">USD Dolar Americano</option>
                                        <option value="EUR">EUR Euro</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-form-label">Forma de pago</label>
                                    <select name="formapago" id="formapago" class="form-control">
                                        <?php 
                                            foreach ($formapago->result() as $item) { ?>
                                            <option value="<?php echo $item->formapago;?>"><?php echo $item->formapago_text;?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-form-label">Método de pago</label>
                                    <select name="metodopago" id="metodopago" class="form-control">
                                        <?php 
                                            foreach ($metodopago->result() as $item) { ?>
                                            <option value="<?php echo $item->metodopago;?>"><?php echo $item->metodopago_text;?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label class="col-form-label">Condiciones de pago</label>
                                    <input type="text" name="CondicionesDePago" id="CondicionesDePago" class="form-control">
                                </div>
                            </div>
                            <br>
                            <h4>Factura Relacionada</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-5">
                                    <label class="col-form-label">Tipo de relación</label>
                                    <select name="TipoRelacion" id="TipoRelacion" class="form-control">
                                        
                                        <option value="01>">01 Nota de crédito de los documentos relacionados</option>
                                        
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label class="col-form-label">Folio</label>
                                    <input type="text" class="form-control" value="<?php echo $Folio_relacionado;?>" readonly>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-form-label">Folio Fiscal</label>
                                    <input type="text" name="uuid_relacionado" id="uuid_relacionado" class="form-control" value="<?php echo $uuid_relacionado;?>" readonly>
                                </div>
                            </div>

                        </div> 
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table" id="table-conceptos">
                                <tbody class="tbody-table-conceptos">
                                    
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-3"></div>
                            <div class="col-3">
                                <button class="btn btn-primary registrofac_save" onclick="">Guardar factura</button>
                            </div>
                            <div class="col-3">
                                <button class="btn btn-primary registrofac_preview" >Timbrar</button>
                            </div>
                        </div>
                    </div>
                    
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<div class="modal fade" id="modal_timbrado" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body preview_iframe">
                
                
            </div>
            <div class="modal-footer">
                   
                        <button type="button" class="btn btn_light_morado font-weight-bold btn-pill registrofac">Timbrar</button>
                    
                        <button type="button" class="btn btn_light_morado font-weight-bold btn-pill" data-dismiss="modal">Regresar</button>
                    
            </div>
        </div>
    </div>
</div>