<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Guanábana Cine</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>images/gc2.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url();?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url();?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url();?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">
    <script>
        var base_url="<?php echo base_url(); ?>";
    </script>
</head>
<style type="text/css">
    .bg-blue{
        background-color: #026934 !important;
    }
    .login-page {
        background-color: #ffffff !important;
    }
    .cx3 {
        background-color: rgb(2 105 52) !important;
    }
    .cx2 {
        background-color: rgb(201 226 177) !important;
    }
    .col-blue {
        color: #c9e2b1 !important;
    }
</style>
<body class="login-page">
    <div class="sesion_texto" style="width: 400px;
    height: 400px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -200px;
    margin-top: -200px;
    z-index: 1;
    display: none;" align="center">
        <!-- style="background: #ffffffad; border-radius: 20px; c9e2b1 " 
        
        -->   
    <img style="" src="<?php echo base_url();?>images/gc.png" width="50%" class="m-t-90"  />
    
    <h2 style="color: white;">Iniciando Sesión</h2>
    <h3 style="color: white;">Conectado a la base de datos</h3>
    </div>
    <div class="cx3"></div>
    <div class="cx2"></div>
    <div class="cx1"></div>
    
    <div class="login-box">
        <div class="logo">
            <br>
        </div>
        <div class="card login_texto">
            <div class="body">
                <form id="sign_in" method="POST">
                    <div class="row align-center">
                        <div class="col-md-6 " >
                           <img src="<?php echo base_url();?>images/gc.png" width="70%" class="m-t-90" />
                           <h2 style="color: #026934">Sistema de registro</h2>
                        </div>
                        <div class="col-md-6" style="border-left: 1px solid #000;">
                            <i class="material-icons font-40 m-t-20 col-blue">lock</i>
                            <h2>Accede a tu cuenta</h2>
                            <h5>Estamos contentos de que regreses</h5>
                            <br>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="usuario" placeholder="Nombre de Usuario" required autofocus>
                                </div>
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" id="ingresar_login" placeholder="Contraseña" required>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-xs-12">
                                    <button class="btn btn-block bg-blue waves-effect" type="submit">INICIAR SESIÓN</button>
                                </div>
                            </div>
                            <br>
                            <div class="alert bg-pink" id="error">
                                <i class="material-icons ">error</i> <strong>Error!</strong> El nombre de usuario y/o contraseña son incorrectos 
                            </div>
                            <?php /*
                            <p class="align-center"><span style="color:#7d939a;">¿Olvidaste tu contraseña?</span> 
                                <a id="resetPass" ><span class="textEleonor">Restablécela aquí</span></a></p>
                            <p class="align-center"><a href="/register/medic" id="registerLink" class="rastreable" ><span style="color:#7d939a;">¿Aún no tienes una cuenta? </span><span class="textEleonor">Regístrate</span> </a></p>
                            */ 
                            ?>
                        </div>
                    </div>
                </form>
            </div>
        <div>    
    </div>
    </div>
    <div class="text-center" id="loader">
        <div class="lds-ripple"><div></div><div></div></div>
    </div>
    <!--
    <div class="alert bg-light-green" id="success">
        <i class="material-icons ">done</i> <strong>Acceso Correcto!</strong> Será redirigido al sistema 
    </div>
    -->
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url();?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url();?>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url();?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url();?>public/js/login.js"></script>
</body>

</html>