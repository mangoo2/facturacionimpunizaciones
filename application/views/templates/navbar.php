<?php
$color='#026934';
if (!$this->session->userdata('logeado')) {
    redirect('Login');
    $perfilid =0;
}else{
    $perfilid=$this->session->userdata('perfilid');
    $personalId=$this->session->userdata('idpersonal');
    $foto=$this->session->userdata('foto');
    $this->fechainicio = date('Y-m-d');
}

?>
                    
<div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
      <div class="page-main-header">
        <div class="main-header-right row m-0">
          <div class="main-header-left">
            <div class="logo-wrapper"><a href="<?php echo base_url() ?>Inicio"><img style="width: 51px;" class="img-fluid" src="<?php echo base_url()?>public/img/logo.png" alt=""></a></div>
            <div class="dark-logo-wrapper"><a href="index.html"><img style="width: 51px;" class="img-fluid" src="<?php echo base_url()?>public/img/logo.png" alt=""></a></div>
            <div class="toggle-sidebar"><i class="status_toggle middle" data-feather="align-center" id="sidebar-toggle"></i></div>
          </div>
          <div class="left-menu-header col" style=" display: none;">
            <ul>
              <li style="width: 100%;">
                <div class="form-inline search-form" style="margin-top: 0em; margin-block-end: 1em;">
                  <div class="search-bg"><i class="fa fa-search"></i>
                    <!-- <input style="display:none" type="text" name="falsocodigo" autocomplete="nope" /> -->
                    <input class="form-control-plaintext" autocomplete="new-text" autocomplete="nope" placeholder="Buscar médico o factura" id="idclientes" oninput="clientes_buscar(0)">
                  </div>
                </div><span class="d-sm-none mobile-search search-bg"><i class="fa fa-search"></i></span>
              </li>
            </ul>
            <div style="position: relative;">
                <div class="cliente_buscar_t" style="position: absolute; top: 25px; z-index: 3;"></div>
            </div>
          </div>
          <div class="nav-right col pull-right right-menu p-0">
            <ul class="nav-menus">
              <!-- <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
              <li class="onhover-dropdown">
                <div class="bookmark-box"><i data-feather="star"></i></div>
                <div class="bookmark-dropdown onhover-show-div">
                  <div class="form-group mb-0">
                    <div class="input-group">
                      <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                      <input class="form-control" type="text" placeholder="Search for bookmark...">
                    </div>
                  </div>
                  <ul class="m-t-5">
                    <li class="add-to-bookmark"><i class="bookmark-icon" data-feather="inbox"></i>Email<span class="pull-right"><i data-feather="star"></i></span></li>
                    <li class="add-to-bookmark"><i class="bookmark-icon" data-feather="message-square"></i>Chat<span class="pull-right"><i data-feather="star"></i></span></li>
                    <li class="add-to-bookmark"><i class="bookmark-icon" data-feather="command"></i>Feather Icon<span class="pull-right"><i data-feather="star"></i></span></li>
                    <li class="add-to-bookmark"><i class="bookmark-icon" data-feather="airplay"></i>Widgets<span class="pull-right"><i data-feather="star">   </i></span></li>
                  </ul>
                </div>
              </li>
              <li class="onhover-dropdown">
                <div class="notification-box"><i data-feather="bell"></i><span class="dot-animated"></span></div>
                <ul class="notification-dropdown onhover-show-div">
                  <li>
                    <p class="f-w-700 mb-0">You have 3 Notifications<span class="pull-right badge badge-primary badge-pill">4</span></p>
                  </li>
                  <li class="noti-primary">
                    <div class="media"><span class="notification-bg bg-light-primary"><i data-feather="activity"> </i></span>
                      <div class="media-body">
                        <p>Delivery processing </p><span>10 minutes ago</span>
                      </div>
                    </div>
                  </li>
                  <li class="noti-secondary">
                    <div class="media"><span class="notification-bg bg-light-secondary"><i data-feather="check-circle"> </i></span>
                      <div class="media-body">
                        <p>Order Complete</p><span>1 hour ago</span>
                      </div>
                    </div>
                  </li>
                  <li class="noti-success">
                    <div class="media"><span class="notification-bg bg-light-success"><i data-feather="file-text"> </i></span>
                      <div class="media-body">
                        <p>Tickets Generated</p><span>3 hour ago</span>
                      </div>
                    </div>
                  </li>
                  <li class="noti-danger">
                    <div class="media"><span class="notification-bg bg-light-danger"><i data-feather="user-check"> </i></span>
                      <div class="media-body">
                        <p>Delivery Complete</p><span>6 hour ago</span>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
              <li>
                <div class="mode"><i class="fa fa-moon-o"></i></div>
              </li>
              <li class="onhover-dropdown"><i data-feather="message-square"></i>
                <ul class="chat-dropdown onhover-show-div">
                  <li>
                    <div class="media"><img class="img-fluid rounded-circle me-3" src="../assets/images/user/4.jpg" alt="">
                      <div class="media-body"><span>Ain Chavez</span>
                        <p class="f-12 light-font">Lorem Ipsum is simply dummy...</p>
                      </div>
                      <p class="f-12">32 mins ago</p>
                    </div>
                  </li>
                  <li>
                    <div class="media"><img class="img-fluid rounded-circle me-3" src="../assets/images/user/1.jpg" alt="">
                      <div class="media-body"><span>Erica Hughes</span>
                        <p class="f-12 light-font">Lorem Ipsum is simply dummy...</p>
                      </div>
                      <p class="f-12">58 mins ago</p>
                    </div>
                  </li>
                  <li>
                    <div class="media"><img class="img-fluid rounded-circle me-3" src="../assets/images/user/2.jpg" alt="">
                      <div class="media-body"><span>Kori Thomas</span>
                        <p class="f-12 light-font">Lorem Ipsum is simply dummy...</p>
                      </div>
                      <p class="f-12">1 hr ago</p>
                    </div>
                  </li>
                  <li class="text-center"> <a class="f-w-700" href="javascript:void(0)">See All     </a></li>
                </ul>
              </li> -->
              <li class="onhover-dropdown p-0">
                <button class="btn btn-primary-light" type="button"><a href="<?php echo base_url();?>Login/logout"><i data-feather="log-out"></i>Salir</a></button>
              </li>
            </ul>
            
          </div>
          <div class="d-lg-none mobile-toggle pull-right w-auto"><i data-feather="more-horizontal"></i></div>
        </div>
      </div>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper sidebar-icon">
        <!-- Page Sidebar Start-->
        <header class="main-nav">
          <div class="sidebar-user text-center"><a class="setting-primary" href="javascript:void(0)"><i data-feather="settings"></i></a><img class="img-90 rounded-circle" src="<?php echo base_url()?>public/img/profpic.png" alt="">
            <div class="badge-bottom"><span class="badge badge-primary">New</span></div><a href="user-profile.html">
              <h6 class="mt-3 f-14 f-w-600">Super Administrador</h6></a>

          </div>
          <nav>
            <div class="main-navbar">
              <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
              <div id="mainnav">           
                <ul class="nav-menu custom-scrollbar">
                  <li class="back-btn">
                    <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                  </li>
                  <?php 
                  $menu=$this->Login_model->getMenus($perfilid);
                  $menu_aux=0;
                  foreach ($menu as $items) { 
                      $MenuId=intval($items->MenuId);
                      //var_dump($btn_active);die;
                      if($MenuId==$btn_active){
                         $activar='active';
                      }else{
                         $activar=''; 
                      }
                      ?>

                  <li class="sidebar-main-title">
                    <div>
                      <h6><?php echo $items->Nombre ?></h6>
                    </div>
                  </li>
                    <?php $menusub=$this->Login_model->submenus($perfilid,$items->MenuId);
                    foreach ($menusub as $items){ 
                        $MenuId_sub=intval($items->MenusubId);                                                        
                        if($MenuId_sub==$btn_active_sub){
                           $activar_sub='menu-item-active';
                        }else{
                           $activar_sub=''; 
                        }
                        ?>
                        <li class="dropdown"><a class="nav-link menu-title link-nav" href="<?php echo base_url()?><?php echo $items->Pagina ?>"><i data-feather="<?php echo $items->Icon ?>"></i> <span><?php echo $items->Nombre ?></span></a></li>
                    <?php } ?> 
                  <?php 
                      $menu_aux++;
                  }
                  ?>
                </ul>  
              </div>
              <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
            </div>
          </nav>
        </header>
        <!-- Page Sidebar Ends-->
        
        <!-- footer start-->
        