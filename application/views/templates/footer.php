<footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0"> <?php echo date('Y') ?> Mangoo Software todos los derechos reservados.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Hand crafted & made with <i class="fa fa-heart font-secondary"></i></p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>

        <script src="<?php echo base_url();?>assetsapp/js/jquery-3.5.1.min.js"></script>
        <!-- feather icon js-->
        <script src="<?php echo base_url();?>assetsapp/js/icons/feather-icon/feather.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/icons/feather-icon/feather-icon.js"></script>
        <!-- Sidebar jquery-->
        <script src="<?php echo base_url();?>assetsapp/js/sidebar-menu.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/config.js"></script>
        <!-- Bootstrap js-->
        <script src="<?php echo base_url();?>assetsapp/js/bootstrap/popper.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/bootstrap/bootstrap.min.js"></script>
        <!-- Plugins JS start-->
        <script src="<?php  // echo base_url();?>assetsapp/js/chart/chartist/chartist.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/chart/chartist/chartist-plugin-tooltip.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/chart/knob/knob.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/chart/knob/knob-chart.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/chart/apex-chart/apex-chart.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/chart/apex-chart/stock-prices.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/prism/prism.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/clipboard/clipboard.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/counter/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/counter/jquery.counterup.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/counter/counter-custom.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/custom-card/custom-card.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/notify/bootstrap-notify.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/vector-map/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/vector-map/map/jquery-jvectormap-world-mill-en.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/vector-map/map/jquery-jvectormap-us-aea-en.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/vector-map/map/jquery-jvectormap-uk-mill-en.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/vector-map/map/jquery-jvectormap-au-mill.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/vector-map/map/jquery-jvectormap-chicago-mill-en.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/vector-map/map/jquery-jvectormap-in-mill.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/vector-map/map/jquery-jvectormap-asia-mill.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/dashboard/default.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/notify/index.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/datepicker/date-picker/datepicker.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/datepicker/date-picker/datepicker.en.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/datepicker/date-picker/datepicker.custom.js"></script>
        <!-- Plugins JS Ends-->
        <!-- Theme js-->
        <script src="<?php echo base_url();?>assetsapp/js/chart/chartjs/chart.min.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/chart/chartjs/chart.custom.js"></script>
        <script src="<?php echo base_url();?>assetsapp/js/script.js"></script>
        <!-- <script src="<?php // echo base_url();?>assetsapp/js/theme-customizer/customizer.js"></script> -->

         
    
        
        <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
        
        <script src="<?php echo base_url();?>assets/plugins/global/plugins.bundle.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
        <script src="<?php echo base_url();?>assets/js/scripts.bundle.js"></script>
        <!--end::Global Theme Bundle-->
        <!--begin::Page Vendors(used by this page)-->
        <script src="<?php echo base_url();?>assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
        <!--end::Page Vendors-->
        <!--begin::Page Scripts(used by this page)-->
        <script src="<?php echo base_url();?>assets/js/pages/widgets.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/custom/datatables/datatables.bundle.js"></script>
        <script src="<?php echo base_url(); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?php echo base_url();?>public/js/footer.js?v=<?php echo date('YmdGis') ?>"></script>

    </body>
    <!--end::Body-->
</html>
