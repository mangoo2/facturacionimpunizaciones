<html lang="es">
    <!--begin::Head-->
    <head><base href="">
        <meta charset="utf-8" />
        <link rel="icon" href="<?php echo base_url(); ?>public/img/favico2.png" type="image/x-icon">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>public/img/favico2.png" type="image/x-icon">
        <title>FACT INMEX</title>
        <meta name="description" content="Updates and statistics" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- <link rel="canonical" href="https://keenthemes.com/metronic" />

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

        <link href="<?php // echo base_url();?>assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

        <link href="<?php // echo base_url();?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php // echo base_url();?>assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php // echo base_url();?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="<?php // echo base_url();?>public/img/logo_grapas.png" /> -->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/fontawesome.css">
      
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/icofont.css">
 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/themify.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/flag-icon.css">
   
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/feather-icon.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/animate.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/chartist.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/date-picker.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/prism.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/vector-map.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/bootstrap.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/style.css">
        <link id="color" rel="stylesheet" href="<?php echo base_url(); ?>/assetsapp/css/color-1.css" media="screen">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assetsapp/css/responsive.css">
        




        <link href="<?php echo base_url();?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />



        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
    <!--end::Head-->
    <!--begin::Body-->
    <style type="text/css">
        .vd_red {
            color: red;
        }
        .vd_green {
            color: green;
        }
        label{
            color: #1770aa;
        }
        .proselected.selected {
            background-image: linear-gradient(177deg, #ff8d42, #ff8d42);
        }
    </style>

    <body>
        <div class="loader-wrapper">
          <div class="theme-loader">    
            <div class="loader-p"></div>
          </div>
        </div>
