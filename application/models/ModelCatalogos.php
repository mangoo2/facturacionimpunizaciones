<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelCatalogos extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }


    function get_empleados($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'u.Usuario',
            3=>'p.check_baja',
            4=>'p.fechabaja',
            5=>'p.motivo',
            6=>'per.nombre as perfil'
        );
        $columnsss = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'u.Usuario',
            3=>'p.check_baja',
            4=>'p.fechabaja',
            5=>'p.motivo',
            6=>'per.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId');
        $this->db->join('perfiles per','per.perfilId=u.perfilId');

        $where = array('p.estatus'=>1);
        $this->db->where($where);
        $this->db->where('p.personalId!=1');
        if($params['empleado']!=''){
            $this->db->like('p.nombre',$params['empleado']);
            $this->db->or_like('p.Usuario',$params['empleado']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_empleados($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'u.Usuario',
            3=>'p.check_baja',
            4=>'p.fechabaja',
            5=>'p.motivo',
            6=>'per.nombre as perfil'
        );
        $columnsss = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'u.Usuario',
            3=>'p.check_baja',
            4=>'p.fechabaja',
            5=>'p.motivo',
            6=>'per.nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId');
        $this->db->join('perfiles per','per.perfilId=u.perfilId');

        $where = array('p.estatus'=>1);
        $this->db->where($where);
        $this->db->where('p.personalId!=1');
        if($params['empleado']!=''){
            $this->db->like('p.nombre',$params['empleado']);
            $this->db->or_like('p.Usuario',$params['empleado']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    ///===============!!
    function get_resporte($params){
        $columns = array( 
            0=>'cpd.id',
            1=>'DATE_FORMAT(cpd.reg,  "%d / %m / %Y  %r" ) AS reg',
            2=>'p.nombre AS empleado',
            3=>'cp.nombre AS point'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $columns2 = array( 
            0=>'cpd.id',
            1=>'cpd.reg',
            2=>'p.nombre',
            3=>'cp.nombre'
        );
        $this->db->select($select);
        $this->db->from('check_points_detalles cpd');
        $this->db->join('personal p','p.personalId=cpd.personalId','left');
        $this->db->join('check_points cp','cp.id=cpd.idpoints_check','left');
        if($params['fecha_inicio']!='' && $params['fecha_fin']!=''){
            $this->db->where('cpd.reg BETWEEN "'.$params['fecha_inicio'].' 00:00:00" AND "'.$params['fecha_fin'].' 23:59:59"');
        }
        if($params['points_check']!=0){
             $this->db->where('cpd.idpoints_check='.$params['points_check']);   
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_reporte($params){
        $columns = array( 
            0=>'cpd.id',
            1=>'cpd.reg',
            2=>'p.nombre',
            3=>'cp.nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('check_points_detalles cpd');
        $this->db->join('personal p','p.personalId=cpd.personalId','left');
        $this->db->join('check_points cp','cp.id=cpd.idpoints_check','left');
        if($params['fecha_inicio']!='' && $params['fecha_fin']!=''){
            $this->db->where('cpd.reg BETWEEN "'.$params['fecha_inicio'].' 00:00:00" AND "'.$params['fecha_fin'].' 23:59:59"');
        }
        if($params['points_check']!=0){
             $this->db->where('cpd.idpoints_check='.$params['points_check']);   
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /////////////////////////////// Guanabana
    //=================================================
    function get_check_points($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
            2=>'codigo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('check_points');
        $where = array('activo'=>1);
        $this->db->where($where);
        if($params['points']!=''){
            $this->db->like('nombre',$params['points']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_check_points($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
            2=>'codigo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('check_points');
        $where = array('activo'=>1);
        $this->db->where($where);
        if($params['points']!=''){
            $this->db->like('nombre',$params['points']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    //////////////////////// Hules y grapas
    function get_unidades($params){
        $columns = array( 
            0=>'UnidadId',
            1=>'Clave',
            2=>'nombre',
            3=>'status'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('unidades');
        if($params['buscar_registro']!=''){
            $this->db->like('Clave',$params['buscar_registro']);
            $this->db->or_like('nombre',$params['buscar_registro']);
        }
        $this->db->order_by('status', "desc");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_unidades($params){
        $columns = array( 
            0=>'UnidadId',
            1=>'Clave',
            2=>'nombre',
            3=>'status'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('unidades');
        if($params['buscar_registro']!=''){
            $this->db->like('Clave',$params['buscar_registro']);
            $this->db->or_like('nombre',$params['buscar_registro']);
        }
        $this->db->order_by('status', "desc");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_servicios($params){
        $columns = array( 
            0=>'ServiciosId',
            1=>'Clave',
            2=>'nombre',
            3=>'status'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('servicios');
        if($params['buscar_registro']!=''){
            $this->db->like('Clave',$params['buscar_registro']);
            $this->db->or_like('nombre',$params['buscar_registro']);
        }
        $this->db->order_by('status', "desc");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_servicios($params){
        $columns = array( 
            0=>'ServiciosId',
            1=>'Clave',
            2=>'nombre',
            3=>'status'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('servicios');
        if($params['buscar_registro']!=''){
            $this->db->like('Clave',$params['buscar_registro']);
            $this->db->or_like('nombre',$params['buscar_registro']);
        }
        $this->db->order_by('status', "desc");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_productos_hules($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.codigo',
            2=>'p.nombre',
            3=>'c.nombre AS categoria',
            4=>'p.precio',
            5=>'p.stock',
            6=>'p.categoria AS idcategoria',
            7=>'p.laboratorio',
            8=>'p.codigo'
        );
        $columns2 = array( 
            0=>'p.id',
            1=>'p.codigo',
            2=>'p.nombre',
            3=>'c.nombre',
            4=>'p.precio',
            5=>'p.stock',
            6=>'p.laboratorio',
            7=>'p.codigo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos_hules p');
        $this->db->join('categoria c','c.id=p.categoria','left');
        $where = array('p.activo'=>1);
        $this->db->where($where);
        if($params['buscar_registro']!=''){
            $this->db->like('p.codigo',$params['buscar_registro']);
            $this->db->or_like('p.nombre',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
            $this->db->or_like('p.categoria',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
            $this->db->or_like('p.precio',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
            $this->db->or_like('p.stock',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
            $this->db->or_like('c.nombre',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_productos_hules($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.codigo',
            2=>'p.nombre',
            3=>'c.nombre',
            4=>'p.precio',
            5=>'p.stock',
            6=>'p.laboratorio',
            7=>'p.codigo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('productos_hules p');
        $this->db->join('categoria c','c.id=p.categoria','left');
        $where = array('p.activo'=>1);
        $this->db->where($where);
        if($params['buscar_registro']!=''){
            $this->db->like('p.codigo',$params['buscar_registro']);
            $this->db->or_like('p.nombre',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
            $this->db->or_like('p.categoria',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
            $this->db->or_like('p.precio',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
            $this->db->or_like('p.stock',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
            $this->db->or_like('c.nombre',$params['buscar_registro']);
            $this->db->where(array('p.activo'=>1));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}