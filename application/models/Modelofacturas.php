<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelofacturas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getfacturas($params){
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $star=$params['star'];
        if(isset($params['reporte'])){
            $report=1;
        }else{
            $report=0;
        }
        $columns = array( 
            0=>'fac.FacturasId',
            1=>'fac.Folio',
            2=>'fac.Nombre',
            3=>'fac.info_factura',
            4=>'fac.Rfc',
            5=>'fac.total',
            6=>'fac.fechafac',
            7=>'fac.fechatimbre',
            8=>'fac.uuid',
            9=>'fac.favorito',
            10=>'fac.favorito',
            11=>'fac.Estado',
            12=>'fac.rutaXml',
            13=>'fac.rutaAcuseCancelacion',
            14=>'fac.cadenaoriginal',
            15=>'fac.FormaPago',
            16=>'fac.clienteId',
            17=>'fac.correoenviado',
            18=>'fac.estatus_correo',
            19=>'fac.MetodoPago',
            20=>'(SELECT SUM(facp.monto) as total FROM f_facturas_pagos as facp WHERE facp.idfactura=fac.FacturasId and facp.activo=1) as depositos'
        );
        $columns_search = array( 
            0=>'fac.FacturasId',
            1=>'fac.Folio',
            2=>'fac.Nombre',
            3=>'fac.info_factura',
            4=>'fac.Rfc',
            5=>'fac.total',
            6=>'fac.fechafac',
            7=>'fac.fechatimbre',
            8=>'fac.uuid',
            9=>'fac.favorito',
            10=>'fac.favorito',
            11=>'fac.Estado',
            12=>'fac.rutaXml',
            13=>'fac.rutaAcuseCancelacion',
            14=>'fac.cadenaoriginal',
            15=>'fac.FormaPago',
            16=>'fac.clienteId',
            17=>'fac.correoenviado',
            18=>'fac.estatus_correo',
            19=>'fac.MetodoPago',
        );

        if($report==1){
            // se puede usar cualquiera de las dos solo que el primero solo se puede ejecutar en la version de mysql mayor a 5.7.22.
            $col_sub="IFNULL((
                        SELECT JSON_ARRAYAGG(JSON_OBJECT('fecha',facp2.deposito, 'name', facp2.depositante,'monto',facp2.monto))
                        FROM f_facturas_pagos as facp2
                        WHERE facp2.idfactura=fac.FacturasId and facp2.activo=1 ORDER BY facp2.id ASC
                    ),'[]') AS caract_sub, ";
            $col_sub=`IFNULL(
                        (
                            SELECT 
                                CONCAT('[', GROUP_CONCAT(
                                    CONCAT(
                                        '{"fecha":"', facp2.deposito, '",',
                                        '"name":"', facp2.depositante, '",',
                                        '"crastreo":"', facp2.clave_rastreo, '",',
                                        '"monto":', facp2.monto, '}'
                                    )
                                ), ']')
                            FROM f_facturas_pagos as facp2 
                            WHERE facp2.idfactura=fac.FacturasId 
                              AND facp2.activo=1 
                            ORDER BY facp2.id ASC
                        ), '[]'
                    ) AS pagos_sub`;
            $col_sub = "IFNULL(
                    (
                        SELECT 
                            CONCAT('[', GROUP_CONCAT(
                                CONCAT(
                                    '{\"fecha\":\"', facp2.deposito, '\",',
                                    '\"name\":\"', facp2.depositante, '\",',
                                    '\"crastreo\":\"', facp2.clave_rastreo, '\",',
                                    '\"monto\":', facp2.monto, '}'
                                )
                            ), ']')
                        FROM f_facturas_pagos as facp2 
                        WHERE facp2.idfactura=fac.FacturasId 
                          AND facp2.activo=1 
                        ORDER BY facp2.id ASC
                    ), '[]'
                ) AS pagos_sub";
            $columns[]=$col_sub;
        }
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_facturas fac');
        $this->db->join('personal per', 'per.personalId = fac.usuario_id','left');
        
        if($cliente>0){
            $this->db->where(array('fac.clienteId'=>$cliente));
        }
        if($finicio!=''){
            $this->db->where(array('fac.fechafac >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('fac.fechafac <='=>$ffin.' 23:59:59'));
        }
        if ($star==1) {
            $this->db->where(array('fac.favorito'=>1));
        }
        if($params['estatus_v']!=0) {
            $this->db->where(array('fac.Estado'=>$params['estatus_v']));    
        }
        $this->db->where(array('fac.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_search as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns_search[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_facturas($params){
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $star=$params['star'];
        $columns = array( 
            0=>'FacturasId',
            1=>'Folio',
            2=>'Nombre',
            3=>'info_factura',
            4=>'Rfc',
            5=>'total',
            6=>'fechafac',
            7=>'fechatimbre',
            8=>'uuid'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('f_facturas');
       
        if($cliente>0){
            $this->db->where(array('clienteId'=>$cliente));
        }
        if($finicio!=''){
            $this->db->where(array('fechatimbre >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if ($star==1) {
            $this->db->where(array('favorito'=>1));
        }
        if($params['estatus_v']!=0) {
            $this->db->where(array('Estado'=>$params['estatus_v']));    
        }
        $this->db->where(array('activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function documentorelacionado($complemento){
        $strq = "SELECT 
                    compd.facturasId,
                    compd.IdDocumento,
                    compd.NumParcialidad,
                    compd.ImpSaldoAnt,
                    compd.ImpPagado,
                    compd.ImpSaldoInsoluto,
                    compd.MetodoDePagoDR,
                    fac.moneda,
                    fac.Folio,
                    fac.serie,
                    fac.FormaPago,
                    fac.iva,
                    fac.ivaretenido,
                    fac.subtotal,
                    fac.serie
                FROM f_complementopago_documento as compd
                inner join f_facturas as fac on fac.FacturasId=compd.facturasId
                where compd.complementoId=$complemento";
        $query = $this->db->query($strq);
        return $query;
    }
    function facturadetalle($facturaId){
        $strq = "SELECT * FROM `f_facturas_servicios` as fas 
                inner join unidades as uni on uni.Clave=fas.Unidad
                WHERE fas.FacturasId=".$facturaId;
        $query = $this->db->query($strq);
        return $query; 
    }
    
    function facturadetalle_excel($mes,$anio,$fecha,$laboratoriov,$laboratorio){
        if($laboratoriov==1){
            if($mes==0){
                $where="WHERE fac.fechatimbre>='$fecha 00:00:00' AND fac.fechatimbre<='$fecha 23:59:59' AND fac.Estado=1;";
            }else{
                $where="WHERE YEAR(fac.fechatimbre)=$anio AND MONTH(fac.fechatimbre) = $mes AND fac.Estado=1";
            }
            $strq = "SELECT fac.Folio,fac.Nombre,facd.Descripcion2,facd.Cantidad,fac.fechatimbre,c.cp,c.direccion,pr.laboratorio,c.rfc,pr.codigo
            FROM `f_facturas_servicios` as facd 
            INNER JOIN f_facturas as fac on fac.FacturasId=facd.FacturasId 
            INNER JOIN clientes as c on c.clienteId=fac.clienteId 
            INNER JOIN productos_hules as pr on pr.nombre=facd.Descripcion2
            $where";
    
        }else{
            if($mes==0){
                $where="WHERE fac.fechatimbre>='$fecha 00:00:00' AND fac.fechatimbre<='$fecha 23:59:59' AND fac.Estado=1 AND pr.laboratorio='$laboratorio'";
            }else{
                $where="WHERE YEAR(fac.fechatimbre)=$anio AND MONTH(fac.fechatimbre) = $mes AND fac.Estado=1 AND pr.laboratorio='$laboratorio'";
            }
            //var_dump($where);die;
            $strq = "SELECT fac.Folio,fac.Nombre,facd.Descripcion2,facd.Cantidad,fac.fechatimbre,c.cp,c.direccion,pr.laboratorio,c.rfc,pr.codigo
            FROM `f_facturas_servicios` as facd 
            INNER JOIN f_facturas as fac on fac.FacturasId=facd.FacturasId 
            INNER JOIN clientes as c on c.clienteId=fac.clienteId 
            INNER JOIN productos_hules as pr on pr.nombre=facd.Descripcion2
            $where";

        }    
        $query = $this->db->query($strq);
        return $query; 
    }
    
    function get_laboratorios(){
        $strq = "SELECT * FROM productos_hules WHERE laboratorio!='' GROUP BY laboratorio";
        $query = $this->db->query($strq);
        return $query; 
    }
    
    function get_numero_gardasil9($fecha1,$fecha2,$pro){
        $strq = "SELECT SUM(fas.cantidad) AS cantidad FROM f_facturas AS f
                LEFT JOIN f_facturas_servicios AS fas ON fas.FacturasId=f.FacturasId
                WHERE f.Estado=1 and fas.Descripcion2='$pro' AND f.fechatimbre BETWEEN '$fecha1 00:00:00' AND '$fecha2 23:59:59 '";
        $query = $this->db->query($strq);
        return $query->row(); 
    }
    
    function get_valor_unitario_gardasil9($fecha1,$fecha2,$pro){
        $strq = "SELECT SUM(fas.cantidad*fas.Cu) AS unidtario FROM f_facturas AS f
                LEFT JOIN f_facturas_servicios AS fas ON fas.FacturasId=f.FacturasId
                WHERE f.Estado=1 and fas.Descripcion2='$pro' AND f.fechatimbre BETWEEN '$fecha1 00:00:00' AND '$fecha2 23:59:59 '";
        $query = $this->db->query($strq);
        return $query->row(); 
    }



}