<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operacion extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,3);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('escaneo/escaneo');
        $this->load->view('templates/footer');
        $this->load->view('escaneo/escaneojs.php');
    }
    public function camara($id){
        $data['ponit']=$id;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('escaneo/camara',$data);
        $this->load->view('templates/footer');
        $this->load->view('escaneo/camarajs.php');
    }
    function validarcodigo(){
        $params = $this->input->post();
        $codigo=$params['codigo'];
        $point=$params['point'];
        $resultado=$this->General_model->getselectwhere('personal','personalId',$codigo);
        $personalId=0;
        foreach ($resultado as $item) {
            $personalId=$item->personalId;
        }
        if ($personalId>0){
            $data['personalId']=$personalId;
            $data['idpoints_check']=$point;
            $data['idempleado']=$this->idpersonal;
            $data['reg']=$this->fechahoy;
            $id=$this->General_model->add_record('check_points_detalles',$data);
        }
        echo $personalId;
    }
    public function empleado($id,$point){
        $data['get_emp']=$this->General_model->get_record('personalId',$id,'personal');
        $data['check_points']=$this->General_model->get_record('id',$point,'check_points');
        $data['point']=$point;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('escaneo/empleado',$data);
        $this->load->view('templates/footer');
        $this->load->view('escaneo/escaneojs.php');
    }
}