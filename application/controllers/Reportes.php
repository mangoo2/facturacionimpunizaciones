<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';//php 7.1 como minimo
//use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border; // Asegúrate de importar la clase Border
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class Reportes extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('Modelofacturas');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->fechahoyc = date('Y-m-d_G_i_s');
        if ($this->session->userdata('logeado')){
            $this->perfilid=$this->session->userdata('perfilid');
            //$permiso=$this->Login_model->getviewpermiso($this->perfilid,4);// id del perfil y id del submenu
            //if ($permiso==0) {
              //  redirect('Login');
            //}
        }else{
            redirect('/Sistema');
        }
    }
    public function index(){
        $data['fecha_hoy']=$this->fecha_reciente;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('reportes/reporte',$data);
        $this->load->view('templates/footer');
        $this->load->view('reportes/reportejs.php');
    }
    
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_resporte($params);
        $totaldata= $this->ModelCatalogos->total_reporte($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function metadata0($cliente,$estatus_v,$finicio='',$ffin=''){
        $params['cliente']=$cliente;
        $params['estatus_v']=$estatus_v;
        $params['finicio']=$finicio;
        $params['ffin']=$ffin;
        $params['star']=0;

        $params['order'][0]['column']=1;
        $params['order'][0]['dir']='desc';
        $params['length']=500;
        $params['start']=0;

        $params['reporte']=1;

        $getdata = $this->Modelofacturas->getfacturas($params);
        //echo json_encode($getdata->result());

        $spreadsheet = new Spreadsheet();
                $row_h=0;//num de pagina

        if($row_h==0){
            $sheet = $spreadsheet->getActiveSheet();//cuendo solo se tiene una pagina
        }else{
            $spreadsheet->createSheet();
            $sheet = $spreadsheet->setActiveSheetIndex($row_h);//a partir de la segunda pagina
        }

        $sheet->setTitle('Metadata');//titulo de pa pagina
        $rc=1;

        $sheet->setCellValue('A1', 'Nombre / Razón social');
        $sheet->setCellValue('B1', 'Nombre del médico');
        $sheet->setCellValue('C1', 'RFC');
        $sheet->setCellValue('D1', 'Total');
        $sheet->setCellValue('E1', 'Fecha de Emisión');
        $sheet->setCellValue('F1', 'Fecha Timbre');
        $sheet->setCellValue('G1', 'Método de pago');
        $sheet->setCellValue('H1', 'Forma de pago');
        $sheet->setCellValue('I1', 'Folio Fiscal');
        $sheet->setCellValue('J1', 'Estatus');
        $sheet->setCellValue('K1', 'Fecha de deposito');
        $sheet->setCellValue('L1', 'Depositarte');
        $sheet->setCellValue('M1', 'Monto');
        $sheet->setCellValue('N1', 'Diferencia');
        $sheet->setCellValue('O1', 'Conciliado');
        //=================================================
            foreach ($getdata->result() as $item) {
                $rc++; 
                $label_estado='';
                if($item->Estado==0){
                    $label_estado='Cancelado';
                }
                if($item->Estado==1){
                    $label_estado='Timbrado';
                }
                if($item->Estado==2){
                    $label_estado='Sin Timbrar';
                }
                $sheet->setCellValue('A'.$rc, $item->Nombre);
                $sheet->setCellValue('B'.$rc, $item->info_factura);
                $sheet->setCellValue('C'.$rc, $item->Rfc);
                $sheet->setCellValue('D'.$rc, $item->total);
                $sheet->setCellValue('E'.$rc, $item->fechafac);
                $sheet->setCellValue('F'.$rc, $item->fechatimbre);
                $sheet->setCellValue('G'.$rc, $item->FormaPago);
                $sheet->setCellValue('H'.$rc, $this->gf_FormaPago($item->MetodoPago));
                $sheet->setCellValue('I'.$rc, $item->uuid);
                $sheet->setCellValue('J'.$rc, $label_estado);
                //============================================
                    $restante=$item->total;
                    if(isset($item->pagos_sub)){
                        //$html.='<p>si existe caract_sub '.$item['caract_sub'].'</p>';
                        $car_sub=json_decode($item->pagos_sub);
                        foreach ($car_sub as $i_c) {
                            $restante=$restante-$i_c->monto;
                            if($restante==0){
                              $badge_t='Conciliado';
                            }else{
                              $badge_t='No Conciliado';
                            }
                            $monto=$i_c->monto;
                            $monto_l='$ '.number_format($monto,2,'.',',');

                            $sheet->setCellValue('K'.$rc, $i_c->fecha);
                            $sheet->setCellValue('L'.$rc, $i_c->name);
                            $sheet->setCellValue('M'.$rc, $monto_l);
                            $sheet->setCellValue('N'.$rc, '$ '.number_format($this->cambiarsigno($restante),2,'.',','));
                            $sheet->setCellValue('O'.$rc, $badge_t);

                            $rc++;
                        }
                    }
                //============================================

                
            }
        //=================================================
            $celda='A1:O1';
                                    //$celda='A'.$rc;
                                    $color_back='b4c6e7';
                                    $style = $sheet->getStyle($celda);
                                    $style->applyFromArray([
                                        'fill' => [
                                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                            'startColor' => ['rgb' => $color_back],

                                        ],
                                        'borders' => [
                                                'allBorders' => [
                                                    'borderStyle' => Border::BORDER_THIN, // Cambia el estilo de borde según lo necesites
                                                    'color' => ['rgb' => '000000'] // Cambia el color del borde según lo necesites
                                                ]
                                        ]
                                    ]);
        //===================================================
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('n')->setAutoSize(true);

        $tiposave=0;//0 guarde directamente 1 descargue
        if($tiposave==1){
            /*
            $writer = new Xlsx($spreadsheet);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="archivo.xlsx"');
            header('Cache-Control: max-age=0');

            // Guardar el archivo en la salida del búfer de salida (output buffer)
            $writer->save('php://output');
            */
        }else{
            // Guardar el archivo XLSX
            $writer = new Xlsx($spreadsheet);
            $urlarchivo0='fichero_reporte/reporte_facturas_'.$this->fechahoyc.'.xlsx';
            $urlarchivo=FCPATH.$urlarchivo0;
            $writer->save($urlarchivo);

            redirect(base_url().$urlarchivo0); 

        }
    }
    function cambiarsigno($num){
      if ($num != 0) {
          $num = -$num; // Cambia el signo
      }else{
        $num=0;
      }
      return $num;
    }
    function gf_FormaPago($text){
        $textl='';
        if ($text=='01') {
          $textl='01 Efectivo';
        }elseif ($text=='02') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='03') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='04') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='05') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='06') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='07') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='08') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='28') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='29') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='99') {
          $textl='99 Otros';
        }elseif ($text=='12') {
          $textl='12 Dacion Pago';
        }elseif ($text=='13') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='14') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='15') {
          $textl='15 Condonacion';
        }elseif ($text=='17') {
          $textl='17 Compensacion';
        }elseif ($text=='23') {
          $textl='23 Novacion';
        }elseif ($text=='24') {
          $textl='24 Confusion';
        }elseif ($text=='25') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='26') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='27') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='30') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='99') {
          $textl='99 Por definir';
        }elseif ($text=='31') {
          $textl='31 Intermediario pagos';
        }
        //log_message('error', 'gf_FormaPago:'.$textl);
        return $textl; 
    }
    function gt_MetodoPago($text){
      if ($text=='PUE') {
          $textl='PUE Pago en una sola exhibicion';
      }else{
          $textl='PPD Pago en parcialidades o diferido';
      }
      return $textl;
    }
    function metadata($cliente,$estatus_v,$finicio='',$ffin=''){
        
        header("Pragma: public");
        header("Expires: 0");
        $filename = "Metadata_".date('Y-m-d_G_i_s').".xls";
        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        
        $params['cliente']=$cliente;
        $params['estatus_v']=$estatus_v;
        $params['finicio']=$finicio;
        $params['ffin']=$ffin;
        $params['star']=0;

        $params['order'][0]['column']=1;
        $params['order'][0]['dir']='desc';
        $params['length']=500;
        $params['start']=0;

        $params['reporte']=1;

        $getdata = $this->Modelofacturas->getfacturas($params);
        //echo json_encode($getdata->result());
        $rc=1;
        $html='';
        $html.='<table border="1">';
            $html.='<thead>';
                $html.='<tr>';
                    $html.='<th>Nombre / Razón social</th>';
                    $html.='<th>Nombre del médico</th>';
                    $html.='<th>RFC</th>';
                    $html.='<th>Total</th>';
                    $html.='<th>Fecha de Emisión</th>';
                    $html.='<th>Fecha Timbre</th>';
                    $html.='<th>Método de pago</th>';
                    $html.='<th>Forma de pago</th>';
                    $html.='<th>Folio Fiscal</th>';
                    $html.='<th>Estatus</th>';
                    $html.='<th>Fecha de deposito</th>';
                    $html.='<th>Depositarte</th>';
                    $html.='<th>Monto</th>';
                    $html.='<th>Diferencia</th>';
                    $html.='<th>Conciliado</th>';
                    $html.='<th>Clave de rastreo</th>';
                $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody>';
                foreach ($getdata->result() as $item) {
                    $rc++; 
                    $label_estado='';
                    if($item->Estado==0){
                        $label_estado='Cancelado';
                    }
                    if($item->Estado==1){
                        $label_estado='Timbrado';
                    }
                    if($item->Estado==2){
                        $label_estado='Sin Timbrar';
                    }
                    $html.='<tr>';

                    $html.='<td>'.$item->Nombre.'</td>';
                    $html.='<td>'.$item->info_factura.'</td>';
                    $html.='<td>'.$item->Rfc.'</td>';
                    $html.='<td>'.$item->total.'</td>';
                    $html.='<td>'.$item->fechafac.'</td>';
                    $html.='<td>'.$item->fechatimbre.'</td>';
                    $html.='<td>'.$item->FormaPago.'</td>';
                    $html.='<td>'.$this->gf_FormaPago($item->MetodoPago).'</td>';
                    $html.='<td>'.$item->uuid.'</td>';
                    $html.='<td>'.$label_estado.'</td>';
                    //============================================
                        $restante=$item->total;
                        $rowpagos=0;
                        if(isset($item->pagos_sub)){
                            //$html.='<p>si existe caract_sub '.$item['caract_sub'].'</p>';
                            $car_sub=json_decode($item->pagos_sub);
                            foreach ($car_sub as $i_c) {
                                $restante=$restante-$i_c->monto;
                                if($restante==0){
                                  $badge_t='Conciliado';
                                }else{
                                  $badge_t='No Conciliado';
                                }
                                $monto=$i_c->monto;
                                $monto_l='$ '.number_format($monto,2,'.',',');
                                if($rowpagos>0){
                                    $html.='<tr>';
                                    $html.='<td colspan="10"></td>';
                                }
                                $html.='<td>'.$i_c->fecha.'</td>';
                                $html.='<td>'.$i_c->name.'</td>';
                                $html.='<td>'.$monto_l.'</td>';
                                $html.='<td>'.'$ '.number_format($this->cambiarsigno($restante),2,'.',',').'</td>';
                                $html.='<td>'.$badge_t.'</td>';
                                $html.='<td>'.$i_c->crastreo.'</td>';

                                $html.='</tr>';

                                $rc++;$rowpagos++;
                            }
                        }
                    //============================================

                    
                }
            $html.='</tbody>';
            $html.='</table>';
            echo $html;


    }

}    