<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Folios extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,7);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=3;
        $data['btn_active_sub']=7;
        $data['get_laboratorios']=$this->Modelofacturas->get_laboratorios();
        //$data['np']=$this->Modelofacturas->get_numero_gardasil9('2024-03-25',$this->fechahoy);
        //$data['npu']=$this->Modelofacturas->get_valor_unitario_gardasil9('2024-03-25',$this->fechahoy);
        $data['productosh']=$this->ModeloCatalogos->getselectwheren('productos_hules',array('activo'=>1));
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('folio/listado');
        $this->load->view('templates/footer');
        $this->load->view('folio/listadojs');
    }

    public function registro($id=0){
        $data['btn_active']=3;
        $data['btn_active_sub']=7;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('folio/vista');
        $this->load->view('templates/footer');
        $this->load->view('folio/vistajs');
    }
    public function getlistfacturas() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getfacturas($params);
        $totaldata= $this->Modelofacturas->total_facturas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function facturapdf($FacturasId){
      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        if($datosconfiguracion->logotipo!=''){
          $logotipo = base_url().'public/img/'.$datosconfiguracion->logotipo;
        }else{
          $logotipo = base_url().'public/img/logo_hules.png';
        }
        $data["logotipo"]=$logotipo;
        
      $data["Nombrerasonsocial"]=$datosconfiguracion->Nombre;
      $data['rrfc']=$datosconfiguracion->Rfc;
      $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Noexterior.'  interior '.$datosconfiguracion->Nointerior.' '.$datosconfiguracion->Colonia.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;
      $data['regimenf']=$this->gt_regimenfiscal($datosconfiguracion->Regimen);
      $factura = $this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
      foreach ($factura->result() as $item) {
          $MetodoPago=$item->MetodoPago;
          $FormaPago=$item->FormaPago;
          $observaciones=$item->observaciones;
          //$Folio=str_pad($item->Folio, 4, "0", STR_PAD_LEFT);
          $Folio=$item->Folio;
          $isr=$item->isr;
          $uuid=$item->uuid;
          $nocertificadosat=$item->nocertificadosat;
          $certificado=$item->nocertificado;
          $fechatimbre=$item->fechatimbre;
          $uso_cfdi=$item->uso_cfdi;
          $cliente=$item->Nombre;
          $clirfc=$item->Rfc;
          $clidireccion=$item->Direccion;
          $numproveedor=$item->numproveedor;
          $numordencompra=$item->numordencompra;
          $Caducidad=$item->Caducidad;
          $Lote=$item->Lote;
          $moneda=$item->moneda;
          $subtotal  = $item->subtotal;
          $iva = $item->iva;
          $total = $item->total;
          $ivaretenido = $item->ivaretenido;
          $cedular = $item->cedular;
          $selloemisor = $item->sellocadena;
          $sellosat = $item->sellosat;
          $cadenaoriginal = $item->cadenaoriginal;
          $tarjeta=$item->tarjeta;
          $tipoComprobante=$item->TipoComprobante;
          $cp=$item->Cp;
          
          $isr = $item->isr; 
          $ivaretenido = $item->ivaretenido; 
          $cincoalmillarval = $item->cincoalmillarval; 
          $outsourcing = $item->outsourcing; 
          $clienteId=$item->clienteId;
          $fechafac=$item->fechafac;

      }
          $dcliente=$this->ModeloCatalogos->getselectwheren('clientes',array('clienteId'=>$clienteId));
        foreach ($dcliente->result() as $item) {
            $RegimenFiscalReceptor = $item->RegimenFiscalReceptor;
            
        }
      $facturadetalles = $this->Modelofacturas->facturadetalle($FacturasId);
          $data['cp']=$cp;
      //=============
          $data['FormaPago']=$MetodoPago;
          //log_message('error', 'FormaPago c: '.$MetodoPago);
          $data['FormaPagol']=$this->gf_FormaPago($MetodoPago);
          //$data['FormaPagol']='vv';
        //=================
          $data['MetodoPago']=$FormaPago;
          $data['MetodoPagol']=$this->gt_MetodoPago($FormaPago);
          //$data['MetodoPagol']='cc';
        //=================
          $data['observaciones']=$observaciones;
          $data['Folio']=$Folio;
        //=================
          $data["isr"]=$isr;
        //=================
          $data['folio_fiscal']=$uuid;
          $data['nocertificadosat']=$nocertificadosat;
          $data['certificado']=$certificado;
          $data['fechatimbre']=$fechatimbre;
          $data['cfdi']=$this->gt_uso_cfdi($uso_cfdi);
          $data['cliente']=$cliente;
          $data['clirfc']=$clirfc;
          $data['clidireccion']=$clidireccion;
          $data['RegimenFiscalReceptor']   =   $this->gt_regimenfiscal($RegimenFiscalReceptor);
          $data['numproveedor']=$numproveedor;
          $data['numordencompra']=$numordencompra;
          $data['moneda']=$moneda;
          $data['subtotal']=$subtotal;
          $data['iva']=$iva;

          $data['isr']=$isr;
          $data['ivaretenido']=$ivaretenido;
          $data['cincoalmillarval']=$cincoalmillarval;
          $data['outsourcing']=$outsourcing;

          $data['total']=$total;
          $data['ivaretenido']=$ivaretenido;
          $data['cedular']=$cedular;
          $data['selloemisor']=$selloemisor;
          $data['sellosat']=$sellosat;
          $data['cadenaoriginal']=$cadenaoriginal;
          $data['tarjeta']=$tarjeta;
          $data['facturadetalles']=$facturadetalles->result();
          $data['tipoComprobante']=$tipoComprobante;
          $data['idfactura']=$FacturasId;
          //$data['detalleperidofactura']= $this->Rentas_model->detalleperidofactura($FacturasId);
          //$data['detalleconsumiblesfolios']= $this->Rentas_model->detalleconsumiblesfolios($FacturasId);
          //$data['detallecimages']= $this->Rentas_model->detalleimagesRenta($FacturasId);
          $data['Caducidad']      =   $Caducidad;
          $data['Lote']           =   $Lote;
          $data['fechafac']       =   $fechafac;

          $this->load->view('reportes/factura',$data);
    }
    function gf_FormaPago($text){
      //log_message('error', 'gf_FormaPago:'.$text);
        if ($text=='Efectivo') {
          $textl='01 Efectivo';
        }elseif ($text=='ChequeNominativo') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='TransferenciaElectronicaFondos') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='TarjetasDeCredito') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='MonederoElectronico') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='DineroElectronico') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='Tarjetas digitales') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='ValesDeDespensa') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='TarjetaDebito') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='TarjetaServicio') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='Otros') {
          $textl='99 Otros';
        }elseif ($text=='DacionPago') {
          $textl='12 Dacion Pago';
        }elseif ($text=='PagoSubrogacion') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='PagoConsignacion') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='Condonacion') {
          $textl='15 Condonacion';
        }elseif ($text=='Compensacion') {
          $textl='17 Compensacion';
        }elseif ($text=='Novacion') {
          $textl='23 Novacion';
        }elseif ($text=='Confusion') {
          $textl='24 Confusion';
        }elseif ($text=='RemisionDeuda') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='PrescripcionoCaducidad') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='SatisfaccionAcreedor') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='AplicacionAnticipos') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='PorDefinir') {
          $textl='99 Por definir';
        }
        if ($text=='01') {
          $textl='01 Efectivo';
        }elseif ($text=='02') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='03') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='04') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='05') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='06') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='07') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='08') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='28') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='29') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='99') {
          $textl='99 Otros';
        }elseif ($text=='12') {
          $textl='12 Dacion Pago';
        }elseif ($text=='13') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='14') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='15') {
          $textl='15 Condonacion';
        }elseif ($text=='17') {
          $textl='17 Compensacion';
        }elseif ($text=='23') {
          $textl='23 Novacion';
        }elseif ($text=='24') {
          $textl='24 Confusion';
        }elseif ($text=='25') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='26') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='27') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='30') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='99') {
          $textl='99 Por definir';
        }
        //log_message('error', 'gf_FormaPago:'.$textl);
        return $textl; 
    }
    function gt_MetodoPago($text){
      //log_message('error', 'gt_MetodoPago:'.$text);
      if ($text=='PUE') {
          $textl='PUE Pago en una sola exhibicion';
      }else{
          $textl='PPD Pago en parcialidades o diferido';
      }
      //log_message('error', 'gt_MetodoPago:'.$textl);
      return $textl;
    }
    function gt_uso_cfdi($text){
          if ($text=='G01') {
            $textl=' G01 Adquisición de mercancias';
          }elseif ($text=='G02') {
            $textl=' G02 Devoluciones, descuentos o bonificaciones';
          }elseif ($text=='G03') {
            $textl=' G03 Gastos en general';
          }elseif ($text=='I01') {
            $textl=' I01 Construcciones';
          }elseif ($text=='I02') {
            $textl=' I02 Mobilario y equipo de oficina por inversiones';
          }elseif ($text=='I03') {
            $textl=' I03 Equipo de transporte';
          }elseif ($text=='I04') {
            $textl=' I04 Equipo de computo y accesorios';
          }elseif ($text=='I05') {
            $textl=' I05 Dados, troqueles, moldes, matrices y herramental';
          }elseif ($text=='I06') {
            $textl=' I06 Comunicaciones telefónicas';
          }elseif ($text=='I07') {
            $textl=' I07 Comunicaciones satelitales';
          }elseif ($text=='I08') {
            $textl=' I08 Otra maquinaria y equipo';
          }elseif ($text=='D01') {
            $textl=' D01 Honorarios médicos, dentales y gastos hospitalarios.';
          }elseif ($text=='D02') {
            $textl=' D02 Gastos médicos por incapacidad o discapacidad';
          }elseif ($text=='D03') {
            $textl=' D03 Gastos funerales.';
          }elseif ($text=='D04') {
            $textl=' D04 Donativos.';
          }elseif ($text=='D05') {
            $textl=' D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).';
          }elseif ($text=='D06') {
            $textl=' >D06 Aportaciones voluntarias al SAR.';
          }elseif ($text=='D07') {
            $textl=' D07 Primas por seguros de gastos médicos.';
          }elseif ($text=='D08') {
            $textl=' D08 Gastos de transportación escolar obligatoria.';
          }elseif ($text=='D09') {
            $textl=' D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.';
          }elseif ($text=='D10') {
            $textl=' D10 Pagos por servicios educativos (colegiaturas)';
          }elseif ($text=='P01') {
            $textl=' P01 Por definir';
          }elseif ($text=='S01') {
            $textl=' S01 Sin efectos fiscales';
          }else{
            $textl='';
          }
          return $textl;
    }
    function gt_regimenfiscal($text){
          if($text=="601"){ 
            $textl='601 General de Ley Personas Morales';

          }elseif($text=="603"){ 
            $textl='603 Personas Morales con Fines no Lucrativos';

          }elseif($text=="605"){ 
            $textl='605 Sueldos y Salarios e Ingresos Asimilados a Salarios';

          }elseif($text=="606"){ 
            $textl='606 Arrendamiento';

          }elseif($text=="607"){ 
            $textl='607 Régimen de Enajenación o Adquisición de Bienes';

          }elseif($text=="608"){ 
            $textl='608 Demás ingresos';

          }elseif($text=="609"){ 
            $textl='609 Consolidación';

          }elseif($text=="610"){ 
            $textl='610 Residentes en el Extranjero sin Establecimiento Permanente en México';

          }elseif($text=="611"){ 
            $textl='611 Ingresos por Dividendos (socios y accionistas)';

          }elseif($text=="612"){ 
            $textl='612 Personas Físicas con Actividades Empresariales y Profesionales';

          }elseif($text=="614"){ 
            $textl='614 Ingresos por intereses';

          }elseif($text=="615"){ 
            $textl='615 Régimen de los ingresos por obtención de premios';

          }elseif($text=="616"){ 
            $textl='616 Sin obligaciones fiscales';

          }elseif($text=="620"){ 
            $textl='620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos';

          }elseif($text=="621"){ 
            $textl='621 Incorporación Fiscal';

          }elseif($text=="622"){ 
            $textl='622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras';

          }elseif($text=="623"){ 
            $textl='623 Opcional para Grupos de Sociedades';

          }elseif($text=="624"){ 
            $textl='624 Coordinados';

          }elseif($text=="625"){ 
            $textl='625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas';

          }elseif($text=="626"){ 
            $textl='626 Régimen Simplificado de Confianza';

          }elseif($text=="628"){ 
            $textl='628 Hidrocarburos';

          }elseif($text=="629"){ 
            $textl='629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales';

          }elseif($text=="630"){ 
            $textl='630 Enajenación de acciones en bolsa de valores';
          }else{
            $textl='';
          }
          return $textl;
    }
    function favorito(){
        $params = $this->input->post();
        $factura = $params['factura'];
        $status = $params['status'];
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('favorito'=>$status),array('FacturasId'=>$factura));
    }
    function deletefactura(){
        $params = $this->input->post();
        $factura = $params['factura'];
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('activo'=>0),array('FacturasId'=>$factura));
    }
    function listasdecomplementos(){
        $factura=$this->input->post('facturas');
        //$resultado=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('FacturasId'=>$factura,'Estado'=>1));
        $resultado=$this->ModeloCatalogos->complementofacturas($factura);
        $html='<table class="table" id="tableliscomplementos">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Numero de parcialidad</th>
                        <th>Monto</th>
                        <th>Archivo</th>
                    </tr>
                </thead>
                <tbody>';
        foreach ($resultado->result() as $item) {
            $html.='<tr>
                        <td>'.$item->complementoId.'</td>
                        <td>'.$item->FechaPago.'</td>
                        <td>'.$item->NumParcialidad.'</td>
                        <td>'.$item->ImpPagado.'</td>
                        <td>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().$item->rutaXml.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609"><i class="far fa-file-code fa-2x"></i></a>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().'Facturaslis/complementodoc/'.$item->complementoId.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML"  data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609">
                                <i class="fas fa-file-pdf fa-2x"></i>
                                </a>
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function correoscliente(){
      $params=$this->input->post();
      $clienteId=$params['cliente'];
      $factura = $this->ModeloCatalogos->getselectwheren('cliente_mail',array('clienteId'=>$clienteId,'activo'=>1));
      $option='';
      foreach ($factura->result() as $item) {
        $option.='<tr class="climailid_'.$item->climailId.'"><td><input value="'.$item->mail.'" id="mailcli" class="form-control" readonly style="border:0px;"></td>';
        $option.='<td><a type="button"
                          class="b-btn b-btn-primary tooltipped btn-retimbrar" onclick="deletemailcli('.$item->climailId.')"
                          data-position="top" data-delay="50" data-tooltip="Elimiar" id="refacturar">
                          <span class="fa-stack">
                          <i class="icon-xl far fa-trash-alt"></i>
                          </span>
                        </a></td></tr>';
      }
      echo $option; 

    }
    function estatusdecomplementos(){
      $factura=$this->input->post('factura');
      $total=$this->ModeloCatalogos->sumapagadocomplemento($factura);
      echo $total;
    }

    function reporte_excel($mes,$anio,$fecha,$laboratorio){  
      $data['mes']=$mes;
      $data['anio']=$anio;
      $data['anio']=$anio;
      $data['fecha']=$fecha;
      //var_dump($mes.','.$anio.','.$fecha.','.$laboratorio);
      if(!is_numeric($laboratorio)){
        $laboratoriov=0;//texto
      }else{
        $laboratoriov=1;//numero
      }
     
      $data['get_folios']=$this->Modelofacturas->facturadetalle_excel($mes,$anio,$fecha,$laboratoriov,$laboratorio);
      $this->load->view('folio/excel',$data);
    }

    function get_tabla_vacunas($value=''){
      $factura=$this->input->post('factura');
      $html='<table class="table table-sm">
            <thead>
                <tr>
                    <th>Cantidad</th>
                    <th>Clave de unidad</th>
                    <th>Clave del producto y/o servicio</th>
                    <th>Descripción</th>
                    <th>Valor unitario</th>
                    <th>Subtotal</th>
                </tr>
            </thead>
            <tbody>';
              $facturadetalles = $this->Modelofacturas->facturadetalle($factura);
              foreach ($facturadetalles->result() as $item) {
                $html.='<tr>
                  <td>'.$item->Cantidad.'</td>
                  <td>'.$item->Unidad.' / '.$item->nombre.'</td>
                  <td>'.$item->servicioId.' / '.$item->Descripcion.'</td>
                  <td>'.$item->Descripcion2.'</td>
                  <td>$ '.$item->Cu.'</td>
                  <td>$'.$item->descuento.'</td>
                  <td>$ '.number_format(($item->Cantidad*$item->Cu)-$item->descuento,2,'.',',').'</td>
                </tr>';
              }
                $html.='
            </tbody>
        </table>';
      echo $html;
    }
    
    function get_cantidad(){
      $f1=$this->input->post('f1');
      $f2=$this->input->post('f2');
      $pro=$this->input->post('pro');
      $np=$this->Modelofacturas->get_numero_gardasil9($f1,$f2,$pro);
      echo (int) ($np->cantidad);
    }

    function get_unitario(){
      $f1=$this->input->post('f1');
      $f2=$this->input->post('f2');
      $pro=$this->input->post('pro');
      $npu=$this->Modelofacturas->get_valor_unitario_gardasil9($f1,$f2,$pro);
      echo '$'.number_format($npu->unidtario,2,'.',',');
    }
    function updateinfo(){
      $params = $this->input->post();
      $idfac = $params['idfac'];
      $info = $params['info'];
      $this->ModeloCatalogos->updateCatalogo('f_facturas',array('info_factura'=>$info),array('FacturasId'=>$idfac));
    }
    function add_depositos(){
      $params = $this->input->post();
      $this->ModeloCatalogos->Insert('f_facturas_pagos',$params);
    }
    function view_depositos(){
      $params = $this->input->post();
      $idfac = $params['idfac'];
      $html='';
      $resul_fac=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfac));
      $total_fac=0;
      foreach ($resul_fac->result() as $itemf) {
        $total_fac=$itemf->total;
      }
      $resul_pagos=$this->ModeloCatalogos->getselectwheren('f_facturas_pagos',array('idfactura'=>$idfac,'activo'=>1));
      $restante=$total_fac;
      foreach ($resul_pagos->result() as $item) {
        $restante=$restante-$item->monto;

      
        if($restante==0){
          $badge_t='Conciliado';
          $badge_c='badge-success';
        }else{
          $badge_t='No Conciliado';
          $badge_c='badge-danger';
        }
        $badge='<div class="div_bad '.$badge_c.'" >'.$badge_t.'</div>';
        $btn_delete='<a type="button" class="btn btn-danger btn-sm" onclick="delete_depositos_fac('.$item->id.')"><i class="fas fa-trash"></i></a>';

        $html.='<tr>';
            $html.='<td>'.$item->deposito.'</td>';
            $html.='<td>'.$item->depositante.'</td>';
            $html.='<td>$ '.number_format($item->monto,2,'.',',').'</td>';
            $html.='<td>'.$item->clave_rastreo.'</td>';
            $html.='<td>'.$badge.'</td>';
            $html.='<td>$ '.number_format($this->cambiarsigno($restante),2,'.',',').'</td>';
            $html.='<td>'.$btn_delete.'</td>';
        $html.='</tr>';
      }

      echo $html;
    }

    function cambiarsigno($num){
      if ($num != 0) {
          $num = -$num; // Cambia el signo
      }else{
        $num=0;
      }
      return $num;
    }
    function delete_depositos_fac(){
      $params = $this->input->post();
      $id = $params['id'];
      $this->ModeloCatalogos->updateCatalogo('f_facturas_pagos',array('activo'=>0),array('id'=>$id));
    }
}    