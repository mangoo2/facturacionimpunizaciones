<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloCatalogos');

    }
	public function index(){
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $data['result']=$datosconfiguracion;

        $this->load->view('login2',$data);
	}
	public function login(){
		$username = $this->input->post('usuario');
        $password = $this->input->post('password');
        // Inicializamos la variable de respuesta en 0;
        $count = 0;
        // Obtenemos los datos del usuario ingresado mediante su usuario
        log_message('error', 'usuario: '.$username);
        $respuesta = $this->Login_model->login($username);
        $contrasena='';
        foreach ($respuesta as $item) {
            $contrasena =$item->contrasena;
        }
        // Verificamos si las contraseñas son iguales
        log_message('error', 'contra1: '.$password);
        log_message('error', 'contra2: '.$contrasena);
        $verificar = password_verify($password,$contrasena);

        // En caso afirmativo, inicializamos datos de sesión
        if ($verificar) 
        {
            $data = array(
                        'logeado' => true,
                        'usuarioid' => $respuesta[0]->UsuarioID,
                        'usuario' => $respuesta[0]->nombre,
                        'perfilid'=>$respuesta[0]->perfilId,
                        'idpersonal'=>$respuesta[0]->personalId,
                        'empleado'=>$respuesta[0]->empleado,
                        'perfil'=>$respuesta[0]->perfil,
                        'foto'=>$respuesta[0]->foto,
                    );
            $this->session->set_userdata($data);
            $this->ModeloCatalogos->Insert('historial_session',array('idpersonal'=>$respuesta[0]->personalId));
            $count=1;
        }
        // Devolvemos la respuesta
        echo $count;

	}
	public function logout(){
		$this->session->sess_destroy();
        redirect(base_url(), 'refresh');
	}
}
