<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaVacunas extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,2);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=11;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('productoshulesgrapas/listado');
        $this->load->view('templates/footer');
        $this->load->view('productoshulesgrapas/listadojs');
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_productos_hules($params);
        $totaldata= $this->ModelCatalogos->total_productos_hules($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function registrar_datos(){
        $data=$this->input->post();
        $id_registro=$data['id'];
        unset($data['id']);
        if($id_registro==0){
            $id=$this->General_model->add_record('productos_hules',$data);
        }else{
            $id=$this->General_model->edit_record('id',$id_registro,$data,'productos_hules');
            $id=$id_registro;
        }
        echo $id;
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'productos_hules');
    }

    public function registrar_datos_cat(){
        $data=$this->input->post();
        $id_registro=$data['id'];
        unset($data['id']);
        if($id_registro==0){
            $id=$this->General_model->add_record('categoria',$data);
        }else{
            $id=$this->General_model->edit_record('id',$id_registro,$data,'categoria');
            $id=$id_registro;
        }
        echo $id;
    }

    public function tabla_datos_cat_get(){
        $html='';
        $result=$this->General_model->getselectwhere_orden_desc('categoria',array('activo'=>1),'id');
        foreach ($result as $x){
            $html.='<tr>
                <td>'.$x->id.'</td>
                <td>'.$x->nombre.'</td>
                <td>
                    <a class="btn"><i class="icon-xl far fa-edit cat_'.$x->id.'"
                        data-nombre="'.$x->nombre.'"
                        onclick="editar_registro_cat('.$x->id.')"></i></a>
                    <a class="btn"><i class="icon-xl far fa-trash-alt" onclick="eliminar_registro_cat('.$x->id.')"></i></a>
                </td>
            </tr>';
        }    
        echo $html;
    }

    public function delete_record_cat(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'categoria');
    }
    
    public function select_datos_cat_get(){
        $html='<label>Categoría</label>
            <select class="form-control" name="categoria" id="categoria">';
        $result=$this->General_model->getselectwhere_orden_desc('categoria',array('activo'=>1),'id');
        foreach ($result as $x){
            $html.='<option value="'.$x->id.'">'.$x->nombre.'</option>';
        }    
        $html.='</select>';
        echo $html;
    }
    function searchproductos(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclike3('productos_hules','nombre',$pro);
        //echo $results;
        echo json_encode($results->result());
    }
                                
}