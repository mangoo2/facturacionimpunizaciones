<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preview extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('ModeloCatalogos');
        //$this->load->model('Modelofacturas');
        $this->load->helper('url');
    }

	public function index(){
        
        
	}   

    public function factura(){
        log_message('error', 'info GET:'.json_encode($_GET));
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        if($datosconfiguracion->logotipo!=''){
          $logotipo = base_url().'public/img/'.$datosconfiguracion->logotipo;
        }else{
          $logotipo = base_url().'public/img/logo_hules.png';
        }
        $data["logotipo"]=$logotipo;
        $data["Nombrerasonsocial"]=$datosconfiguracion->Nombre;
        $data['rrfc']=$datosconfiguracion->Rfc;
        $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;
        $data['regimenf']=$this->gt_regimenfiscal($datosconfiguracion->Regimen);

        $data['Folio']=0001;
        $data['folio_fiscal']='XX00X000-000X-0X00-0X0X-X0X00XXXX000';
        $data['nocertificadosat']='00001000000000000000';
        $data['certificado']='00001000000000000000';
        $data['cfdi']=$this->gt_uso_cfdi($_GET['uso_cfdi']);
        $data['fechatimbre']='0000-00-00 00:00:00';

        $dcliente=$this->ModeloCatalogos->getselectwheren('clientes',array('clienteId'=>$_GET['idcliente']));
        foreach ($dcliente->result() as $item) {
            $cliente=$item->razon_social;
            $direccion=$item->direccion;
            $cp = $item->cp;
            $RegimenFiscalReceptor = $item->RegimenFiscalReceptor;
            
        }
        /*
        $destado=$this->ModeloCatalogos->getselectwheren('estado',array('EstadoId'=>$idestado));
        foreach ($destado->result() as $item) {
            $estado=$item->Nombre;
        }
        */

        //$resultcli=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('id'=>$_GET['rfc']));
        //$cliente='';
        $clirfc=$_GET['rfc'];
        //$clidireccion='';
        /*
        foreach ($resultcli->result() as $item) {
            $cliente=$item->razon_social;
            $clirfc=$item->rfc;
            $cp = $item->cp;
            $num_ext = $item->num_ext;
            $num_int = $item->num_int;
            $colonia = $item->colonia;
            $calle = $item->calle;
            $cp = $item->cp;

            if($item->estado!=null){
                $estado=$item->estado;
            }
            if($item->municipio!=null){
              $municipio=$item->municipio;   
            }


        }
        */
        
        $clidireccion     =       $direccion ;


        $data['cp']             =   $cp;
        $data['cliente']        =   $cliente;
        $data['clirfc']         =   $clirfc;
        $data['clidireccion']   =   $clidireccion;
        $data['RegimenFiscalReceptor']   =   $this->gt_regimenfiscal($RegimenFiscalReceptor);
        $data['isr']            =   $_GET['visr'];
        $data['numordencompra'] =   $_GET['numordencompra'];
        $data['ivaretenido']    =   $_GET['vriva'];
        $data['cedular']        =   0;
        $data['cincoalmillarval']        =   $_GET['v5millar'];
        $data['outsourcing']        =   $_GET['outsourcing'];
        $data['numproveedor']   =   $_GET['numproveedor'];
        //$data['observaciones']  =   $_GET['observaciones'];
        $data['observaciones']  =   '';
        $data['iva']            =   $_GET['iva'];
        $data['subtotal']       =   $_GET['subtotal'];
        $data['total']          =   $_GET['total'];
        $data['moneda']         =   $_GET['moneda'];
        //$data['tarjeta']        =   $_GET['tarjeta'];
        $data['tarjeta']        =   '';
        $data['FormaPago']      =   $_GET['MetodoPago'];
        $data['FormaPagol']     =   $this->gf_FormaPago($_GET['MetodoPago']);
        $data['MetodoPago']     =   $_GET['FormaPago'];
        $data['MetodoPagol']    =   $this->gt_MetodoPago($_GET['FormaPago']);
        $data['tipoComprobante']='I-Ingreso';

        $data['Caducidad']      =   $_GET['Caducidad'];
        $data['Lote']           =   $_GET['Lote'];
        $data['fechafac']       =   $_GET['fechafac'];

        $conseptos=json_decode($_GET['conceptos']);
        $arrayconceptos=array();
        foreach ($conseptos as $item) {
            $arrayconceptos[]=array(
                                    'Cantidad'=>$item->Cantidad,
                                    'Unidad'=>$item->Unidad,
                                    'nombre'=>$this->get_f_undades($item->Unidad),
                                    'servicioId'=>$item->servicioId,
                                    'Descripcion'=>$item->Descripcion,
                                    'Descripcion2'=>$item->Descripcion2,
                                    'Cu'=>$item->Cu,
                                    'descuento'=>$item->descuento
                                );
        }
        $arrayconceptos=json_encode($arrayconceptos);
        $arrayconceptos=json_decode($arrayconceptos);

        $data['facturadetalles']=$arrayconceptos;
        $data['selloemisor']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['sellosat']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['cadenaoriginal'] = '||3.3|U|819|0000-00-00T00:00:00|99|00001000000000000000|CONTADO|'.$_GET['subtotal'].'|MXN|'.$_GET['total'].'|I|'.$_GET['FormaPago'].'|72090|APR980122KZ6|ALTA PRODUCTIVIDAD SA DE
CV|'.$_GET['uso_cfdi'].'|'.$clirfc.'|'.$cliente.'|G03|44103103|1.00|H87|Pieza|EJEMPLO|1562.88|1562.88|1562.88|002|Tasa|0.160000|250.06|002|Tasa|0.160000|250.06|250.06||';



        $this->load->view('reportes/factura',$data);
    }   
    public function facturar(){
        $data["Nombrerasonsocial"]='Nombrerasonsocial';
        $data['rrfc']='erftgyuio5678';
        $data['rdireccion']='direccion conocida';
        $data['regimenf']='601 General de Ley Personas Morales';
        $data['Folio']=0001;
        $data['folio_fiscal']='XX00X000-000X-0X00-0X0X-X0X00XXXX000';
        $data['nocertificadosat']='00001000000000000000';
        $data['certificado']='00001000000000000000';
        $data['cfdi']=$this->gt_uso_cfdi($_GET['uso_cfdi']);
        $data['fechatimbre']='0000-00-00 00:00:00';
        $resultcli=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('Rfc'=>$_GET['rfc']));
        $cliente='';
        $clirfc='';
        $clidireccion='';
        foreach ($resultcli->result() as $item) {
            $cliente=$item->razon_social;
            $clirfc=$item->rfc;
            $cp = $item->cp;
            $num_ext = $item->num_ext;
            $num_int = $item->num_int;
            $colonia = $item->colonia;
            $calle = $item->calle;

        }
        $dcliente=$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$_GET['idcliente']));
          foreach ($dcliente->result() as $item) {
            $municipio=$item->municipio;
            $idestado=$item->estado;
          }
          $destado=$this->ModeloCatalogos->getselectwheren('estado',array('EstadoId'=>$idestado));
          foreach ($destado->result() as $item) {
            $estado=$item->Nombre;
          }
          $clidireccion     =       $calle . ' No ' . $num_ext . ' Col: ' . 
                                    $colonia . ' , ' . 
                                    $municipio . ' , ' . $estado . ' . ';

        $data['cp']             =   $cp;
        $data['cliente']        =   $cliente;
        $data['clirfc']         =   $clirfc;
        $data['clidireccion']   =   $clidireccion;
        $data['isr']            =   $_GET['visr'];
        $data['numordencompra'] =   $_GET['numordencompra'];
        $data['ivaretenido']    =   $_GET['vriva'];
        $data['cedular']        =   0;
        $data['cincoalmillarval']        =   $_GET['v5millar'];
        $data['outsourcing']        =   $_GET['outsourcing'];
        $data['numproveedor']   =   $_GET['numproveedor'];
        $data['observaciones']  =   $_GET['observaciones'];
        $data['iva']            =   $_GET['iva'];
        $data['subtotal']       =   $_GET['subtotal'];
        $data['total']          =   $_GET['total'];
        $data['moneda']         =   $_GET['moneda'];
        $data['tarjeta']        =   $_GET['tarjeta'];
        $data['FormaPago']      =   $_GET['MetodoPago'];
        $data['FormaPagol']     =   $this->gf_FormaPago($_GET['MetodoPago']);
        $data['MetodoPago']     =   $_GET['FormaPago'];
        $data['MetodoPagol']    =   $this->gt_MetodoPago($_GET['FormaPago']);
        $data['tipoComprobante']='I-Ingreso';


        $conseptos=json_decode($_GET['conceptos']);
        $arrayconceptos=array();
        foreach ($conseptos as $item) {
            $arrayconceptos[]=array(
                                    'Cantidad'=>$item->Cantidad,
                                    'Unidad'=>$item->Unidad,
                                    'nombre'=>$this->get_f_undades($item->Unidad),
                                    'servicioId'=>$item->servicioId,
                                    'Descripcion'=>$item->Descripcion,
                                    'Descripcion2'=>$item->Descripcion2,
                                    'Cu'=>$item->Cu,
                                    'descuento'=>$item->descuento
                                );
        }
        $arrayconceptos=json_encode($arrayconceptos);
        $arrayconceptos=json_decode($arrayconceptos);

        $data['facturadetalles']=$arrayconceptos;
        $data['selloemisor']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['sellosat']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['cadenaoriginal'] = '||3.3|U|819|0000-00-00T00:00:00|99|00001000000000000000|CONTADO|'.$_GET['subtotal'].'|MXN|'.$_GET['total'].'|I|'.$_GET['FormaPago'].'|72090|APR980122KZ6|ALTA PRODUCTIVIDAD SA DE
CV|'.$_GET['uso_cfdi'].'|'.$clirfc.'|'.$cliente.'|G03|44103103|1.00|H87|Pieza|EJEMPLO|1562.88|1562.88|1562.88|002|Tasa|0.160000|250.06|002|Tasa|0.160000|250.06|250.06||';



        $this->load->view('Reportes/factura',$data);
    } 
    public function complementodoc(){
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->row();
        
        if($datosconfiguracion->logotipo!=''){
          $logotipo = base_url().'public/img/'.$datosconfiguracion->logotipo;
        }else{
          $logotipo = base_url().'public/img/logo_hules.png';
        }
        $data["logotipo"]=$logotipo;
        $data['rdireccion']=$datosconfiguracion->Calle.' '.$datosconfiguracion->Municipio.' '.$datosconfiguracion->Estado.' '.$datosconfiguracion->PaisExpedicion.' C.P. '.$datosconfiguracion->CodigoPostal;

        $data['rfcemisor']=$datosconfiguracion->Rfc;
        $data['nombreemisor']=$datosconfiguracion->Nombre;
        $data['regimenf']='601 General de Ley Personas Morales';
        $data['idcomplemento']=0;
        $data['uuid']='XX00X000-000X-0X00-0X0X-X0X00XXXX000';
        $data['nocertificadosat']='00001000000000000000';
        $data['certificado']='00001000000000000000';
        $data['rfcreceptor']=$_GET['rfcreceptor'];
        $data['LugarExpedicion']=$_GET['LugarExpedicion'];
        $data['fechatimbre']='0000-00-00 00:00:00';
        $data['nombrereceptorr']=$_GET['razonsocialreceptor'];
        $data['NumOperacion']=$_GET['NumOperacion'];
        $data['moneda']=$_GET['Moneda'];

        //====================================================================
            $resultsfp=$this->ModeloCatalogos->getselectwheren('f_formapago',array('id'=>$_GET['FormaDePagoP']));
            $resultsfp=$resultsfp->row();
            $data['formapago']=$resultsfp->formapago_text;
        //==========================================
        $data['fechapago']=$_GET['Fecha'];
        $data['monto']=$_GET['Monto'];
        $data['Sello']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['sellosat']    =   'MDExMTAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDExMTAxMDAgMDExMDExMTEgMDAxMDAwMDAgMDExMDAxMDAgMDExMDAxMDEgMDAxMDAwMDAgMDExMTAwMDAgMDExMTAwMTAgMDExMTAxMDEgMDExMDAxMDEgMDExMDAwMTAgMDExMDAwMDEgMDAxMDAwMDAgMDEwMTEwMTEgMDExMTAwMTEgMDExMDAxMDAgMDExMTAwMTAgMDExMDAxMDEgMDExMTAxMTAgMDExMDAwMDEgMDExMDAxMDEgMDExMDAwMTAgMDEwMTExMDEgMDAxMDAwMDAgMDEwMDAwMDEgMDEwMDAxMTEgMDEwMDAwMTA=';
        $data['cadenaoriginal'] = '||3.3|282|0000-00-00T00:00:00|00001000000000000000|0|XXX|0|P|'.$_GET['LugarExpedicion'].'|APR980122KZ6|ALTA PRODUCTIVIDAD SA DE CV|601|'.$_GET['rfcreceptor'].'|'.$_GET['rfcreceptor'].'|P01|84111506|1|ACT|Pago|0|0|1.0|0000-00-00T00:00:00|03|MXN|0.00|00446880|XX00X000-000X-0X00-0X0X-X0X00XXXX000|U|282|MXN|PPD|1|4060|4060|0||';      
        $docrelac=json_decode($_GET['arraydocumento']);
        
        $conseptos=json_decode($_GET['arraydocumento']);
        $arrayconceptos=array();
        foreach ($conseptos as $item) {
            $resultscp=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$item->idfactura));
            $resultscp=$resultscp->row();
            $arrayconceptos[]=array(
                                    'idfactura'=>$item->idfactura,
                                    'IdDocumento'=>$item->IdDocumento,
                                    'serie'=>$item->serie,
                                    'NumParcialidad'=>$item->NumParcialidad,
                                    'ImpSaldoAnt'=>$item->ImpSaldoAnt,
                                    'ImpPagado'=>$item->ImpPagado,
                                    'moneda'=>$resultscp->moneda,
                                    'FormaPago'=>$resultscp->FormaPago,
                                    'Folio'=>$resultscp->Folio,
                                    'ImpSaldoInsoluto'=>$item->ImpSaldoInsoluto
                                );
        }
        $arrayconceptos=json_encode($arrayconceptos);
        $arrayconceptos=json_decode($arrayconceptos);




        $data['docrelac']=$arrayconceptos;


      $this->load->view('reportes/complemento',$data);
    }  
    function gt_uso_cfdi($text){
          if ($text=='G01') {
            $textl=' G01 Adquisición de mercancias';
          }elseif ($text=='G02') {
            $textl=' G02 Devoluciones, descuentos o bonificaciones';
          }elseif ($text=='G03') {
            $textl=' G03 Gastos en general';
          }elseif ($text=='I01') {
            $textl=' I01 Construcciones';
          }elseif ($text=='I02') {
            $textl=' I02 Mobilario y equipo de oficina por inversiones';
          }elseif ($text=='I03') {
            $textl=' I03 Equipo de transporte';
          }elseif ($text=='I04') {
            $textl=' I04 Equipo de computo y accesorios';
          }elseif ($text=='I05') {
            $textl=' I05 Dados, troqueles, moldes, matrices y herramental';
          }elseif ($text=='I06') {
            $textl=' I06 Comunicaciones telefónicas';
          }elseif ($text=='I07') {
            $textl=' I07 Comunicaciones satelitales';
          }elseif ($text=='I08') {
            $textl=' I08 Otra maquinaria y equipo';
          }elseif ($text=='D01') {
            $textl=' D01 Honorarios médicos, dentales y gastos hospitalarios.';
          }elseif ($text=='D02') {
            $textl=' D02 Gastos médicos por incapacidad o discapacidad';
          }elseif ($text=='D03') {
            $textl=' D03 Gastos funerales.';
          }elseif ($text=='D04') {
            $textl=' D04 Donativos.';
          }elseif ($text=='D05') {
            $textl=' D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).';
          }elseif ($text=='D06') {
            $textl=' >D06 Aportaciones voluntarias al SAR.';
          }elseif ($text=='D07') {
            $textl=' D07 Primas por seguros de gastos médicos.';
          }elseif ($text=='D08') {
            $textl=' D08 Gastos de transportación escolar obligatoria.';
          }elseif ($text=='D09') {
            $textl=' D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.';
          }elseif ($text=='D10') {
            $textl=' D10 Pagos por servicios educativos (colegiaturas)';
          }elseif ($text=='P01') {
            $textl=' P01 Por definir';
          }elseif ($text=='S01') {
            $textl=' S01 Sin efectos fiscales';
          }else{
            $textl='';
          }
          return $textl;
    }
    function gf_FormaPago($text){
      //log_message('error', 'gf_FormaPago:'.$text);
        if ($text=='Efectivo') {
          $textl='01 Efectivo';
        }elseif ($text=='ChequeNominativo') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='TransferenciaElectronicaFondos') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='TarjetasDeCredito') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='MonederoElectronico') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='DineroElectronico') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='Tarjetas digitales') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='ValesDeDespensa') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='TarjetaDebito') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='TarjetaServicio') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='Otros') {
          $textl='99 Otros';
        }elseif ($text=='DacionPago') {
          $textl='12 Dacion Pago';
        }elseif ($text=='PagoSubrogacion') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='PagoConsignacion') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='Condonacion') {
          $textl='15 Condonacion';
        }elseif ($text=='Compensacion') {
          $textl='17 Compensacion';
        }elseif ($text=='Novacion') {
          $textl='23 Novacion';
        }elseif ($text=='Confusion') {
          $textl='24 Confusion';
        }elseif ($text=='RemisionDeuda') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='PrescripcionoCaducidad') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='SatisfaccionAcreedor') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='AplicacionAnticipos') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='PorDefinir') {
          $textl='99 Por definir';
        }
        if ($text=='01') {
          $textl='01 Efectivo';
        }elseif ($text=='02') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='03') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='04') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='05') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='06') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='07') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='08') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='28') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='29') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='99') {
          $textl='99 Otros';
        }elseif ($text=='12') {
          $textl='12 Dacion Pago';
        }elseif ($text=='13') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='14') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='15') {
          $textl='15 Condonacion';
        }elseif ($text=='17') {
          $textl='17 Compensacion';
        }elseif ($text=='23') {
          $textl='23 Novacion';
        }elseif ($text=='24') {
          $textl='24 Confusion';
        }elseif ($text=='25') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='26') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='27') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='30') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='99') {
          $textl='99 Por definir';
        }
        //log_message('error', 'gf_FormaPago:'.$textl);
        return $textl; 
    }
    function gt_regimenfiscal($text){
          if($text="601"){ 
            $textl='601 General de Ley Personas Morales';

          }elseif($text="603"){ 
            $textl='603 Personas Morales con Fines no Lucrativos';

          }elseif($text="605"){ 
            $textl='605 Sueldos y Salarios e Ingresos Asimilados a Salarios';

          }elseif($text="606"){ 
            $textl='606 Arrendamiento';

          }elseif($text="607"){ 
            $textl='607 Régimen de Enajenación o Adquisición de Bienes';

          }elseif($text="608"){ 
            $textl='608 Demás ingresos';

          }elseif($text="609"){ 
            $textl='609 Consolidación';

          }elseif($text="610"){ 
            $textl='610 Residentes en el Extranjero sin Establecimiento Permanente en México';

          }elseif($text="611"){ 
            $textl='611 Ingresos por Dividendos (socios y accionistas)';

          }elseif($text="612"){ 
            $textl='612 Personas Físicas con Actividades Empresariales y Profesionales';

          }elseif($text="614"){ 
            $textl='614 Ingresos por intereses';

          }elseif($text="615"){ 
            $textl='615 Régimen de los ingresos por obtención de premios';

          }elseif($text="616"){ 
            $textl='616 Sin obligaciones fiscales';

          }elseif($text="620"){ 
            $textl='620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos';

          }elseif($text="621"){ 
            $textl='selected="">621 Incorporación Fiscal';

          }elseif($text="622"){ 
            $textl='622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras';

          }elseif($text="623"){ 
            $textl='623 Opcional para Grupos de Sociedades';

          }elseif($text="624"){ 
            $textl='624 Coordinados';

          }elseif($text=="625"){ 
            $textl='625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas';

          }elseif($text=="626"){ 
            $textl='626 Régimen Simplificado de Confianza';

          }elseif($text="628"){ 
            $textl='628 Hidrocarburos';

          }elseif($text="629"){ 
            $textl='629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales';

          }elseif($text="630"){ 
            $textl='630 Enajenación de acciones en bolsa de valores';
          }else{
            $textl='';
          }
          return $textl;
    }
    function gt_MetodoPago($text){
      //log_message('error', 'gt_MetodoPago:'.$text);
      if ($text=='PUE') {
          $textl='PUE Pago en una sola exhibicion';
      }else{
          $textl='PPD Pago en parcialidades o diferido';
      }
      //log_message('error', 'gt_MetodoPago:'.$textl);
      return $textl;
    }
    function get_f_undades($unidad){
        $result=$this->ModeloCatalogos->getselectwheren('unidades',array('Clave'=>$unidad));
        $nombre='';
        foreach ($result->result() as $item) {
            $nombre=$item->nombre;
        }
        return $nombre;
    }
    
}
