<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Envio extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloclientes');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            
        }else{
            redirect('/Sistema');
        }
    }

	public function index(){
        redirect('/Sistema');
    }

    public function enviocorreo(){
            $params = $this->input->post();
            
            $factura = $params['factura'];
            $correo = $params['correo'];
            
            $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
            $datosconfiguracion=$datosconfiguracion->result();
            $datosconfiguracion=$datosconfiguracion[0];

            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('correoenviado'=>1),array('FacturasId'=>$factura));

            $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$factura));
            $datosfactura=$datosfactura->result();
            $datosfactura=$datosfactura[0];
            $Folio=$datosfactura->Folio;

            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('estatus_correo'=>1),array('FacturasId'=>$factura));
            //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.facturacioninmunizaciones.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'contacto@facturacioninmunizaciones.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'w[=iE9r#DTRI';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('contacto@facturacioninmunizaciones.com','Inmunizaciones');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
       
        
        //$this->email->to('agerardob@gmail.com', 'enviofactura');
        //$this->email->to('contacto@anahuac.sicoi.net', 'enviofactura');
        $contactosarray = array();
        $DATAc = json_decode($correo); 
        for ($i=0;$i<count($DATAc);$i++) {
            //$this->email->to($DATAc[$i]->correo, '');
            $contactosarray[]=$DATAc[$i]->correo; 
        }
        $this->email->to($contactosarray);
        if($datosconfiguracion->Email2!=''){
            //log_message('error',$datosconfiguracion->Email2);
            $this->email->cc($datosconfiguracion->Email2);
            $this->email->bcc($datosconfiguracion->Email2);
        }
        //$this->email->bcc('contacto@hulesygrapas.com');
        $asunto='Factura';
        
        

      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)


        $message  = $datosconfiguracion->cuerpo;

        $this->email->message($message);

        
        $this->email->attach(base_url().$datosfactura->rutaXml);
        $this->email->attach(base_url().'hulesyg/facturaspdf/Factura_'.$Folio.'.pdf');
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

        //==================
        
    
    }
    public function enviocorreocomplemento(){
            $params = $this->input->post();
            
            $conplementoid = $params['conplementoid'];
            $correo = $params['correo'];
            
            $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
            $datosconfiguracion=$datosconfiguracion->result();
            $datosconfiguracion=$datosconfiguracion[0];

            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('correoenviado'=>1),array('complementoId'=>$conplementoid));
            
            $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$conplementoid));
            $datoscomplemento=$datoscomplemento->result();
            $datoscomplemento=$datoscomplemento[0];
            //$Folio=$datoscomplemento->Folio;
            //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.facturacioninmunizaciones.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'contacto@facturacioninmunizaciones.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'w[=iE9r#DTRI';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('contacto@facturacioninmunizaciones.com','inmunizaciones');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
       
        
        //$this->email->to('agerardob@gmail.com', 'enviofactura');
        //$this->email->to('contacto@anahuac.sicoi.net', 'enviofactura');
        $this->email->to($correo, '');
        //$this->email->bcc('contacto@hulesygrapas.com');
        $asunto='Complemento';
        
        

      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)


        $message  = $datosconfiguracion->cuerpo;

        $this->email->message($message);

        
        $this->email->attach(base_url().$datoscomplemento->rutaXml);
        $this->email->attach(base_url().'hulesyg/facturaspdf/Complemento_'.$conplementoid.'.pdf');
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

        //==================
        
    
    }

}    