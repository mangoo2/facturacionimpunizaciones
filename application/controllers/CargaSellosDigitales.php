<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CargaSellosDigitales extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,2);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=13;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cargasellosdigitales/vista');
        $this->load->view('templates/footer');
        $this->load->view('cargasellosdigitales/vistajs');
    }
    function cargacertificado(){
        $configUpload['upload_path'] = FCPATH.'hulesyg/elementos/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = 'archivos';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('cerdigital');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('archivocer'=>$file_name),array('ConfiguracionesId'=>1));

        $output = [];
        echo json_encode($output);
    }
    function cargakey(){
        //$rutainterna= $_SERVER['DOCUMENT_ROOT'].'/hulesygrapas/';
        $rutainterna= $_SERVER['DOCUMENT_ROOT'];
        $rutaarchivos=$rutainterna.'/hulesyg/elementos/';

        $pass = $_POST['pass'];
        $configUpload['upload_path'] = FCPATH.'hulesyg/elementos/';
        //$configUpload['upload_path'] = base_url().'hulesyg/elementos/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = 'archivos';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('claveprivada');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('archivokey'=>$file_name,'paswordkey'=>$pass),array('ConfiguracionesId'=>1));
        file_put_contents($rutaarchivos.'passs.txt', $pass);
        $output = [];
        echo json_encode($output);
    }
}