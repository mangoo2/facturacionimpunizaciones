<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('General_model');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set("America/Mexico_City");
        $this->mes_act=date('n');
    }
    public function index(){
        $data['btn_active']=1;
        $data['btn_active_sub']=1;
        $data['mes']=$this->mes_act;
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];


        $timbresvigentes=$datosconfiguracion->timbresvigentes;
        $data['timbresvigentes']=$timbresvigentes;
        $tv_inicio=$datosconfiguracion->tv_inicio;
        $tv_fin=$datosconfiguracion->tv_fin;
        $facturastimbradas=$this->ModeloCatalogos->total_facturas(1,$tv_inicio,$tv_fin);
        $facturascanceladas=$this->ModeloCatalogos->total_facturas(0,$tv_inicio,$tv_fin);
        $complementotimbradas=$this->ModeloCatalogos->totalcomplementos(1,$tv_inicio,$tv_fin);
        $complementocanceldados=$this->ModeloCatalogos->totalcomplementos(0,$tv_inicio,$tv_fin);

        $data['facturastimbradas'] = $facturastimbradas+$facturascanceladas;
        
        $data['facturascanceladas'] = $facturascanceladas;
        $data['complementotimbradas'] = $complementotimbradas+$complementocanceldados;
        $data['complementocanceldados'] = $complementocanceldados;

        $data['timbresocupados']=$facturastimbradas+$complementotimbradas;
        $data['timbrescancelados']=$facturascanceladas+$complementocanceldados;
        $data['facturasdisponibles'] = $timbresvigentes-($facturastimbradas+$facturascanceladas)-$facturascanceladas-($complementotimbradas+$complementocanceldados)-$complementocanceldados;
            //log_message('error',$timbresvigentes.'-('.$facturastimbradas.'+'.$facturascanceladas.')-'.$facturascanceladas.'-('.$complementotimbradas.'+'.$complementocanceldados.')-'.$complementocanceldados);
        //==================================================================================
        $nombre_fichero = 'hulesyg/temporalsat/FinVigencia.txt';
        if (file_exists($nombre_fichero)) {
            $fh = fopen(base_url().$nombre_fichero, 'r') or die("Se produjo un error al abrir el archivo");
                $linea = fgets($fh);
            fclose($fh); 

            $remplazar = array('notAfter=','  ','GMT'); 
            $remplazarpor = array('',' ','');
            $fechavigente = str_replace($remplazar,$remplazarpor,$linea);
            $fechavigenteex = explode(" ", $fechavigente);
            $fechafin=$fechavigenteex[1].' '.$fechavigenteex[0].' '.$fechavigenteex[3];
        }else{
            $fechafin='';
        }
        $data['fechafin']=$fechafin;
        //==================================================================================
        $data['topcliente']=$this->ModeloCatalogos->topclientesfacturados();
        //==================================================================================
        $ano_actual=date('Y');
        $ano_anterior=date('Y', strtotime('-1 year'));
        $data['ano_actual']=$ano_actual;
        $data['ano_anterior']=$ano_anterior;
        $my_date = new DateTime();
        //==========================================================================
                    $my_date->modify('first day of january '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of january '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorEnero']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of February '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of February '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorFebrero']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of March '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of March '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorMarzo']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of April '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of April '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorAbril']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of May '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of May '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorMayo']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of June '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of June '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorJunio']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of July '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of July '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorJulio']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of August '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of August '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorAgosto']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of September '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of September '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorSeptiembre']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of October '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of October '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorOctubre']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of November '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of November '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorNomviembre']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of December '.$ano_actual);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of December '.$ano_actual);
            $ffin = $my_date->format('Y-m-d');
            $data['valorDiciembre']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of january '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of january '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorEnerop']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of February '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of February '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorFebrerop']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of March '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of March '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorMarzop']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of April '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of April '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorAbrilp']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of May '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of May '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorMayop']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of June '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of June '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorJuniop']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of July '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of July '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorJuliop']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of August '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of August '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorAgostop']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of September '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of September '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorSeptiembrep']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of October '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of October '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorOctubrep']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of November '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of November '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorNomviembrep']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        //==========================================================================
                    $my_date->modify('first day of December '.$ano_anterior);
            $fini = $my_date->format('Y-m-d');
                    $my_date->modify('last day of December '.$ano_anterior);
            $ffin = $my_date->format('Y-m-d');
            $data['valorDiciembrep']=$this->ModeloCatalogos->sumadelmesfacturados($fini,$ffin);
        //==================================================================================
        

        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('inicio');
        $this->load->view('templates/footer');
        $this->load->view('iniciojs');
    }

    public function buscar_cliente(){
        $nom=$this->input->post('clientes');
        $result=$this->ModeloCatalogos->get_cliente($nom);
        echo json_encode($result);
    }

    public function get_data_factura_mes(){

        $total1=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),1);//Enero
        $mes1=number_format($total1, 2, '.', '');
        $total2=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),2);//Febrero
        $mes2=number_format($total2, 2, '.', '');
        $total3=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),3);//Marzo
        $mes3=number_format($total3, 2, '.', '');
        $total4=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),4);//Abril
        $mes4=number_format($total4, 2, '.', '');
        $total5=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),5);//Mayo
        $mes5=number_format($total5, 2, '.', '');
        $total6=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),6);//Junio
        $mes6=number_format($total6, 2, '.', '');
        $total7=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),7);//Julio
        $mes7=number_format($total7, 2, '.', '');
        $total8=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),8);//Agosto
        $mes8=number_format($total8, 2, '.', '');
        $total9=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),9);//Septiembre
        $mes9=number_format($total9, 2, '.', '');
        $total10=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),10);//Octubre
        $mes10=number_format($total10, 2, '.', '');
        $total11=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),11);//Noviembre
        $mes11=number_format($total11, 2, '.', '');
        $total12=$this->ModeloCatalogos->total_facturas_mes_num(date('Y'),12);//Diciembre
        $mes12=number_format($total12, 2, '.', '');

        $result = array('mes1'=>$mes1,'mes2'=>$mes2,'mes3'=>$mes3,'mes4'=>$mes4,'mes5'=>$mes5,'mes6'=>$mes6,'mes7'=>$mes7,'mes8'=>$mes8,'mes9'=>$mes9,'mes10'=>$mes10,'mes11'=>$mes11,'mes12'=>$mes12);
        echo json_encode($result);
    }

    public function get_data_factura_ventas_mes(){

        $total1=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),1);//Enero
        $mes1=number_format($total1, 2, '.', '');
        $total2=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),2);//Febrero
        $mes2=number_format($total2, 2, '.', '');
        $total3=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),3);//Marzo
        $mes3=number_format($total3, 2, '.', '');
        $total4=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),4);//Abril
        $mes4=number_format($total4, 2, '.', '');
        $total5=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),5);//Mayo
        $mes5=number_format($total5, 2, '.', '');
        $total6=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),6);//Junio
        $mes6=number_format($total6, 2, '.', '');
        $total7=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),7);//Julio
        $mes7=number_format($total7, 2, '.', '');
        $total8=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),8);//Agosto
        $mes8=number_format($total8, 2, '.', '');
        $total9=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),9);//Septiembre
        $mes9=number_format($total9, 2, '.', '');
        $total10=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),10);//Octubre
        $mes10=number_format($total10, 2, '.', '');
        $total11=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),11);//Noviembre
        $mes11=number_format($total11, 2, '.', '');
        $total12=$this->ModeloCatalogos->total_facturas_ventas_mes(date('Y'),12);//Diciembre
        $mes12=number_format($total12, 2, '.', '');

        $result = array('mes1'=>$mes1,'mes2'=>$mes2,'mes3'=>$mes3,'mes4'=>$mes4,'mes5'=>$mes5,'mes6'=>$mes6,'mes7'=>$mes7,'mes8'=>$mes8,'mes9'=>$mes9,'mes10'=>$mes10,'mes11'=>$mes11,'mes12'=>$mes12);
        echo json_encode($result);
    }

    
    function get_cliente_top_10_tabla()
    {    
        $anio=$this->input->post('anio');
        $mes=$this->input->post('mes');
        $html='<table class="table table-sm" id="tabla_clientes">
                    <thead>
                        <tr>
                            <th scope="col">Médico</th>
                            <th scope="col"># Facturas</th>
                            <th scope="col">$ Monto</th>
                        </tr>
                    </thead>
                    <tbody>';
                    $result=$this->ModeloCatalogos->get_cliente_top_10_fecha($anio,$mes);
                    foreach ($result as $x){
                        $html.='<tr>
                            <td>'.$x->razon_social.'</td>
                            <td>'.$x->reg.'</td>
                            <td>'.$x->total.'</td>
                        </tr>';
                     } 
                    $html.='</tbody>
                </table>';
        echo $html;
    }

    public function get_data_top_10_clientes(){
        $result=$this->ModeloCatalogos->get_cliente_top_10();
        echo json_encode($result);
    }

    function get_vacunas_top_10_tabla()
    {   
        $anio=$this->input->post('anio');
        $mes=$this->input->post('mes');
        $html='<table class="table table-sm" id="tabla_clientes">
                    <thead>
                        <tr>
                            <th scope="col">Vacuna</th>
                            <th scope="col"># de facturadas</th>
                            <th scope="col">$ Monto</th>
                        </tr>
                    </thead>
                    <tbody>';
                    $result=$this->ModeloCatalogos->get_vacunas_top_10_fecha($anio,$mes);
                    foreach ($result as $x){
                        $html.='<tr>
                            <td>'.$x->Descripcion.'</td>
                            <td>'.$x->reg.'</td>
                            <td>'.$x->total.'</td>
                        </tr>';
                     } 
                    $html.='</tbody>
                </table>';
        echo $html;
    }

    public function get_data_top_10_vacunas(){
        $result=$this->ModeloCatalogos->get_vacunas_top_10();
        echo json_encode($result);
    }
    function consultartimbres($fechainicio,$fechafin){
        //consultar timbres consumidos
        $strq1="SELECT COUNT(*) as total FROM `f_facturas` WHERE (Estado=1 or Estado=0) AND activo=1 AND fechatimbre BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query1 = $this->db->query($strq1);
        $query1=$query1->row();
        echo 'Facturas timbradas: '.$query1->total.'<br>';

        $strq2="SELECT COUNT(*) as total FROM `f_facturas` WHERE Estado=0 AND activo=1 AND fechatimbre BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query2 = $this->db->query($strq2);
        $query2=$query2->row();
        echo 'Facturas canceladas'.$query2->total.'<br>';

        

        $strq4="SELECT COUNT(*) as total FROM f_complementopago WHERE (Estado=1 or Estado=0) AND activo=1 AND fechatimbre BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query4 = $this->db->query($strq4);
        $query4=$query4->row();
        echo 'Complementos Timbrados: '.$query4->total.'<br>';

        $strq3="SELECT COUNT(*) as total FROM f_complementopago WHERE Estado=0 AND activo=1 AND fechatimbre BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query3 = $this->db->query($strq3);
        $query3=$query3->row();
        echo 'Complementos cancelados'.$query3->total.'<br>';
    }

}