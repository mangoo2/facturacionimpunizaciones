-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-03-2023 a las 11:47:10
-- Versión del servidor: 5.7.41
-- Versión de PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wwalta_kyocera`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_uso_cfdi`
--

CREATE TABLE `f_uso_cfdi` (
  `id` int(11) NOT NULL,
  `uso_cfdi` varchar(10) NOT NULL,
  `uso_cfdi_text` text NOT NULL,
  `pararf` text NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `f_uso_cfdi`
--

INSERT INTO `f_uso_cfdi` (`id`, `uso_cfdi`, `uso_cfdi_text`, `pararf`, `activo`) VALUES
(1, 'G01', 'G01 Adquisición de mercancias', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625,626', 1),
(2, 'G02', 'G02 Devoluciones, descuentos o bonificaciones', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625,626', 1),
(3, 'G03', 'G03 Gastos en general', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625, 626', 1),
(4, 'I01', 'I01 Construcciones', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625, 626', 1),
(5, 'I02', 'I02 Mobilario y equipo de oficina por inversiones', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625, 626', 1),
(6, 'I03', 'I03 Equipo de transporte', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625, 626', 1),
(7, 'I04', 'I04 Equipo de computo y accesorios', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625, 626', 1),
(8, 'I05', 'I05 Dados, troqueles, moldes, matrices y herramental', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625, 626', 1),
(9, 'I06', 'I06 Comunicaciones telefónicas', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625, 626', 1),
(10, 'I07', 'I07 Comunicaciones satelitales', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625, 626', 1),
(11, 'I08', 'I08 Otra maquinaria y equipo', '601, 603, 606, 612, 620, 621, 622, 623, 624, 625, 626', 1),
(12, 'D01', 'D01 Honorarios médicos, dentales y gastos hospitalarios.', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(13, 'D02', 'D02 Gastos médicos por incapacidad o discapacidad', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(14, 'D03', 'D03 Gastos funerales.', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(15, 'D04', 'D04 Donativos.', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(16, 'D05', 'D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(17, 'D06', 'D06 Aportaciones voluntarias al SAR.', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(18, 'D07', 'D07 Primas por seguros de gastos médicos.', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(19, 'D08', 'D08 Gastos de transportación escolar obligatoria.', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(20, 'D09', 'D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(21, 'D10', 'D10 Pagos por servicios educativos (colegiaturas)', '605, 606, 608, 611, 612, 614, 607, 615, 625', 1),
(22, 'S01', 'S01 Sin efectos fiscales', '601, 603, 605, 606, 608, 610, 611, 612, 614, 616, 620, 621, 622, 623, 624, 607, 615, 625, 626', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `f_uso_cfdi`
--
ALTER TABLE `f_uso_cfdi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `f_uso_cfdi`
--
ALTER TABLE `f_uso_cfdi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
